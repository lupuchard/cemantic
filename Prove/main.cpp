
#include "Coder.h"
#include "Problem.h"

int main(int argc, char** argv) {
	for (size_t i = 3; i < 20; i++) {
		Problem problem;
		problem.generate(i, i);

		if (problem.get_precondition() != nullptr) std::cout << "Pre:  " << problem.get_precondition()->to_string() << std::endl;
		std::cout << "Post: " << problem.get_postcondition()->to_string() << std::endl;
	}
}
