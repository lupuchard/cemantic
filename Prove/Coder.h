#pragma once

#include "Op.h"
#include "Problem.h"
#include "Action.h"
#include <armadillo>
//#include <mlpack/methods/

class Coq;

class Coder {
public:
	Coder();

	void get_output();

private:
	struct Branch {
		Branch(): solution(new Op(OpKind::None)) { }

		inline Branch clone() const {
			Branch new_branch;
			new_branch.problem = problem.clone();
			new_branch.solution.reset(new Op(solution->clone()));
			//new_branch.proof.reset(new Coq(coq->clone())); TODO
			new_branch.actions = arma::vec(actions);
			new_branch.selected_action = selected_action;
			return new_branch;
		}

		Problem problem;
		Box<Op> solution;
		Box<Coq> proof;
		arma::vec actions;
		Action selected_action;
	};

	void evaluate_branch(Branch& branch);
	arma::vec evaluate_tree(const Op* op);

	bool verify_proof();

	void perform_action(ProofAction action, Action::AgeHint age_hint);
	Vec<Box<Branch>> perform_action(Branch& branch, SolutionAction action, Action::SolutionTypeHint type_hint,
	                                Action::SolutionPosHint pos_hint, Action::AgeHint age_hint);
	void find_insertion_points(Vec<Vec<char>>& insertion_points, Vec<char>& pos_stack, Op& op,
	                           Action::SolutionTypeHint type_hint, Action::SolutionPosHint pos_hint, Action::AgeHint age_hint);
	bool verify_type(Action::SolutionTypeHint type_hint, Type type);
	bool verify_pos(Action::SolutionPosHint pos_hint, bool is_left);
	bool verify_age(Action::AgeHint, Op& op);

	//arma::sp_mat generate_input();
	//void add_op_to_input(arma::sp_mat& input, Op* op, int offset);
};
