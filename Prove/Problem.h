#ifndef WHAT_PROBLEM_H
#define WHAT_PROBLEM_H

#include "Op.h"
#include "../Src/Util/Rando.h"
#include <unordered_map>

class Problem {
public:
	Problem clone() const;
	void generate(int length, int random_seed);

	inline const Op* get_precondition()  { return precondition.get(); }
	inline const Op* get_postcondition() { return postcondition.get(); }

private:
	void generate(Rando& rando, std::unique_ptr<Op>& to_generate, int length, bool include_ret);
	void finish_generate_op(Op& op, size_t idx, Vec<Pair<Op*, bool>>& gen_remaining_params,
	                                            Map<Type, Vec<Pair<Op*, bool>>>& gen_remaining_values);

	template<Type type, int min_params, int max_params>
	static const Vec<OpKind>& get_op_bag();
	static Vec<OpKind> generate_op_bag(Type type, int min_params, int max_params);
	static const Vec<OpKind>& get_op_bag(Type type, int remaining_length);
	template<Type type>
	static const Vec<OpKind>& get_op_bag(int remaining_length);
	OpKind rand_op(Rando& rando, Type type, int remaining_length);

	static int op_weight(OpKind op);
	static int get_num_params(const OpInfo& info, Type type);
	static int param_size(Type param_type, Type return_type, Type expected_type);

	Box<Op> precondition;
	Box<Op> postcondition;
};


#endif //WHAT_PROBLEM_H
