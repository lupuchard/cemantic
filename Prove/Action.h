#ifndef WHAT_ACTION_H
#define WHAT_ACTION_H

#include "armadillo"
#include "OpInfo.h"

enum class ProofAction {
	Auto,
	Assumption,
	Contradiction,
	Discriminate,
	Reflexivity,
	IntrosOne,
	IntrosAll,
	Simpl,
	SimplTo,
	Unfold,
	UnfoldTo,
	Apply,
	ApplyIn,
	RewriteTo,
	RewriteFrom,
	RewriteIn,
	Inversion,
	Left,
	Right,
	Split,
	Destruct,
	Induction,
	Ring,
	Tauto,
	Field,
	COUNT,
};

enum class SolutionAction {
	If,
	Zero, One, Two, Three, Four, Five, False, True,
	Eq, Not, And, Or, Lt, Add, Neg, Mul,
	Decl,
	Var,
	Ret,
	COUNT,
};

struct Action {
	enum Type { Proof, Solution };

	enum class AgeHint { Any, Latest, Eldest, Between, COUNT };
	enum class SolutionTypeHint { Any, Number, Bool, COUNT };
	enum class SolutionPosHint { Any, Left, Right, COUNT };

	ProofAction proof_action;
	AgeHint proof_age_hint;

	SolutionAction solution_action;
	SolutionTypeHint solution_type_hint;
	SolutionPosHint solution_pos_hint;
	AgeHint solution_age_hint;

	double confidence;
	int action_index;

	static inline int action_output_length() {
		return (int)ProofAction::COUNT + (int)SolutionAction::COUNT + (int)AgeHint::COUNT +
		       (int)SolutionTypeHint::COUNT + (int)SolutionPosHint::COUNT;
	}

	template<typename E>
	static E extract(const arma::vec& output, int start_hint) {
		return (E)(output.subvec(start_hint, start_hint + (int)E::COUNT).index_max() - start_hint);
	}

	static inline Action extract(const arma::vec& output) {
		const int START_PROOF_AGE_HINT = (int)ProofAction::COUNT + (int)SolutionAction::COUNT;
		const int START_SOLUTION_TYPE_HINT = START_PROOF_AGE_HINT + (int)AgeHint::COUNT;
		const int START_SOLUTION_POS_HINT = START_SOLUTION_TYPE_HINT + (int)SolutionTypeHint::COUNT;
		const int START_SOLUTION_AGE_HINT = START_SOLUTION_POS_HINT + (int)SolutionPosHint::COUNT;

		Action action;
		action.action_index = output.subvec(0, (int)ProofAction::COUNT + (int)SolutionAction::COUNT).index_max();
		action.confidence = output[action.action_index];
		if (action.action_index < (int)ProofAction::COUNT) {
			action.proof_action = (ProofAction)action.action_index;
			action.proof_age_hint = extract<AgeHint>(output, START_PROOF_AGE_HINT);
		} else {
			action.solution_action = (SolutionAction)(action.action_index - (int)ProofAction::COUNT);
			action.solution_type_hint = extract<SolutionTypeHint>(output, START_SOLUTION_TYPE_HINT);
			action.solution_pos_hint  = extract<SolutionPosHint>(output, START_SOLUTION_POS_HINT);
			action.solution_age_hint  = extract<AgeHint>(output, START_SOLUTION_AGE_HINT);
		}

		return action;
	}

	static inline OpKind to_op(SolutionAction action) {
		using enum SolutionAction;
		switch (action) {
			case If:    return OpKind::If;
			case Zero:  return OpKind::Zero;
			case One:   return OpKind::One;
			case Two:   return OpKind::Two;
			case Three: return OpKind::Three;
			case Four:  return OpKind::Four;
			case Five:  return OpKind::Five;
			case False: return OpKind::False;
			case True:  return OpKind::True;
			case Eq:    return OpKind::Eq;
			case Not:   return OpKind::Not;
			case And:   return OpKind::And;
			case Or:    return OpKind::Or;
			case Lt:    return OpKind::Lt;
			case Add:   return OpKind::And;
			case Neg:   return OpKind::Neg;
			case Mul:   return OpKind::Mul;
			case Decl:
			case Var:   return OpKind::Reg;
			case Ret:   return OpKind::Ret;
			default:
				assert(false);
				return OpKind::None;
		}
	}
};

#endif //WHAT_ACTION_H
