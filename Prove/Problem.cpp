#include "Problem.h"
#include "../Src/Util/Rando.h"

using enum Type;

void Problem::generate(int length, int random_seed) {
	Rando rando(random_seed);
	int precond_length = rando.rand(0, std::max(length - 3, 2) / 2);
	if (precond_length < 3) precond_length = 0;
	int postcond_length = length - precond_length;

	if (precond_length > 0) {
		generate(rando, precondition, precond_length, false);
	}
	generate(rando, postcondition, postcond_length, true);
}

void Problem::generate(Rando& rando, Box<Op>& to_generate, int length, bool include_ret) {
	Vec<std::pair<Op*, bool>> gen_remaining_params;
	Map<Type, Vec<Pair<Op*, bool>>> gen_remaining_values;
	gen_remaining_params.emplace_back(nullptr, false);

	// Generate operators first, track values
	to_generate.reset(new Op(rand_op(rando, Bool, length), Bool));
	finish_generate_op(*to_generate, 0, gen_remaining_params, gen_remaining_values);
	while (!gen_remaining_params.empty()) {
		size_t idx = gen_remaining_params.size() > 1 ? rando.rand(0, gen_remaining_params.size()) : 0;
		auto& next = gen_remaining_params[idx];

		int remaining_length = length;
		for (auto& pair : gen_remaining_params) {
			if (&pair == &next) continue;
			if (pair.first->param_type(pair.second) == Bool) remaining_length -= 3;
			else remaining_length -= 1;
		}
		OpKind op = rand_op(rando, next.first->param_type(next.second), remaining_length);
		Op* new_op = next.first->add_child(op, next.second);
		assert(new_op != nullptr);

		finish_generate_op(*new_op, idx, gen_remaining_params, gen_remaining_values);
		length--;
	}

	// Next fill out numeric values, starting with Ret and A
	if (gen_remaining_values.count(Number)) {
		auto& remaining_nums = gen_remaining_values[Number];
		rando.shuffle(remaining_nums);
		size_t i = 0;
		if (include_ret) {
			auto& next = remaining_nums[i++];
			next.first->add_child(OpKind::Ret, next.second);
		}

		if (i >= remaining_nums.size()) return;
		auto& next = remaining_nums[i++];
		next.first->add_child(OpKind::Param, next.second)->add_child(OpKind::A, true);

		for (; i < remaining_nums.size(); i++) {
			auto& next = remaining_nums[i];
			switch (rando.rand(0, 4)) {
				case 0:
				case 1: next.first->add_child((OpKind)(rando.rand(0, 6) + (int)OpKind::Zero), next.second); break;
				case 2: next.first->add_child(OpKind::Ret, next.second); break;
				case 3: next.first->add_child(OpKind::Param, next.second)->add_child(OpKind::B, true); break;
			}
		}
	}

	// Finally fill out boolean values
	if (gen_remaining_values.count(Bool)) {
		auto& remaining_bools = gen_remaining_values[Bool];
		for (size_t i = 0; i < remaining_bools.size(); i++) {
			auto& next = remaining_bools[i];
			next.first->add_child(rando.rand(0, 2) ? OpKind::True : OpKind::False, next.second);
		}
	}
}

void Problem::finish_generate_op(Op& op, size_t idx, Vec<Pair<Op*, bool>>& gen_remaining_params,
                                                     Map<Type, Vec<Pair<Op*, bool>>>& gen_remaining_values) {
	if (op.get_kind() == OpKind::If) {
		op.add_right(OpKind::Pair);
		gen_remaining_params[idx] = std::pair(&op, true);
		gen_remaining_params.emplace_back(op.right(), true);
		gen_remaining_params.emplace_back(op.right(), false);
		return;
	} else if (op.get_kind() == OpKind::Zero || op.get_kind() == OpKind::True) {
		gen_remaining_values[op.return_type()].push_back(gen_remaining_params[idx]);
		gen_remaining_params.erase(gen_remaining_params.begin() + idx);
		return;
	}/* else if (op.get_kind() == Param) {
		remaining.erase(remaining.begin() + idx);
		op.add_left(A);
		return;
	}*/

	auto& info = OpInfo::Of(op.get_kind());
	if (info.left != Void) {
		gen_remaining_params[idx] = std::pair(&op, true);
		if (info.right != Void) gen_remaining_params.emplace_back(&op, false);
	} else {
		gen_remaining_params.erase(gen_remaining_params.begin() + idx);
	}
}

int Problem::op_weight(OpKind op) {
	using enum OpKind;
	switch (op) {
		case If:
			return 1;
		case Zero:
			return 10;
		/*case One:
		case Two:
		case Three:
		case Four:
		case Five:
			return 1;
		case False:*/
		case True:
			return 1;
		case Eq:
		case Lt:
			return 3;
		case Not:
		case And:
		case Or:
		case Add:
		case Neg:
		case Mul:
			return 1;
		/*case Param:
		case Ret:
			return 2;*/
		default:
			return 0;
	}
}

int Problem::get_num_params(const OpInfo& info, Type type) {
	if (info.returns == Digit) return 0;
	if (info.kind == OpKind::If) return 3 + param_size(info.right, info.returns, type) * 2;
	return param_size(info.left, info.returns, type) + param_size(info.right, info.returns, type);
}

int Problem::param_size(Type param_type, Type return_type, Type expected_type) {
	if (param_type == Void || param_type == Ident) return 0;
	if (param_type == Bool || (param_type == Inherit && return_type == Inherit && expected_type == Bool)) return 3;
	return 1;
}

template<Type type, int min_params, int max_params>
const Vec<OpKind>& Problem::get_op_bag() {
	static Vec<OpKind> bag = generate_op_bag(type, min_params, max_params);
	return bag;
}

Vec<OpKind> Problem::generate_op_bag(Type type, int min_params, int max_params) {
	Vec<OpKind> bag;
	for (size_t i = 0; i < (int)OpKind::COUNT; i++) {
		if (op_weight((OpKind)i) == 0) continue;
		auto& info = OpInfo::Of((OpKind)i);
		if (!(type == Inherit || info.returns == Inherit || OpInfo::CastsTo(info.returns, type))) continue;
		int num_params = get_num_params(info, type);
		if (num_params < min_params || num_params > max_params) continue;

		for (int j = 0; j < op_weight((OpKind)i); j++) {
			bag.push_back((OpKind)i);
		}
	}

	return bag;
}

const Vec<OpKind>& Problem::get_op_bag(Type type, int len) {
	switch (type) {
		case Bool:    return get_op_bag<Bool>(len);
		case Number:
		case Digit:   return get_op_bag<Number>(len);
		case Inherit: return get_op_bag<Inherit>(len);
		default:
			assert(false);
			return get_op_bag<Void, 0, 0>();
	}
}

template<Type type>
const Vec<OpKind>& Problem::get_op_bag(int remaining_length) {
	switch (remaining_length) {
		case 2: return get_op_bag<type, 0, 1>();
		case 3: return get_op_bag<type, 1, 2>();
		case 4: return get_op_bag<type, 1, 3>();
		case 5: return get_op_bag<type, 1, 4>();
		case 6: return get_op_bag<type, 2, 5>();
		case 7: return get_op_bag<type, 3, 6>();
		case 8: return get_op_bag<type, 3, 7>();
		case 9: return get_op_bag<type, 3, 8>();
		default:
			if (remaining_length <= 1) {
				return get_op_bag<type, 0, 0>();
			} else {
				return get_op_bag<type, 3, 9>();
			}
	}
}

OpKind Problem::rand_op(Rando& rando, Type type, int remaining_length) {
	auto& vec = get_op_bag(type, remaining_length);
	return vec[rando.rand(0, vec.size())];
}

Problem Problem::clone() const {
	Problem new_problem;
	new_problem.precondition.reset(new Op(precondition->clone()));
	new_problem.postcondition.reset(new Op(postcondition->clone()));
	return new_problem;
}
