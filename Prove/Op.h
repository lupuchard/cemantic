#ifndef WHAT_OP_H
#define WHAT_OP_H

#include "OpInfo.h"
#include "../Src/Util/TypeUtil.h"
#include <string>

class Op {
	using enum Type;

public:
	Op(OpKind kind, Op* parent): kind(kind), /*inherit(OpInfo::Of(kind).returns),*/ parent_op(parent) {
		//type = OpInfo::Of(kind).returns;
	}

	Op(OpKind kind, Type type = Inherit): kind(kind) {
		if (type != Inherit) {
			assert(OpInfo::Of(kind).returns == type || OpInfo::Of(kind).returns == Inherit);
			this->inherit = type;
		}
	}

	Op clone() const {
		Op new_op(kind, parent_op);
		new_op.inherit = inherit;
		if (left_op != nullptr)  new_op.left_op  = std::make_unique<Op>(left_op->clone());
		if (right_op != nullptr) new_op.right_op = std::make_unique<Op>(right_op->clone());
		return new_op;
	}

	inline Op* add_left(OpKind new_op) {
		return add_child(left_op, OpInfo::Of(kind).left, new_op);
	}

	inline Op* add_right(OpKind new_op) {
		return add_child(right_op, OpInfo::Of(kind).right, new_op);
	}

	inline Op* add_child(OpKind new_op, bool left) {
		return left ? add_left(new_op) : add_right(new_op);
	}

	inline const Op* left()   const { return left_op.get(); }
	inline       Op* left()         { return left_op.get(); }
	inline const Op* right()  const { return right_op.get(); }
	inline       Op* right()        { return right_op.get(); }
	inline const Op* parent() const { return parent_op; }
	inline       Op* parent()       { return parent_op; }
	inline const Op* child(bool left) const { return left ? left_op.get() : right_op.get(); }
	inline       Op* child(bool left)       { return left ? left_op.get() : right_op.get(); }

	inline OpKind get_kind() const {
		return kind;
	}

	inline Op* get_descendant(const Vec<char>& sides) {
		Op* cur = this;
		for (size_t i = 0; i < sides.size() && cur != nullptr; i++) {
			cur = cur->child(sides[i]);
		}
		return cur;
	}

	/*inline Type inherited() const {
		return inherit;
	}*/

	inline Type return_type() const {
		auto& info = OpInfo::Of(kind);
		return OpInfo::ConvertsTo((info.returns == Inherit) ? inherit : info.returns);
	}

	inline Type param_type(bool left) const {
		auto& info = OpInfo::Of(kind);
		Type type = left ? info.left : info.right;
		return (type == Inherit) ? inherit : type;
	}

	double eval_number();
	bool eval_bool();

	std::string to_string() const;

private:
	Op* add_child(std::unique_ptr<Op>& side, Type expected_type, OpKind new_op);
	void update_inherited_type(Type new_type);

	OpKind kind;
	Type inherit = Inherit;
	Box<Op> left_op;
	Box<Op> right_op;
	Op* parent_op = nullptr;
};

#endif //WHAT_OP_H
