#include "Op.h"

using enum Type;

Op* Op::add_child(std::unique_ptr<Op>& side, Type expected_type, OpKind new_op) {
	auto& child_info = OpInfo::Of(new_op);
	Type child_type = child_info.returns;

	if (child_info.parent != OpKind::Any && child_info.parent != kind) {
		return nullptr;
	}

	if (expected_type == Inherit && OpInfo::Of(kind).returns == Inherit) {
		expected_type = inherit;
	}

	if (child_type == Inherit) {
		side.reset(new Op(new_op, this));
		side->inherit = OpInfo::ConvertsTo(expected_type);
	} else if (expected_type == Inherit) {
		update_inherited_type(child_type);
		side.reset(new Op(new_op, this));
	} else if (OpInfo::CastsTo(child_type, expected_type)) {
		side.reset(new Op(new_op, this));
	} else {
		return nullptr;
	}

	return side.get();
}

void Op::update_inherited_type(Type new_type) {
	if (inherit == Inherit) {
		inherit = OpInfo::ConvertsTo(new_type);
	} else {
		assert(OpInfo::ConvertsTo(new_type) == inherit || OpInfo::Of(kind).returns != Inherit);
		return;
	}

	if (left_op != nullptr) {
		left_op->update_inherited_type(new_type);
	}

	if (right_op != nullptr) {
		right_op->update_inherited_type(new_type);
	}

	if (parent_op != nullptr) {
		auto& parent_info = OpInfo::Of(parent_op->kind);
		if ((parent_op->left_op.get() == this && parent_info.left == Inherit) ||
		   (parent_op->right_op.get() == this && parent_info.right == Inherit)) {
			parent_op->update_inherited_type(new_type);
		}
	}
}

double Op::eval_number() {
	assert(inherit == Number || inherit == Digit);
	using enum OpKind;

	if (kind >= Zero && kind <= Five) {
		Op* cur = this;
		while (cur->left_op != nullptr) {
			cur = cur->left_op.get();
		}

		double val = 0;
		while (cur != this->parent_op) {
			val = val * 6 + ((int)cur->kind - (int)Zero);
			cur = cur->parent_op;
		}

		return val;
	}

	switch (kind) {
		case Mul:
			assert(left_op != nullptr && right_op != nullptr);
			return left_op->eval_number() * right_op->eval_number();
		case Add:
			assert(left_op != nullptr && right_op != nullptr);
			return left_op->eval_number() + right_op->eval_number();
		case Neg:
			assert(left_op != nullptr);
			return -left_op->eval_number();
		case If:
			assert(left_op != nullptr && right_op != nullptr && right_op->left_op != nullptr && right_op->right_op != nullptr);
			if (left_op->eval_bool()) {
				return right_op->left_op->eval_number();
			} else {
				return right_op->right_op->eval_number();
			}
		//case Var:
		//	break;
		default: assert(false);
	}

	return 0;
}

bool Op::eval_bool() {
	assert(inherit == Bool);
	using enum OpKind;

	switch (kind) {
		case Lt:
			assert(left_op != nullptr && right_op != nullptr);
			return left_op->eval_number() < right_op->eval_number();
		case Eq:
			assert(left_op != nullptr && right_op != nullptr);
			switch (left_op->inherit) {
				case Bool: return left_op->eval_bool() == right_op->eval_bool();
				case Number:
				case Digit: return left_op->eval_number() == right_op->eval_number();
				default: assert(false);
			}
		case And:
			assert(left_op != nullptr && right_op != nullptr);
			return left_op->eval_bool() && right_op->eval_bool();
		case Or:
			assert(left_op != nullptr && right_op != nullptr);
			return left_op->eval_bool() || right_op->eval_bool();
		case Not:
			assert(left_op != nullptr && right_op != nullptr);
			return !left_op->eval_bool();
		case If:
			assert(left_op != nullptr && right_op != nullptr && right_op->left_op != nullptr && right_op->right_op != nullptr);
			if (left_op->eval_bool()) {
				return right_op->left_op->eval_bool();
			} else {
				return right_op->right_op->eval_bool();
			}
		case False: return false;
		case True: return true;
		//case Var: break;
		default: assert(false);
	}

	return false;
}

enum class DisplayType {
	Func,
	Op,
};

std::string Op::to_string() const {
	using enum OpKind;
	std::string name;
	auto display = DisplayType::Func;
	switch (kind) {
		case Any:   name = "Any";   break;
		case None:  name = "None";  break;
		case If:    name = "If";    break;
		case Pair:  name = "?";     break;
		case Zero:  name = "0";     break;
		case One:   name = "1";     break;
		case Two:   name = "2";     break;
		case Three: name = "3";     break;
		case Four:  name = "4";     break;
		case Five:  name = "5";     break;
		case False: name = "False"; break;
		case True:  name = "True";  break;
		case Eq:    name = "==";    display = DisplayType::Op; break;
		case Not:   name = "!";     display = DisplayType::Op; break;
		case And:   name = "&&";    display = DisplayType::Op; break;
		case Or:    name = "||";    display = DisplayType::Op; break;
		case Lt:    name = "<";     display = DisplayType::Op; break;
		case Add:   name = "+";     display = DisplayType::Op; break;
		case Neg:   name = "-";     display = DisplayType::Op; break;
		case Mul:   name = "*";     display = DisplayType::Op; break;
		case Param: return left_op->to_string();
		case A:     name = "A";     break;
		case B:     name = "B";     break;
		case C:     name = "C";     break;
		case D:     name = "D";     break;
		case E:     name = "E";     break;
		case F:     name = "F";     break;
		case Reg:   name = "Reg";   break;
		case I:     name = "I";     break;
		case J:     name = "J";     break;
		case K:     name = "K";     break;
		case L:     name = "L";     break;
		case M:     name = "M";     break;
		case N:     name = "N";     break;
		case Ret:   name = "Ret";   break;
		default:    name = "???";   break;
	}

	if (display == DisplayType::Func) {
		if (left_op == nullptr) {
			if (right_op == nullptr) {
				return name;
			} else {
				return name + "(, " + right_op->to_string() + ")";
			}
		} else {
			if (right_op == nullptr) {
				return name + "(" + left_op->to_string() + ")";
			} else {
				return name + "(" + left_op->to_string() + ", " + right_op->to_string() + ")";
			}
		}
	} else {
		if (left_op == nullptr) {
			if (right_op == nullptr) {
				return name;
			} else {
				return "(" + name + left_op->to_string() + ")";
			}
		} else {
			if (right_op == nullptr) {
				return name + left_op->to_string();
			} else {
				return "(" + left_op->to_string() + " " + name + " " + right_op->to_string() + ")";
			}
		}
	}
}
