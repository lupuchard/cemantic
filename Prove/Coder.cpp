#include "Coder.h"

//const int INPUT_DEPTH = 4;
const int TREENET_OUTPUT_SIZE = 10;
const int TREENET_WIDTH = 10;
const int TREENET_NUM_LAYERS = 3;
using enum Type;

Coder::Coder() {
	/*
	 int input_size = NUM_TYPES + OpKind::COUNT + TREENET_OUTPUT_SIZE * 2;
	 RNN<> model;
	 model.Add<Linear<>>(input_size, TREENET_WIDTH);
     model.Add<FlexibleReLU<>>();
     for (int i = 0; i < TREENET_NUM_LAYERS; i++) {

     }
	*/
}

void Coder::get_output() {
	Vec<Box<Branch>> branches;
	branches.push_back(std::make_unique<Branch>());

	while (true) {
		//Branch& branch = select_branch(branches);
		Branch* best_branch = branches[0].get();
		double best_action = best_branch->selected_action.confidence;
		for (size_t i = 1; i < branches.size(); i++) {
			double selected_action = branches[i]->selected_action.confidence;
			if (selected_action > best_action) {
				best_branch = branches[i].get();
				best_action = selected_action;
			}
		}

		best_branch->actions[best_branch->selected_action.action_index] = -std::numeric_limits<double>::infinity();
		best_branch->selected_action = Action::extract(best_branch->actions);

		if (verify_proof()) {
			break;
		}
	}
}

void Coder::evaluate_branch(Branch& branch) {
	// five op trees:
	//   problem precondition
	//   problem postcondition
	//   current proof goal
	//   current proof hypotheses
	//   solution
	// two networks:
	//   op tree network - combines op tree together into single passup output
	//   action network - takes outputs of the five op trees and outputs the action to take next

	arma::vec precond_output    = evaluate_tree(branch.problem.get_precondition());
	arma::vec postcond_output   = evaluate_tree(branch.problem.get_postcondition());
	//arma::vec subgoals_output   = evaluate_tree(branch.coq->get_subgoals());
	//arma::vec hypotheses_output = evaluate_tree(branch.coq->get_hypotheses());
	arma::vec solution_output   = evaluate_tree(branch.solution.get());

	//branch.actions = nn.something(...)

	branch.selected_action = Action::extract(branch.actions);
}

arma::vec Coder::evaluate_tree(const Op* op) {
	if (op == nullptr) {
		// TODO: this is where neural network evaluation goes
	}
}

/*arma::sp_mat Coder::generate_input() {
	arma::sp_mat input_mat((1 << INPUT_DEPTH) * ((int)TYPE_MAX + (int)OPKIND_MAX) * 2, 1);
	add_op_to_input(input_mat, spec_pos, 0);
	add_op_to_input(input_mat, code_pos, 1 << INPUT_DEPTH);
	return input_mat;
}*/

/*void Coder::add_op_to_input(arma::sp_mat& input_mat, Op* op, int offset) {
	if (op->parent() != nullptr) {
		input_mat[offset + (int)op->parent()->return_type()] = 1;
		input_mat[offset + (int)NUM_TYPES + (int)op->parent()->get_kind()] = 1;
	}

	Vec<const Op*> cur_layer;
	cur_layer.push_back(op);
	for (size_t i = 0; i < INPUT_DEPTH; i++) {
		Vec<const Op*> next_layer;
		for (size_t j = 0; j < cur_layer.size(); j++) {
			const Op* op = cur_layer[j];
			if (i < INPUT_DEPTH - 1) {
				if (op == nullptr) {
					next_layer.push_back(nullptr);
					next_layer.push_back(nullptr);
					continue;
				} else {
					next_layer.push_back(op->left());
					next_layer.push_back(op->right());
				}
			}

			int idx = ((1 << i) + j) * ((int)NUM_TYPES + (int)OpKind::COUNT) + offset;
			assert(op->get_kind() < OpKind::COUNT && (int)op->get_kind() >= 0);
			assert(op->return_type() < NUM_TYPES && (int)op->return_type() >= 0);
			input_mat[idx + (int)op->return_type()] = 1;
			input_mat[idx + (int)NUM_TYPES + (int)op->get_kind()] = 1;
		}

		cur_layer = next_layer;
	}
}*/

Vec<Box<Coder::Branch>> Coder::perform_action(Branch& branch, SolutionAction action, Action::SolutionTypeHint type_hint,
	                                          Action::SolutionPosHint pos_hint, Action::AgeHint age_hint) {
	Vec<Box<Branch>> new_branches;

	if (action == SolutionAction::Decl) {
		// TODO
		return new_branches;
	}

	OpKind new_op = Action::to_op(action);
	auto& info = OpInfo::Of(new_op);
	switch (info.returns) {
		case Void:
		case Ident:
			return new_branches;
		case Bool:
			if (type_hint == Action::SolutionTypeHint::Any) {
				type_hint = Action::SolutionTypeHint::Bool;
			} else if (type_hint != Action::SolutionTypeHint::Bool) {
				return new_branches;
			} break;
		case Digit:
		case Number:
			if (type_hint == Action::SolutionTypeHint::Any) {
				type_hint = Action::SolutionTypeHint::Number;
			} else if (type_hint != Action::SolutionTypeHint::Number) {
				return new_branches;
			} break;
		case Inherit:
			break;
		default:
			assert(false);
			return new_branches;
	}

	Vec<Vec<char>> insertion_points; // The char is a bool
	Vec<char> pos_stack;
	find_insertion_points(insertion_points, pos_stack, *branch.solution, type_hint, pos_hint, age_hint);

	for (auto& insertion_point : insertion_points) {
		new_branches.push_back(std::make_unique<Branch>(branch.clone()));
		auto& new_solution = *new_branches.back()->solution;
		Op* parent = new_solution.get_descendant(insertion_point)->parent();
		bool is_left = insertion_point.back();
		if (action == SolutionAction::Decl) {
			// TODO
		} else if (action == SolutionAction::Var) {
			// TODO any number of vars, from Param and Decl
			Op* var = parent->add_child(OpKind::Param, is_left);
			var->add_left(OpKind::A);

			new_branches.push_back(std::make_unique<Branch>(branch.clone()));
			Op* parent2 = new_branches.back()->solution->get_descendant(insertion_point)->parent();
			Op* var2 = parent2->add_child(OpKind::Param, is_left);
			var2->add_left(OpKind::B);
		} else if (action == SolutionAction::If) {
			Op* if_op = parent->add_child(new_op, is_left);
			if_op->add_left(OpKind::None);
			Op* child = if_op->add_right(OpKind::Pair);
			child->add_left( OpKind::None);
			child->add_right(OpKind::None);
		} else {
			Op* child = parent->add_child(new_op, is_left);
			if (info.left != Void)  child->add_left( OpKind::None);
			if (info.right != Void) child->add_right(OpKind::None);
		}
	}

	return new_branches;
}

void Coder::find_insertion_points(Vec<Vec<char>>& insertion_points, Vec<char>& pos_stack, Op& op,
                                  Action::SolutionTypeHint type_hint, Action::SolutionPosHint pos_hint, Action::AgeHint age_hint) {
	for (int is_left = 0; is_left < 2; is_left++) {
		pos_stack.push_back(is_left);
		Op* child = op.child((bool)is_left);
		auto& info = OpInfo::Of(op.get_kind());
		if (child != nullptr) {
			if (child->get_kind() == OpKind::None) {
				if (verify_type(type_hint, op.param_type(is_left)) && verify_pos(pos_hint, is_left) && verify_age(age_hint, *child)) {
					insertion_points.push_back(pos_stack);
				}
			} else {
				find_insertion_points(insertion_points, pos_stack, *child, type_hint, pos_hint, age_hint);
			}
		}
		pos_stack.pop_back();
	}
}

bool Coder::verify_type(Action::SolutionTypeHint type_hint, Type type) {
	return type_hint == Action::SolutionTypeHint::Any || type == Inherit ||
	      (type_hint == Action::SolutionTypeHint::Bool && type == Bool) ||
	      (type_hint == Action::SolutionTypeHint::Number && type == Number);
}

bool Coder::verify_pos(Action::SolutionPosHint pos_hint, bool is_left) {
	return pos_hint == Action::SolutionPosHint::Any ||
	       (pos_hint == Action::SolutionPosHint::Left && is_left) ||
	       (pos_hint == Action::SolutionPosHint::Right && !is_left);
}

bool Coder::verify_age(Action::AgeHint age_hint, Op& op) {
	// TODO
	return true;
}
