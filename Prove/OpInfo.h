#ifndef WHAT_OPINFO_H
#define WHAT_OPINFO_H

#include <cassert>
#include <limits>

enum class Type {
	Void = 0,
	// Null = 1,
	// Misc = 2,
	Bool = 3,

	Digit  = 4,
	Number = 5,
	// Integer = 6,
	// Byte = 7,

	// List = 8,
	// Set = 9,
	// Map = 10,
	// Queue?

	Ident = 12,
	NUM_TYPES = 16,
	Inherit,
};

enum class OpKind {
	Any = -1,
	None = 0,

	// Let = 1,
	// Call = 2,
	// Param = 3,
	If = 8,
	Pair = 9,
	// Seq = 10,
	// While = 11
	// Continue,
	// Break,
	// Goto,

	Zero  = 30,
	One   = 31,
	Two   = 32,
	Three = 33,
	Four  = 34,
	Five  = 35,
	False = 36,
	True  = 37,

	Eq = 40,
	// Neq = 41,
	Not = 42,
	And = 43,
	Or  = 44,
	// Forall = 48,
	// Exists = 49,

	Lt  = 50,
	Add = 51,
	Neg = 52,
	Mul = 53,

	Param = 100, A = 101, B, C, D, E, F,
	Reg   = 110, I = 111, J, K, L, M, N,
	Ret   = 120,

	COUNT = 128,
};

struct OpInfo {
	using enum Type;
	using enum OpKind;

	OpInfo(OpKind kind, Type returns, Type left, Type right = Void, OpKind parent = Any):
			kind(kind), parent(parent), returns(returns), left(left), right(right) {
	}

	OpKind kind;
	OpKind parent;
	Type returns;
	Type left;
	Type right;

	template<OpKind kind, Type returns, Type left = Void, Type right = Void, OpKind parent = Any>
	static const OpInfo& New() {
		static OpInfo info(kind, returns, left, right, parent);
		return info;
	}

	static const OpInfo& Of(OpKind kind) {
		switch (kind) {
			case None:  return New<None,  Void>();
			case Mul:   return New<Mul,   Number,  Number,  Number>();
			case Add:   return New<Add,   Number,  Number,  Number>();
			case Neg:   return New<Neg,   Number,  Number>();
			case Lt:    return New<Lt,    Bool,    Number,  Number>();
			case Eq:    return New<Eq,    Bool,    Inherit, Inherit>();
			case And:   return New<And,   Bool,    Bool,    Bool>();
			case Or:    return New<Or,    Bool,    Bool,    Bool>();
			case Not:   return New<Not,   Bool,    Bool>();
			case If:    return New<If,    Inherit, Bool,    Inherit>();
			case Pair:  return New<Pair,  Inherit, Inherit, Inherit, If>();
			case Zero:  return New<Zero,  Digit,   Digit>();
			case One:   return New<One,   Digit,   Digit>();
			case Two:   return New<Two,   Digit,   Digit>();
			case Three: return New<Three, Digit,   Digit>();
			case Four:  return New<Four,  Digit,   Digit>();
			case Five:  return New<Five,  Digit,   Digit>();
			case False: return New<False, Bool>();
			case True:  return New<True,  Bool>();
			case Param: return New<Param, Number, Ident>();
			case Reg:   return New<Reg,   Number, Ident>();
			case Ret:   return New<Ret,   Number>();
			default: return New<A, Ident>();
		}
	}

	static bool CastsTo(Type from, Type to) {
		if (from == to) return true;
		if (from == Digit && to == Number) return true;
		return false;
	}

	static Type ConvertsTo(Type from) {
		if (from == Digit) {
			return Number;
		}

		return from;
	}
};

#endif //WHAT_OPINFO_H
