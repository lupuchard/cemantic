#include "Sim.h"
#include "Evaluator.h"

Sim::Sim(): rando(123456) { }

const Vec<ItemId>& Sim::all_items() const {
	return items;
}

const Vec<Tk>& Sim::item_attributes(ItemId item) const {
	static Vec<Tk> empty;
	auto iter = attribute_keys.find(item);
	return iter == attribute_keys.end() ? empty : iter->second;
}

const Vec<Tk>& Sim::item_children(ItemId item) const {
	static Vec<Tk> empty;
	auto iter = child_keys.find(item);
	return iter == child_keys.end() ? empty : iter->second;
}

Tk Sim::item_attribute(ItemId item, Tk key) const {
	assert(!item.is_var());
	auto iter = attributes.find(std::make_pair(item, key));
	return iter == attributes.end() ? Tk::Null : iter->second;
}

ItemId Sim::item_child(ItemId item, Tk key) const {
	assert(!item.is_var());
	auto iter = children.find(std::make_pair(item, key));
	return iter == children.end() ? ItemId() : iter->second;
}

ItemId Sim::new_item() {
	if (items.empty()) {
		items.emplace_back(ItemId(NUM_RESERVED_ITEM_IDS));
	} else {
		items.emplace_back(ItemId(items.back().val + 1));
	}
	return items.back();
}

ItemId Sim::new_item(std::string name) {
	ItemId item = new_item();
	item_names[item] = name;
	item_name_map[name] = item;
	return item;
}

ItemId Sim::get_item(const std::string& name) const {
	auto iter = item_name_map.find(name);
	return iter == item_name_map.end() ? ItemId(0) : iter->second;
}

void Sim::set_item_attribute(ItemId item, Tk attrib, Tk val) {
	assert(!item.is_var());
	auto iter = attributes.find(std::make_pair(item, attrib));
	if (iter == attributes.end()) {
		attribute_keys[item].push_back(attrib);
		attributes[std::make_pair(item, attrib)] = val;
	} else {
		iter.value() = val;
	}
}

void Sim::set_item_child(ItemId item, Tk key, ItemId child) {
	assert(!item.is_var());
	auto iter = children.find(std::make_pair(item, key));
	if (iter == children.end()) {
		child_keys[item].push_back(key);
		children[std::make_pair(item, key)] = child;
	} else {
		iter.value() = child;
	}
}

void Sim::perform_action(Action action) {
	Evaluator evaluator(*this);
	for (MutCausal causal : events) {
		rep_all(causal, ACTION_TARGET, action.target);

		if (evaluator.evaluate_bool(causal.get_condition())) {
			auto trait = causal.get_event().get_trait();
			if (trait.get_value().is_tk()) {
				set_item_attribute(causal.get_event().get_item(), trait.get_key(), trait.get_value().tk());
			} else {
				set_item_child(causal.get_event().get_item(), trait.get_key(), trait.get_value().item());
			}
		}

		evaluator.clear_assignments();
	}
}

void Sim::enable_event(const MutCausal& causal) {
	events.insert(causal);
}

void Sim::disable_event(const MutCausal& causal) {
	events.erase(causal);
}

std::string Sim::item_name(ItemId item) const {
	if (item.is_null()) {
		return "NULL";
	} else if (item.is_var()) {
		static std::string vars = "!XYZWABCDEFGHIJKLMNOPQRSTUV";
		return std::string(1, vars[item.val]);
	} else {
		auto iter = item_names.find(item);
		if (iter == item_names.end()) {
			Tk kind = item_attribute(item, Tk::Kind);
			return ::to_string(kind) + "#" + ::to_string(item.val);
		}
		return iter->second;
	}
}
