#ifndef WHAT_VISIBLE_H
#define WHAT_VISIBLE_H

#include "../Brain/Knowledge/ItemId.h"
#include "TypeUtil.h"

class Visible {
public:
	virtual const Vec<ItemId>& all_items() const = 0;
	virtual const Vec<Tk>& item_attributes(ItemId item) const = 0;
	virtual const Vec<Tk>& item_children(ItemId item) const = 0;
	virtual Tk item_attribute(ItemId item, Tk key) const = 0;
	virtual ItemId item_child(ItemId item, Tk key) const = 0;
	virtual std::string item_name(ItemId item) const = 0;
	virtual ItemId get_item(const std::string& name) const = 0;
};

#endif //WHAT_VISIBLE_H
