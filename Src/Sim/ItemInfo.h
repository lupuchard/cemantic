#ifndef PROJECT_ITEMINFO_H
#define PROJECT_ITEMINFO_H

//#include <Knowledge/Item.h>

/*class Hub: public BasicItemInfo {
public:
	explicit Hub(int counter = 0): BasicItemInfo(BasicItemKind::Hub), counter(counter) { }
	BasicItemInfo* clone(std::function<BasicItemInfo*(BasicItemInfo&)> clone_child) const override;

	bool active() const override;
	double stat() const override;
	const std::vector<BasicItemInfo*>& children() const override;
	const std::vector<BasicItemInfo*>& locked_children() const;

	void add_exit(BasicItemInfo& location);
	void add_locked(BasicItemInfo& location);
	void unlock();
	void modify_counter(int amount);

private:
	int counter = 1;
	std::vector<BasicItemInfo*> exits;
	std::vector<BasicItemInfo*> locked;
};

class Shed: public BasicItemInfo {
public:
	Shed(): BasicItemInfo(BasicItemKind::Shed) { }
	BasicItemInfo* clone(std::function<BasicItemInfo*(BasicItemInfo&)> clone_child) const override;

	const std::vector<BasicItemInfo*>& children() const override;
	void add_item(BasicItemInfo& item);

private:
	std::vector<BasicItemInfo*> items;
};

class Key: public BasicItemInfo {
public:
	explicit Key(Hub* door): BasicItemInfo(BasicItemKind::Key), door(door) { }
	BasicItemInfo* clone(std::function<BasicItemInfo*(BasicItemInfo&)> clone_child) const override;

	Hub* get_door() const;

private:
	Hub* door;
};

class Lever: public BasicItemInfo {
public:
	Lever(BasicItemInfo* target, int modify): BasicItemInfo(BasicItemKind::Lever), target(target), modify(modify) { }
	BasicItemInfo* clone(std::function<BasicItemInfo*(BasicItemInfo&)> clone_child) const override;

	bool active() const override;

	void pull();

private:
	BasicItemInfo* target = nullptr;
	int modify = 0;
	bool pulled = false;
};*/

#endif //PROJECT_ITEMINFO_H
