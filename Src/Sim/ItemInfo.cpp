#include "ItemInfo.h"

/*bool Hub::active() const {
	return locked.empty();
}

double Hub::stat() const {
	return counter;
}

const std::vector<BasicItemInfo*>& Hub::children() const {
	return exits;
}

const std::vector<BasicItemInfo*>& Hub::locked_children() const {
	return locked;
}

void Hub::add_exit(BasicItemInfo& location) {
	exits.push_back(&location);
	location.set_parent(*this);
}

void Hub::add_locked(BasicItemInfo& location) {
	locked.push_back(&location);
	location.set_parent(*this);
}

void Hub::unlock() {
	if (active()) return;
	exits.insert(exits.end(), locked.begin(), locked.end());
	locked.clear();
}

void Hub::modify_counter(int amount) {
	counter += amount;
	if (counter <= 0) unlock();
}

BasicItemInfo* Hub::clone(std::function<BasicItemInfo*(BasicItemInfo&)> clone_child) const {
	Hub* new_hub = new Hub(*this);
	for (size_t i = 0; i < new_hub->exits.size(); i++) {
		new_hub->exits[i] = clone_child(*new_hub->exits[i]);
	}
	for (size_t i = 0; i < new_hub->locked.size(); i++) {
		new_hub->locked[i] = clone_child(*new_hub->locked[i]);
	}
	return new_hub;
}


const std::vector<BasicItemInfo*>& Shed::children() const {
	return items;
}

void Shed::add_item(BasicItemInfo& item) {
	items.push_back(&item);
	item.set_parent(*this);
}

BasicItemInfo* Shed::clone(std::function<BasicItemInfo*(BasicItemInfo&)> clone_child) const {
	Shed* new_shed = new Shed(*this);
	for (size_t i = 0; i < new_shed->items.size(); i++) {
		new_shed->items[i] = clone_child(*new_shed->items[i]);
	}
	return new_shed;
}


Hub* Key::get_door() const {
	return door;
}

BasicItemInfo* Key::clone(std::function<BasicItemInfo*(BasicItemInfo&)> clone_child) const {
	return new Key(*this);
}


bool Lever::active() const {
	return pulled;
}

void Lever::pull() {
	if (target == nullptr || target->kind() != BasicItemKind::Hub) return;
	((Hub*)target)->modify_counter(pulled ? -modify : modify);
	pulled = !pulled;
}

BasicItemInfo* Lever::clone(std::function<BasicItemInfo*(BasicItemInfo&)> clone_child) const {
	return new Lever(*this);
}*/
