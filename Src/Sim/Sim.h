#ifndef CRAB_SIM_H
#define CRAB_SIM_H

#include "Visible.h"
#include "../Util/Rando.h"
#include "Knowledge/Causal.h"

class Sim : public Visible {
public:
	Sim();

	const Vec<ItemId>& all_items() const override;
	const Vec<Tk>& item_attributes(ItemId item) const override;
	const Vec<Tk>& item_children(ItemId item) const override;
	Tk item_attribute(ItemId item, Tk key) const override;
	ItemId item_child(ItemId item, Tk key) const override;
	std::string item_name(ItemId item) const override;

	ItemId new_item();
	ItemId new_item(std::string name);
	ItemId get_item(const std::string& name) const override;
	void set_item_attribute(ItemId item, Tk attrib, Tk val);
	void set_item_child(ItemId item, Tk key, ItemId child);

	void enable_event(const MutCausal& causal);
	void disable_event(const MutCausal& causal);
	void perform_action(Action action);

private:
	Rando rando;

	Vec<ItemId> items;
	Map<ItemId, Vec<Tk>> attribute_keys;
	Map<ItemId, Vec<Tk>> child_keys;
	Map<Pair<ItemId, Tk>, Tk> attributes;
	Map<Pair<ItemId, Tk>, ItemId> children;

	Map<ItemId, std::string> item_names;
	Map<std::string, ItemId> item_name_map;

	Set<MutCausal> events;
};

#endif //CRAB_SIM_H
