#ifndef WHAT_LINALG_H
#define WHAT_LINALG_H

#include "TypeUtil.h"
#include "Log.h"

// Input is in conjunctive normal form. Each positive integer refers to a variable, and each negative integer
// refers to the negated form of that variable.
Vec<char> sat(Log& log, const Vec<Vec<int>>& prop, int num_terms = -1);

#endif //WHAT_LINALG_H
