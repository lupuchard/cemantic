#ifndef WHAT_STLUTIL_H
#define WHAT_STLUTIL_H

namespace std {
	template<typename T>
	void sort(T& collection) {
		std::sort(collection.begin(), collection.end());
	}

	template<typename T, typename U, typename V>
	void set_intersection(const T& input1, const U& input2, V& output) {
		std::set_intersection(input1.begin(), input1.end(), input2.begin(), input2.end(), std::back_inserter(output));
	}

	template<typename Vec, typename T, typename C>
	void push_heap(Vec& v, T elem, C comp) {
		v.push_back(std::move(elem));
		std::push_heap(v.begin(), v.end(), comp);
	}

	template<typename Vec, typename C>
	auto pop_heap(Vec& v, C comp) {
		std::pop_heap(v.begin(), v.end(), comp);
		auto result = std::move(v.back());
		v.pop_back();
		return result;
	}

	template<typename R, typename Vec>
	auto move_transform(Vec&& v) {
		std::vector<R> output;
		output.reserve(v.size());
		for (auto& elem : v) {
			output.push_back(R(std::move(elem)));
		}
		return output;
	}

	template<typename T>
	void insert(Vec<T>& a, const Vec<T>& b) {
		a.insert(a.end(), b.begin(), b.end());
	}
}

#endif //WHAT_STLUTIL_H
