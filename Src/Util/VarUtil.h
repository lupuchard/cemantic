#ifndef WHAT_VARUTIL_H
#define WHAT_VARUTIL_H

#include "Knowledge/ItemId.h"
#include "TypeUtil.h"
#include "Visible.h"
#include "Knowledge/Hash.h"
#include <iostream>

// Variables should start at 1 and be ascending.
inline bool validate_vars(Vec<ItemId>& items) {
	uint32_t cur_var = 0;
	for (ItemId item : items) {
		if (item.val < NUM_RESERVED_ITEM_IDS) {
			if (item.val > cur_var + 1) {
				return false;
			} else if (item.val == cur_var + 1) {
				cur_var++;
			}
		}
	}
	return true;
}

inline uint32_t get_num_variables_in_list(const Vec<ItemId>& items) {
	uint32_t num_variables = 0;
	for (ItemId item : items) {
		if (item.is_var()) {
			num_variables = std::max(num_variables, item.val);
		}
	}
	assert(num_variables < NUM_RESERVED_ITEM_IDS);
	return num_variables;
}

template<typename T>
uint32_t get_num_variables(const T& t) {
	return get_num_variables_in_list(t.get_all_items());
}

class Assignment {
public:
	Assignment(): num_values(0) { }
	Assignment(size_t num_values): values(new ItemId[num_values + 1]), num_values(num_values + 1) {
		for (size_t i = 0; i < num_values; i++) {
			values[i] = ItemId();
		}
	}

	Assignment(Assignment const& other): values(new ItemId[other.num_values]), num_values(other.num_values) {
		for (size_t i = 0; i < num_values; i++) {
			values[i] = other.values[i];
		}
	}

	Assignment& operator=(Assignment const& other) {
		if (num_values != other.num_values) {
			values.reset(new ItemId(other.num_values));
		}
		num_values = other.num_values;
		for (size_t i = 0; i < num_values; i++) {
			values[i] = other.values[i];
		}
		return *this;
	}

	static Assignment from(const Vec<ItemId>& items, const Vec<ItemId>& raw_items) {
		Assignment assignment(get_num_variables_in_list(items));
		for (size_t i = 0; i < items.size(); i++) {
			if (items[i].is_var()) {
				assignment[items[i]] = raw_items[i];
			} else if (items[i] != raw_items[i]) {
				return Assignment(); // invalid
			}
		}
		return assignment;
	}

	template<typename T>
	static Assignment from(const T& generic_t, const T& raw_t) {
		Vec<ItemId> items, raw_items;
		generic_t.get_all_items(items);
		raw_t.get_all_items(raw_items);
		return from(items, raw_items);
	}

	size_t length() const {
		return num_values;
	}

	ItemId& operator[](ItemId index) {
		assert(index.val < NUM_RESERVED_ITEM_IDS);
		return values[index.val];
	}

	const ItemId& operator[](ItemId index) const {
		assert(index.val < NUM_RESERVED_ITEM_IDS);
		return values[index.val];
	}

	bool valid() const {
		return num_values > 0;
	}

	ItemId fill(ItemId item) const {
		return (item.val < NUM_RESERVED_ITEM_IDS) ? values[item.val] : item;
	}

	Vec<Map<ItemId, ItemId>> invert() const {
		Vec<Map<ItemId, ItemId>> inversion;
		inversion.emplace_back();
		for (size_t i = 1; i < num_values; i++) {
			if (inversion.back().find(values[i]) != inversion.back().end()) {
				assert(false); // TODO: if multiple variables refer to same value, there are multiple inversions
			}

			inversion.back()[values[i]] = ItemId(i);
		}
		return inversion;
	}

	bool operator==(const Assignment& rhs) const {
		if (num_values != rhs.num_values) return false;
		for (size_t i = 0; i < num_values; i++) {
			if (values[i] != rhs.values[i]) return false;
		}
		return true;
	}

private:
	Box<ItemId[]> values;
	size_t num_values;
};

namespace std {
	template<>
	struct hash<Assignment> {
		size_t operator()(const Assignment& a) const {
			if (a.length() < 2) return a.length();
			size_t val = a[ItemId(1)].val;
			for (size_t i = 1; i < a.length(); i++) {
				val = combine_hash(val, a[ItemId(i)].val);
			}
			return val;
		}
	};
}

// Here we recursively iterate through every item in T, applying every possible choice of how to assign
// or not assign the current indices item to a variable.
template<typename T>
void get_generic_forms(T t, Vec<T>& generic_forms, const Vec<ItemId>& raw_items, size_t idx,
					   uint32_t next_unique_var, uint32_t next_expected_var, uint32_t max_var) {

	Vec<ItemId> cur_items;
	t.get_all_items(cur_items);

	// Skip variables, but maintain that next_expected_var is one greater than the highest var encountered so far.
	while (idx < raw_items.size() && raw_items[idx].is_var()) {
		assert(raw_items[idx].val <= next_expected_var);
		if (raw_items[idx].val == next_expected_var) {
			next_expected_var++;
		}
		++idx;
	}

	// Check if finished with this form.
	if (idx == raw_items.size()) {
		generic_forms.push_back(std::move(t));
		return;
	}

	// When assigning a variable to the item, it can either be a new unique variable or
	// match the variable assignment of previous instances of the same item in T.
	for (size_t i = 0; i <= idx; i++) {
		if (!(i == idx || (raw_items[i] == raw_items[idx] && cur_items[i].is_var()))) {
			continue;
		}

		T t_copy = t;

		if (i == idx) {
			// New unique variable
			if (next_unique_var != next_expected_var) {
				// The first occurrence of variables in T should remain in ascending order, which next_expected_var tracks.
				// To maintain order, we will swap next_unique_var with next_expected_var.
				for (size_t j = idx; j < raw_items.size(); j++) {
					if (raw_items[j].val == next_expected_var) {
						t_copy.set_item(j, ItemId(next_unique_var));
					}
				}
				t_copy.set_item(i, ItemId(next_expected_var));
			} else {
				t_copy.set_item(i, ItemId(next_unique_var));
			}

			if (next_unique_var < max_var) {
				get_generic_forms(std::move(t_copy), generic_forms, raw_items, idx + 1, next_unique_var + 1, next_expected_var + 1, max_var);
			}
		} else {
			// Match previous assignment
			t_copy.set_item(idx, cur_items[i]);
			get_generic_forms(std::move(t_copy), generic_forms, raw_items, idx + 1, next_unique_var, next_expected_var, max_var);
		}
	}

	// Final case, item remains not a variable.
	get_generic_forms(std::move(t), generic_forms, raw_items, idx + 1, next_unique_var, next_expected_var, max_var);
}

template<typename T>
Vec<T> get_generic_forms(const T& t, uint32_t max_var = NUM_RESERVED_ITEM_IDS - 1) {
	T t_copy = t;
	Vec<ItemId> items;
	t_copy.get_all_items(items);
	if (items.empty()) return Vec<T> { t };

	Vec<T> generic_forms;
	get_generic_forms<T>(std::move(t_copy), generic_forms, items, 0, get_num_variables(t) + 1, 1, max_var);
	return generic_forms;
}

// TODO: small map, consider non-hash table?
template<typename T>
Vec<Pair<T, Assignment>> get_generic_forms_with_assignments(const T& t) {
	Vec<T> forms = get_generic_forms(t);
	Vec<ItemId> raw_items = t.get_all_items();

	Vec<Pair<T, Assignment>> out;
	for (T& form : forms) {
		out.emplace_back(form, Assignment::from(form.get_all_items(), raw_items));
	}
	return out;
}

template<typename T>
void add_var(const T& t, Vec<T>& forms, const Vec<ItemId>& items, ItemId var, size_t idx) {
	while (idx < items.size() && items[idx].is_var()) {
		idx++;
	}

	if (idx >= items.size()) {
		forms.push_back(t);
		return;
	}

	T t_copy = t;
	t_copy.set_item(idx, var);
	add_var(t_copy, forms, items, var, idx + 1);
	add_var(t, forms, items, var, idx + 1);
}

template<typename T>
Vec<T> add_var(const T& t, ItemId var) {
	Vec<ItemId> items;
	t.get_all_items(items);
	assert(validate_vars(items));
	Vec<T> forms;
	add_var(t, forms, items, var, 0);
	forms.pop_back(); // removes the only form that did not actually add the variable
	return forms;
}

// All variables in 'derived' will be variables in 'base' as well
/*template<typename T, typename U>
Vec<Pair<T, U>> get_generic_forms_pairs(const T& base, const U& derived) {
	Vec<T> generic_bases = get_generic_forms(base);
	Vec<Vec<U>> generic_derived = { { derived } };
	Vec<Pair<T, U>> generic_pairs;
	for (T& t : generic_bases) {
		int num_vars = get_num_variables(generic_bases);
		while (num_vars >= generic_derived.size()) {
			generic_derived.push_back(get_generic_forms(derived, num_vars));
		}
	}
}*/

template<typename T>
void assign_from(T& to_assign, ItemId var, ItemId assignment) {
	if (!var.is_var()) return;

	Vec<ItemId> items_to_assign = to_assign.get_all_items();
	for (size_t j = 0; j < items_to_assign.size(); j++) {
		if (items_to_assign[j] == var) {
			to_assign.set_item(j, assignment);
		}
	}
}

template<typename T, typename U>
void assign_from(T& to_assign, const U& with_vars, const U& with_assignments) {
	Vec<ItemId> vars = with_vars.get_all_items();
	Vec<ItemId> assignments = with_assignments.get_all_items();
	Vec<ItemId> items_to_assign = to_assign.get_all_items();
	for (size_t i = 0; i < vars.size(); i++) {
		assign_from(to_assign, vars[i], assignments[i]);
	}
}

template<typename M, typename F>
void replace_keys(M& map, F replace_func) {
	M replacement_map;
	for (auto iter = map.begin(); iter != map.end(); ) {
		auto replacement = replace_func(iter->first);
		if (replacement) {
			replacement_map[*replacement] = iter->second;
			iter = map.erase(iter);
		} else {
			++iter;
		}
	}
	for (auto& key_val : replacement_map) {
		map[key_val.first] = key_val.second;
	}
}

template<typename T, typename H>
class InfoWrapper {
public:
	InfoWrapper(T t, float confidence = 1):
			num_variables(get_num_variables(t)), confidence(confidence), hash(t.get_hash()), t(std::move(t)) { }

	template<class... Args>
	static Box<InfoWrapper<T, H>> box(Args&&... args) {
		return std::make_unique<InfoWrapper<T, H>>(T(args...));
	}

	template<class... Args>
	static InfoWrapper<T, H> make(Args&&... args) {
		return InfoWrapper<T, H>(T(args...));
	}

	operator const T&() const { return t; }
	const T& v() const { return t; }

	inline bool same(const InfoWrapper<T, H>& other) const {
		if (get_hash() != other.get_hash()) {
			return false;
		}

		return t == other.t;
	}

	H get_hash() const {
		return hash;
	}

	uint32_t get_num_vars() const {
		return num_variables;
	}

	inline float get_confidence() const {
		return confidence;
	}
	inline void boost_confidence(float new_confidence) const {
		confidence = 1 - ((1 - confidence) * (1 - new_confidence));
	}
	inline void set_confidence(float new_confidence) const {
		confidence = new_confidence;
	}

	std::partial_ordering operator<=>(const InfoWrapper<T, H>& other) const {
		if (v() == other.v()) {
			return confidence <=> other.confidence;
		} else {
			return v() <=> other.v();
		}
	}

	template<class... Args>
	std::string to_string(const Visible& visible, Args&&... args) const {
		if (confidence == 1) {
			return t.to_string(visible, args...);
		} else {
			return "[" + std::to_string((int)(confidence * 100)) + "%] " + t.to_string(visible, args...);
		}
	}

private:
	uint32_t num_variables = 0;
	mutable float confidence;
	H hash;
	T t;
};

#endif //WHAT_VARUTIL_H
