#ifndef PROJECT_STRUTIL_H
#define PROJECT_STRUTIL_H

#include <fmt/core.h>
#include <algorithm>
#include <cctype>
#include <locale>
using namespace std::string_literals;

//struct ItemId;
enum class Tk : int16_t;
//std::string to_string(const ItemId& item);

// trim from start (in place)
static inline void ltrim(std::string &s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
		return !std::isspace(ch);
	}));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
		return !std::isspace(ch);
	}).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
	ltrim(s);
	rtrim(s);
}

static inline std::string float_to_str(float f) {
	std::string str = std::to_string(f);
	str.erase(str.find_last_not_of('0') + 1, std::string::npos);

	if (str[str.size() - 1] == '.') {
		str.erase(str.size() - 1);
	}

	return str;
}

using std::to_string;

template<typename X, typename Y>
std::string to_string(const std::pair<X, Y>& pair) {
	return "(" + to_string(pair.first) + ", " + to_string(pair.second) + ")";
}

template<typename T>
auto _to_string(const T& val, int) -> decltype(val.item_name()) {
	return val.item_name();
}

template<typename T>
auto _to_string(const T& container, long) -> decltype(to_string(*container.begin())) {
	std::string str = "{ ";
	bool first = true;
	for (auto iter = container.begin(); iter != container.end(); ++iter) {
		if (first) {
			first = false;
		} else {
			str += ", ";
		}
		str += to_string(*iter);
	}
	return str + " }";
}

template<typename T>
std::string to_string(const T& val) {
	return _to_string(val, 0);
}

/*template<>
struct fmt::formatter<ItemId>: formatter<string_view> {
	template<typename FormatContext>
	auto format(const ItemId& item, FormatContext& ctx) {
		return fmt::format_to(ctx.out(), to_string(item));
	}
};*/

template<>
struct fmt::formatter<Tk>: formatter<string_view> {
	template<typename FormatContext>
	auto format(const Tk& tk, FormatContext& ctx) {
		return fmt::format_to(ctx.out(), to_string(tk));
	}
};



template<typename T>
std::string vec_to_string(const std::vector<T>& vec) {
	std::string str = "{ ";
	bool first = true;
	for (auto& elem : vec) {
		if (!first) str += ", ";
		first = false;
		str += elem.item_name();
	}
	return str + " }";
}

#endif //PROJECT_STRUTIL_H
