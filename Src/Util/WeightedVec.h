#ifndef WHAT_WEIGHTEDVEC_H
#define WHAT_WEIGHTEDVEC_H

#include "BoxUtil.h"
#include <cstring>

template<typename T>
class WeightedVec {
public:
	WeightedVec() = default;

	WeightedVec(T first_val, float weight = 1): data(new T[1]), weights(new float[1]), length(1), capacity(1) {
		data[0] = std::move(first_val);
		weights[0] = weight;
	}

	WeightedVec(WeightedVec const& other): length(other.length), capacity(other.length) {
		if (length > 0) {
			data.reset(new T[capacity]);
			weights.reset(new float[capacity]);

			for (uint32_t i = 0; i < length; i++) {
				data[i] = other.data[i];
			}
			std::memcpy(weights.get(), other.weights.get(), sizeof(float) * length);
		}
	}

	WeightedVec& operator=(WeightedVec const& other) {
		if (capacity < other.capacity) {
			capacity = other.capacity;
			data.reset(new T[capacity]);
			weights.reset(new T[capacity]);
		}

		length = other.length;
		if (length > 0) {
			for (uint32_t i = 0; i < length; i++) {
				data[i] = other.data[i];
			}
			std::memcpy(weights.get(), other.weights.get(), sizeof(float) * length);
		}
	}

	T& operator[](uint32_t index) {
		assert(index < length);
		return data[index];
	}

	const T& operator[](uint32_t index) const {
		assert(index < length);
		return data[index];
	}

	float& weight(uint32_t index) {
		assert(index < length);
		return weights[index];
	}

	float weight(uint32_t index) const {
		assert(index < length);
		return weights[index];
	}

	uint32_t size() const {
		return length;
	}

	void resize(uint32_t new_size) {
		if (new_size > capacity) {
			T* new_data = new T[new_size];
			for (uint32_t i = 0; i < length; i++) {
				new_data[i] = std::move(data[i]);
			}
			for (uint32_t i = length; i < new_size; i++) {
				new_data[i] = T();
			}

			float* new_weights = new float[new_size];

			if (length > 0) {
				std::memcpy(new_weights, weights.get(), sizeof(float) * length);
				for (uint32_t i = length; i < new_size; i++) {
					new_weights[i] = 1;
				}
			}

			data.reset(new_data);
			weights.reset(new_weights);
			length = new_size;
			capacity = new_size;
		} else if (new_size > length) {
			for (uint32_t i = length; i < new_size; i++) {
				data[i] = T();
			}
			for (uint32_t i = length; i < new_size; i++) {
				weights[i] = 1;
			}
			length = new_size;
		} else {
			length = new_size;
		}
	}

	void reserve(uint32_t new_capacity) {
		if (new_capacity <= capacity) return;
		data.reset(new T[new_capacity]);
		weights.reset(new float[new_capacity]);
		capacity = new_capacity;
	}

	template<typename F>
	void filter(F func) {
		uint32_t index = 0;
		for (uint32_t i = 0; i < length; i++) {
			if (func(data[i], weights[i])) {
				if (index < i) {
					data[index] = std::move(data[i]);
					weights[index] = std::move(weights[i]);
				}
				index++;
			}
		}
		length = index;
	}

	void assign(uint32_t index, Pair<T, float> val) {
		assert(index < length);
		data[index] = std::move(val.first);
		weights[index] = std::move(val.second);
	}

	Pair<T*, float*> at(uint32_t index) {
		return std::make_pair(&data[index], &weights[index]);
	}

private:
	Box<T[]> data;
	Box<float[]> weights;
	uint32_t length = 0;
	uint32_t capacity = 0;
};


#endif //WHAT_WEIGHTEDVEC_H
