#ifndef WHAT_MATHUTIL_H
#define WHAT_MATHUTIL_H

#include <cassert>

// TODO: Choose better normalization and mean. They should relate to each other.
//       Also, val can't be negative now.
inline double bad_normalize(double val) {
	return (std::abs(val) < 1) ? val : (val > 0 ? (std::log(val) + 1) : (-std::log(-val) - 1));
}
inline double bad_mean(double val1, double val2) {
	// If the values are different signs, we use the arithmetic mean.
	// Otherwise, we use the geometric mean.
	return ((val1 > 0) != (val2 > 0)) ? (val1 + val2) / 2 : (std::sqrt(val1 * val2) * (val1 < 0 ? -1 : 1));
}

inline int choose2(int n) {
	return n * (n - 1) / 2;
}

inline int factorial(int n) {
	return n > 1 ? n * factorial(n - 1) : 1;
}

inline bool discrete(float f) {
	assert(f >= 0 && f <= 1);
	if (f < 0.01) {
		return false;
	} else {
		assert(f > 0.99);
		return true;
	}
}

enum class AltFloatEnum {
	NONE,
	ANY,
};

class AltFloat {
public:
	AltFloat(): val(0) { }
	AltFloat(float new_val) {
		if (std::isnan(new_val)) {
			assert(false);
			data = 0xFFFFFFFF;
		} else {
			val = new_val;
		}
	}
	AltFloat(AltFloatEnum alt) {
		set_alt((uint16_t)alt);
	}

	inline AltFloat operator=(float new_val) {
		*this = AltFloat(new_val);
		return *this;
	}

	inline operator float() const {
		assert(is_float());
		return val;
	}

	inline bool operator==(AltFloat other) const {
		return data == other.data;
	}

	inline bool operator!=(AltFloat other) const {
		return !operator==(other);
	}

	inline bool is_float() const {
		return !(std::isnan(val) && data != 0xFFFFFFFF);
	}

	inline uint16_t alt() const {
		assert(!is_float());
		return (uint16_t)data;
	}

	inline void set_alt(uint16_t alt) {
		data = 0xFFF00000 | (uint32_t)alt;
	}

private:
	union {
		float val;
		uint32_t data;
	};
};

#endif //WHAT_MATHUTIL_H
