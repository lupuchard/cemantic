#include "Linalg.h"
#include "Log.h"
#include <fstream>
#include <cstring>
//#include <cstdio>

std::string exec(const char* cmd) {
	std::array<char, 128> buffer;
	std::string result;
	std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
	if (!pipe) {
		return "";
	}
	while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
		result += buffer.data();
	}
	return result;
}

Vec<char> sat(Log& log, const Vec<Vec<int>>& prop, int num_terms) {
	if (num_terms == -1) {
		for (auto& clause : prop) {
			for (int term : clause) {
				num_terms = std::max(num_terms, term);
			}
		}
	}

	{
		std::ofstream sat_input("sat_input.cnf");
		sat_input << "p cnf " << num_terms << " " << prop.size() << "\n";
		for (auto& clause : prop) {
			for (int term : clause) {
				sat_input << term << " ";
			}
			sat_input << "0\n";
		}
	}

	std::string output = exec("./kissat sat_input.cnf -q");

	if (output == "") {
		log.error("Failed to launch kissat: " + std::string(std::strerror(errno)));
		return Vec<char>();
	}

	if (output.size() < 3 || output[0] != 's') {
		log.error("Unexpected kissat output: '" + output + "'");
		return Vec<char>();
	}

	std::cout << "Output: " << output << std::endl;

	if (output[2] == 'U') {
		// Unsatisfiable
		return Vec<char>();
	}

	size_t pos = output.find('\n');
	if (pos == std::string::npos) {
		log.error("Unexpected kissat output: '" + output + "'");
		return Vec<char>();
	}
	std::string assignment_str = output.substr(pos + 1);
	std::cout << "Assignment: " << assignment_str << std::endl;

	Vec<char> assignments;
	size_t idx = 2;
	while (assignment_str[idx] != '0') {
		assignments.push_back(assignment_str[idx] != '-');
		idx = assignment_str.find_first_of(' ', idx);
		if (idx == std::string::npos) {
			log.error("Unexpected kissat output: '" + assignment_str + "'");
			return Vec<char>();
		}
		idx++;
	}

	return assignments;
}
