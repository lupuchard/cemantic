#ifndef WHAT_BOXUTIL_H
#define WHAT_BOXUTIL_H

template<typename T>
using Box = std::unique_ptr<T>;

template<typename T>
class CpBox {
public:
	CpBox() = default;
	CpBox(T* data) noexcept: box(data) { }
	CpBox(nullptr_t) noexcept: box(nullptr) { }

	CpBox(CpBox const& other) {
		if (other != nullptr) box.reset(new T(*other));
	}

	CpBox& operator=(CpBox const& other) {
		if (other.box == nullptr) {
			box = nullptr;
		} else {
			box.reset(new T(*other));
		}

		return *this;
	}

	CpBox(CpBox<T>&& other) noexcept {
		box = std::move(other.box);
	}

	CpBox& operator=(CpBox<T>&& other) noexcept {
		box = std::move(other.box);
		return *this;
	}

	CpBox(Box<T>&& other) noexcept {
		box = std::move(other);
	}

	CpBox& operator=(Box<T>&& other) noexcept {
		box = std::move(other);
		return *this;
	}

	typename Box<T>::pointer get() const { return box.get(); }
	void release() { return box.release(); }
	void reset(T* val = T()) { return box.reset(val); }
	void swap(CpBox& other) noexcept { return box.swap(other.box); }
	T* operator->() const { return box.operator->(); }
	T& operator*() const  { return *box; }
	T& operator[](size_t i) const { return box[i]; }

	bool operator==(CpBox<T>& other) const { return box == other.box; }
	bool operator!=(CpBox<T>& other) const { return box != other.box; }
	bool operator==(nullptr_t) const { return box == nullptr; }
	bool operator!=(nullptr_t) const { return box != nullptr; }

private:
	Box<T> box;
};

template<typename T>
class MaybeBox {
public:
	MaybeBox(): data(nullptr), managed(false) { }
	explicit MaybeBox(T* data): data(data), managed(false) { }
	explicit MaybeBox(Box<T> data): data(data.release()), managed(true) { }
	MaybeBox(MaybeBox const&) = delete;
	MaybeBox& operator=(MaybeBox const&) = delete;

	MaybeBox(MaybeBox&& other) noexcept {
		data = other.data;
		managed = other.managed;
		other.data = nullptr;
		other.managed = false;
	}

	MaybeBox& operator=(MaybeBox&& other) noexcept {
		if (managed) delete data;
		data = other.data;
		managed = other.managed;
		other.data = nullptr;
		other.managed = false;
		return *this;
	}

	~MaybeBox() {
		if (managed) delete data;
	}

	MaybeBox ref() const {
		return MaybeBox(data);
	}

	MaybeBox copy() const {
		return MaybeBox(std::make_unique<T>(*data));
	}

	T* operator->() const {
		return data;
	}
	T& operator*() const {
		return *data;
	}

	T* get() const {
		return data;
	}
	bool is_managed() const {
		return managed;
	}

private:
	T* data;
	bool managed;
};


#endif //WHAT_BOXUTIL_H
