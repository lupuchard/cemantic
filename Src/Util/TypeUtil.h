#ifndef WHAT_TYPE_UTIL_H
#define WHAT_TYPE_UTIL_H

#include <memory>
#include <vector>
#include "hopscotch_map.h"
#include "hopscotch_set.h"
#include "BoxUtil.h"

template<typename T, typename U>
using Pair = std::pair<T, U>;

template<typename T>
using Vec = std::vector<T>;

template<typename K, typename V>
using Map = tsl::hopscotch_map<K, V>;

template<typename V>
using Set = tsl::hopscotch_set<V>;

template<typename V>
struct ValueHash {
	size_t operator()(const V& value) const {
		return (size_t)value.get_hash();
	}
};

template<typename V>
struct ValueSame {
	bool operator()(const V& lhs, const V& rhs) const {
		return lhs.same(rhs);
	}
};

template<typename P>
struct PointerHash {
	typedef typename std::remove_reference<decltype(*std::declval<P>())>::type V;
	size_t operator()(const P& pointer) const {
		return (size_t)pointer->get_hash();
	}
	size_t operator()(const V& value) const {
		return (size_t)value.get_hash();
	}
};

template<typename P>
struct PointerSame {
	using is_transparent = void;
	typedef typename std::remove_reference<decltype(*std::declval<P>())>::type V;
	bool operator()(const P& lhs, const P& rhs) const {
		return lhs->same(*rhs);
	}
	bool operator()(const V& lhs, const P& rhs) const {
		return lhs.same(*rhs);
	}
	bool operator()(const P& lhs, const V& rhs) const {
		return lhs->same(rhs);
	}
};

template<typename P>
struct PointerEqual {
	using is_transparent = void;
	typedef typename std::remove_reference<decltype(*std::declval<P>())>::type V;
	bool operator()(const P& lhs, const P& rhs) const {
		return (*lhs) == (*rhs);
	}
	bool operator()(const V& lhs, const P& rhs) const {
		return lhs == (*rhs);
	}
	bool operator()(const P& lhs, const V& rhs) const {
		return (*lhs) == rhs;
	}
};

template<typename P1, typename P2>
struct PairSame {
	using is_transparent = void;
	typedef typename std::remove_reference<decltype(*std::declval<P1>())>::type V1;
	typedef typename std::remove_reference<decltype(*std::declval<P2>())>::type V2;

	template<typename T>
	bool same(const T& lhs, const T& rhs, int) {
		PointerSame<T> p;
		return p(lhs, rhs);
	}

	template<typename T>
	bool same(const T& lhs, const T& rhs, long) {
		PointerEqual<T> p;
		return p(lhs, rhs);
	}

	bool operator()(const Pair<P1, P2>& lhs, const Pair<P1, P2>& rhs) const {
		return same(lhs.first, rhs.first) && same(lhs.second, rhs.second);
	}
};

template<typename P1, typename P2>
struct PairHash {
	size_t operator()(const Pair<P1, P2>& pair) const {
		PointerHash<P1> hash1;
		PointerHash<P2> hash2;
		return combine_hash(hash1(pair.first), hash2(pair.second));
	}
};

template<typename T>
const T& maybe_deref(const Box<T>& t) { return *t; }
template<typename T>
T& maybe_deref(Box<T>& t) { return *t; }
template<typename T>
T& maybe_deref(T& t) { return t; }
template<typename T>
T& maybe_deref(T* t) { return *t; }

template<typename... Base>
struct Visitor: Base... {
	using Base::operator()...;
};
template<typename... T> Visitor(T...) -> Visitor<T...>;

#endif //WHAT_TYPE_UTIL_H
