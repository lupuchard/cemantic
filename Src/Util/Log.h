#ifndef WHAT_LOG_H
#define WHAT_LOG_H

#include <fmt/core.h>
#include <iostream>
#include <vector>

class Log {
public:
	Log(bool print = false): print(print) { }

	inline void info(std::string str) {
		if (print) std::cout << str << std::endl;
		log.push_back(str + '\n');
	}

	template<typename ...T>
	void info(const std::string& str, const T&... args) {
		info(fmt::format(str, args...));
	}

	inline void error(std::string str) {
		info("ERROR: " + str);
	}

	template<typename ...T>
	void error(const std::string& str, const T&... args) {
		info("ERROR: " + fmt::format(str, args...));
	}

	inline const std::vector<std::string>& get() const {
		return log;
	}

private:
	bool print = false;
	std::vector<std::string> log;
};

#endif //WHAT_LOG_H
