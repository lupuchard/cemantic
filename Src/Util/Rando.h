#ifndef CRAB_RANDO_H
#define CRAB_RANDO_H

#include <limits>
#include <cstdint>
#include <vector>

class Rando {
public:
	Rando(uint64_t seed) {
		for (int i = 0; i < 4; i++) {
			uint64_t z = (seed += 0x9e3779b97f4a7c15);
			z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
			z = (z ^ (z >> 27)) * 0x94d049bb133111eb;
			s[i] = z ^ (z >> 31);
		}
	}

	static inline uint64_t rotl(uint64_t x, int k) {
		return (x << k) | (x >> (64 - k));
	}

	inline uint64_t rand() {
		const uint64_t result_starstar = rotl(s[1] * 5, 7) * 9;

		const uint64_t t = s[1] << 17;

		s[2] ^= s[0];
		s[3] ^= s[1];
		s[1] ^= s[2];
		s[0] ^= s[3];

		s[2] ^= t;

		s[3] = rotl(s[3], 45);

		return result_starstar;
	}

	inline int64_t rand(int64_t min, int64_t max) { // [min, max)
		return rand() % (max - min) + min;
	}

	inline double rand_float() {
		return (double)rand() / max();
	}

	inline uint64_t min() {
		return 0;
	}

	inline uint64_t max() {
		return std::numeric_limits<uint64_t>::max();
	}

	inline uint64_t operator()() {
		return rand();
	}

	template<typename T>
	T select(const std::vector<T>& vals) {
		return vals[rand(0, vals.size())];
	}

	template<typename T>
	size_t weighted_random_select(const T& weights) {
		assert(!weights.empty());

		float cumulative = 0;
		float rand_val = rand_float();
		for (size_t i = 0; i < weights.size() - 1; i++) {
			cumulative += weights[i];
			if (rand_val < cumulative) {
				return i;
			}
		}

		return weights.size() - 1;
	}

	template<typename T>
	void shuffle(T& list) {
		for (int i = list.size() - 1; i > 0; i--) {
			std::swap(list[i], list[rand(0, i)]);
		}
	}

private:
	uint64_t s[4];
};

#endif //CRAB_RANDO_H
