#ifndef WHAT_SDL2DISPLAY_H
#define WHAT_SDL2DISPLAY_H

#include "ImGuiManager.h"

const int FPS = 10;

class SDL2Display {
public:
	static int show(ImGuiManager& imgui);
};


#endif //WHAT_SDL2DISPLAY_H
