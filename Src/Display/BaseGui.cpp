#include "BaseGui.h"
#include <imgui.h>

void BaseGui::render_content() {
	if (shown(Panel::Log)) show_log_panel();
	if (shown(Panel::Knowledge)) show_knowledge_panel();
	if (shown(Panel::Sim)) show_sim_panel();
	if (shown(Panel::Demo)) ImGui::ShowDemoWindow();
}

void BaseGui::render_menu_bar() {
	if (ImGui::BeginMenu("View")) {
		for (auto elem: panels_shown) {
			if (ImGui::MenuItem(PanelNames[(int)elem.first].c_str(), nullptr, elem.second)) { elem.second = !elem.second; }
		}
		ImGui::EndMenu();
	}
}

void BaseGui::show_log_panel() {
	ImGui::Begin("Log");
	auto& log_text = log.get();
	for (size_t i = std::max((int)log_text.size() - 100, 0); i < log_text.size(); i++) {
		ImGui::TextWrapped("%s", log_text[i].c_str());
	}

	if (ImGui::GetScrollY() >= ImGui::GetScrollMaxY()) {
		ImGui::SetScrollHereY(1.0f);
	}

	ImGui::End();
}

void BaseGui::show_knowledge_panel() {
	ImGui::Begin("Knowledge");

	for (auto& causal : sem.get_all_causals()) {
		ImGui::Text("%s", causal->to_string(visible).c_str());
	}

	for (ItemId item : visible.all_items()) {
		if (item.is_null()) {
			continue;
		}

		auto attributes = visible.item_attributes(item);
		auto children = visible.item_children(item);
		if (children.empty() && attributes.empty()) {
			continue;
		}

		if (ImGui::TreeNode(visible.item_name(item).c_str())) {
			ImGui::Indent(10);
			show_knowledge(item);
			ImGui::Unindent(10);
			ImGui::TreePop();
		}
	}

	ImGui::End();
}

void BaseGui::show_knowledge(ItemId item) {
	auto attributes = visible.item_attributes(item);
	auto children = visible.item_children(item);

	for (Tk key : attributes) {
		MutTrait trait(key, visible.item_attribute(item, key));
		ImGui::Text("%s", trait.to_string(visible, item).c_str());
	}

	for (Tk key : children) {
		MutTrait trait(key, visible.item_child(item, key));
		ImGui::Text("%s", trait.to_string(visible, item).c_str());
	}
}

void BaseGui::show_sim_panel() {
	ImGui::Begin("Sim");

	for (ItemId item : visible.all_items()) {
		if (!item.is_null() && visible.item_attribute(item, Tk::Kind) == Tk::Hub) {
			show_loc(item);
			break;
		}
	}

	ImGui::End();
}

void BaseGui::show_loc(ItemId item) {
	std::string name = visible.item_name(item);
	if (!visible.item_children(item).empty()) {
		if (visible.item_attribute(item, Tk::Kind) == Tk::Hub) {
			ImGui::SetNextTreeNodeOpen(true, ImGuiCond_Once);
		}

		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.75, 0.75, 1, 1));

		bool tree_open = ImGui::TreeNode(name.c_str());
		ImGui::PopStyleColor();
		if (tree_open) {
			for (Tk key : visible.item_children(item)) {
				show_loc(visible.item_child(item, key));
			}
			ImGui::TreePop();
		}
	} else {
		ImGui::Indent(10);
		ImGui::Text("%s", name.c_str());
		ImGui::Unindent(10);
	}
}
