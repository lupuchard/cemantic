#ifndef WHAT_CYTOMANAGER_H
#define WHAT_CYTOMANAGER_H

#include <Hierarchy.h>
#include <httplib.h>

class CytoManager {
public:
	CytoManager();
	void display_hierarchy(const Hierarchy& hierarchy);

private:
	void launch_cytoscape();

	httplib::Client cli;
	bool cytoscape_launched = false;
};


#endif //WHAT_CYTOMANAGER_H
