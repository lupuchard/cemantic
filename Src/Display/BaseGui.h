#ifndef WHAT_BASEGUI_H
#define WHAT_BASEGUI_H

#include "Log.h"
#include "EpisodicMemory.h"
#include "SemanticMemory.h"

enum class Panel {
	Demo,
	Log,
	Knowledge,
	Sim,
	Goals,
	Hierarchy,
	Explore,
	Memory,
	Queue,
};

const Vec<std::string> PanelNames = {
	"Demo",
	"Log",
	"Knowledge",
	"Sim",
	"Goals",
	"Hierarchy",
	"Explore",
	"Memory"
};

class BaseGui {
public:
	BaseGui(const SemanticMemory& sem, const Visible& visible, const Log& log):
		log(log), sem(sem), visible(visible) { }

	virtual void render_menu_bar();
	virtual void render_content();

	void show_knowledge(ItemId item);

	inline bool shown(Panel panel) { return panels_shown[panel]; }
	inline void show(Panel panel) { panels_shown[panel] = true; }

protected:
	const Log& log;
	const SemanticMemory& sem;
	const Visible& visible;

private:
	void show_log_panel();
	void show_knowledge_panel();

	void show_sim_panel();
	void show_loc(ItemId item);

	std::map<Panel, bool> panels_shown;
};


#endif //WHAT_BASEGUI_H
