#include "SimulationGui.h"
#include "CytoManager.h"
#include <imgui.h>

void SimulationGui::render_content() {
	BaseGui::render_content();
	if (shown(Panel::Goals)) show_goal_panel();
	if (shown(Panel::Hierarchy)) show_hierarchy();
	if (shown(Panel::Explore)) show_action_choices();
}

void SimulationGui::render_menu_bar() {
	BaseGui::render_menu_bar();

	static const int step_amounts[7] = { 1, 2, 5, 10, 20, 50, 100 };
	static const char* step_texts[7] = { "1", "2", "5", "10", "20", "50", "100" };
	if (ImGui::Button("Step")) {
		for (int i = 0; i < step_amounts[step_amount_selected]; i++) {
			brain.execute_one();
			selected_cluster = nullptr;
			selected_cluster_item = -1;
		}
	}

	ImGui::SetNextItemWidth(50);
	ImGui::Combo("##StepAmount", &step_amount_selected, step_texts, 7);
}

void SimulationGui::show_goal_panel() {
	ImGui::Begin("Goals");
	show_goal(brain.get_base_goal());
	ImGui::End();
}

void SimulationGui::show_goal(const Goal& goal) {
	ImGui::SetNextTreeNodeOpen(true, ImGuiCond_Once);
	if (ImGui::TreeNode(goal.to_string(visible).c_str())) {
		for (auto& pair : goal.get_subgoals()) {
			show_goal(*pair.first);
		}
		ImGui::TreePop();
	}
}

void SimulationGui::show_hierarchy() {
	ImGui::Begin("Hierarchy");

	const Hierarchy* hierarchy = brain.get_knowledge().get_hierarchy();
	if (hierarchy != nullptr) {
		if (ImGui::Button("Show in Cytoscape")) {
			CytoManager cyto_manager;
			cyto_manager.display_hierarchy(*hierarchy);
		}

		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.9, 0.8, 0.8, 1));
		show_cluster(hierarchy->get_root(), 0);
		ImGui::PopStyleColor();

		if (selected_cluster != nullptr) {
			if (selected_cluster_item == -1) {
				show_knowledge(ItemId());
			} else {
				auto& pair = (*selected_cluster->subitems())[selected_cluster_item];
				ImGui::Text("Confidence: %d%%", (int)(pair.second * 100));
				show_knowledge(pair.first);
			}
		}
	} else {
		ImGui::Text("No hierarchy yet");
	}

	ImGui::End();
}

void SimulationGui::show_cluster(const Cluster& cluster, int id) {
	ImGui::SetNextTreeNodeOpen(true, ImGuiCond_Once);
	bool selected = (&cluster == selected_cluster && selected_cluster_item == -1);
	if (cluster.subitems() == nullptr) {
		auto& triple = *cluster.triple();
		std::string name = fmt::format("{}Cluster [{} children]##{}", cluster.parent == nullptr ? "Root " : "", (triple[2] == nullptr) ? 2 : 3, id);
		if (ImGui::TreeNodeEx(name.c_str(), selected ? ImGuiTreeNodeFlags_Selected : 0)) {
			if (ImGui::IsItemClicked(ImGuiMouseButton_Right)) {
				selected_cluster = &cluster;
				selected_cluster_item = -1;
			}

			for (size_t i = 0; i < 3; i++) {
				if (triple[i] != nullptr) {
					show_cluster(*triple[i], i);
				}
			}
			ImGui::TreePop();
		}
	} else {
		std::string name = fmt::format("Cluster [{} items]##{}", cluster.subitems()->size(), id);
		if (ImGui::TreeNodeEx(name.c_str(), selected ? ImGuiTreeNodeFlags_Selected : 0)) {
			if (ImGui::IsItemClicked(ImGuiMouseButton_Right)) {
				selected_cluster = &cluster;
				selected_cluster_item = -1;
			}

			for (size_t i = 0; i < cluster.subitems()->size(); i++) {
				show_cluster_item(cluster, i);
			}
			ImGui::TreePop();
		}
	}
}

void SimulationGui::show_cluster_item(const Cluster& cluster, int index) {
	auto& subitems = *cluster.subitems();
	if (subitems[index].second == 0) return;
	float conf = 0.5 + subitems[index].second / 2;
	ItemId item = subitems[index].first;
	switch (sim.item_attribute(item, Tk::Kind)) {
		case Tk::Null:
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1, 1, 1, conf));
			break;
		case Tk::Hub:
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.9, 0.7, 0.5, conf));
			break;
		case Tk::Shed:
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.7, 0.9, 0.5, conf));
			break;
		case Tk::Lever:
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.5, 0.7, 0.9, conf));
			break;
		default:
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.9, 0.5, 0.7, conf));
			break;
	}

	ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_Leaf;
	if (selected_cluster == &cluster && selected_cluster_item == index) {
		flags |= ImGuiTreeNodeFlags_Selected;
	}

	std::string item_name = visible.item_name(item);
	bool open = ImGui::TreeNodeEx(item_name.c_str(), flags);

	if (ImGui::IsItemClicked(ImGuiMouseButton_Left) || ImGui::IsItemClicked(ImGuiMouseButton_Right)) {
		selected_cluster = &cluster;
		selected_cluster_item = index;
	}

	ImGui::PopStyleColor();
	if (open) ImGui::TreePop();
}

void SimulationGui::show_action_choices() {
	ImGui::Begin("Action Selection");

	auto& choices = brain.were_action_choices();
	auto& weights = brain.were_action_weights();

	for (size_t i = 0; i < choices.size(); i++) {
		ImGui::Text("%.*f%% %s", 1, weights[i] * 100, choices[i].to_string(visible).c_str());
	}

	ImGui::End();
}
