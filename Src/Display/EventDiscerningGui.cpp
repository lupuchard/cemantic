#include "EventDiscerningGui.h"
#include <imgui.h>

void EventDiscerningGui::render_content() {
	BaseGui::render_content();
	if (shown(Panel::Memory)) show_mem_panel();
	if (shown(Panel::Queue)) show_queue(&discerner, "Queue");
}

void EventDiscerningGui::render_menu_bar() {
	BaseGui::render_menu_bar();

	if (ImGui::Button("Step")) {
		discerner.step();
	}
}

void EventDiscerningGui::show_mem_panel() {
	ImGui::Begin("Memory");

	for (size_t i = 0; i < mem.size(); i++) {
		const Moment& moment = mem.get(i);
		std::string name = fmt::format("{}:  {}", moment.index, moment.action.to_string(visible));
		auto diff = mem.get_diff(moment);
		Vec<Pair<ItemId, Tk>> changes = diff->get_changes();

		if (changes.empty()) {
			ImGui::Indent(21);
			ImGui::Text("%s", name.c_str());
			ImGui::Unindent(21);
		} else if (ImGui::TreeNodeEx(name.c_str())) {
			for (auto change : changes) {
				auto bef = diff->get_before(change).to_string(visible);
				auto aft = diff->get_after(change).to_string(visible);
				std::string item_name = visible.item_name(change.first);
				auto change_str = fmt::format("{}.{}: {} => {}", item_name, change.second, bef, aft);
				ImGui::Text("%s", change_str.c_str());

			}
			ImGui::TreePop();
		}
	}

	ImGui::End();
}

void EventDiscerningGui::show_queue(CausalClimber* climber, std::string climber_name) {
	ImGui::Begin(climber_name.c_str());

	for (EvCausal* ev : climber->get_queue()) {

		auto causal_name = ev->causal.to_string(visible);
		ImGui::Text(
			"%.2f [%d%%/%d%%] %s",
			ev->weight * 100,
			(int)(ev->get_effective_accuracy() * 100),
			(int)(ev->get_effective_coverage() * 100),
			causal_name.c_str()
		);

		bool tooltip = ev->condition_expanded || ev->variables_expanded || ev->subclimber_result || ev->subclimber;
		if (ImGui::IsItemHovered() && tooltip) {
			std::string done_str;
			if (ev->condition_expanded) done_str += "Condition expanded. ";
			if (ev->variables_expanded) done_str += "Variables expanded. ";
			if (ev->subclimber_result)  done_str += "Subclimb complete. ";
			else if (ev->subclimber)   done_str += "Has subclimber. ";

			ImGui::BeginTooltip();
			ImGui::TextUnformatted(done_str.c_str());
			ImGui::EndTooltip();
		}

		if (ev->subclimber) {
			show_queue(ev->subclimber.get(), ev->causal.to_string(visible));
		}
	}

	ImGui::End();
}
