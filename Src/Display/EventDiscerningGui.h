#ifndef WHAT_EVENTDISCERNINGGUI_H
#define WHAT_EVENTDISCERNINGGUI_H

#include "EventDiscerning/HillClimbing/CausalClimber.h"
#include "BaseGui.h"

class EventDiscerningGui : public BaseGui {
public:
	EventDiscerningGui(CausalClimber& discerner, const Log& log):
			BaseGui(discerner.get_sem(), discerner.get_visible(), log), discerner(discerner), mem(discerner.get_mem()) {
		show(Panel::Log);
		show(Panel::Knowledge);
		show(Panel::Sim);
		show(Panel::Memory);
		show(Panel::Queue);
	}

	void render_menu_bar() override;
	void render_content() override;

private:
	void show_mem_panel();
	void show_queue(CausalClimber* climber, std::string climber_name);

	CausalClimber& discerner;
	const EpisodicMemory& mem;
};


#endif //WHAT_EVENTDISCERNINGGUI_H
