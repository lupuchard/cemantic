#include "ImGuiManager.h"
#include "CytoManager.h"

ImGuiManager::ImGuiManager(Brain& brain, CausalClimber& climber, Log& log): sim_gui(brain, log), ed_gui(climber, log) { }

void ImGuiManager::initialize() {
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	io = &ImGui::GetIO();
	ImGui::StyleColorsDark();
}

void ImGuiManager::render() {
	// Start the Dear ImGui frame
	ImGui::NewFrame();

	if (ImGui::BeginMainMenuBar()) {
		if (ImGui::BeginMenu("Context")) {
			if (ImGui::MenuItem("Simulation", nullptr, cur_context == Context::Simulation)) {
				cur_context = Context::Simulation;
			}

			if (ImGui::MenuItem("Event Discerning", nullptr, cur_context == Context::EventDiscerning)) {
				cur_context = Context::EventDiscerning;
			}

			ImGui::EndMenu();
		}

		switch (cur_context) {
			case Context::Simulation: sim_gui.render_menu_bar(); break;
			case Context::EventDiscerning: ed_gui.render_menu_bar(); break;
		}

		ImGui::EndMainMenuBar();
	}

	switch (cur_context) {
		case Context::Simulation: sim_gui.render_content(); break;
		case Context::EventDiscerning: ed_gui.render_content(); break;
	}

	// Rendering
	ImGui::Render();
}

void ImGuiManager::cleanup() {
	ImGui::DestroyContext();
}

ImVec2 ImGuiManager::get_display_size() {
	assert(io != nullptr);
	return io->DisplaySize;
}
