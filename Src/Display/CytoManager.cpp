#include "CytoManager.h"
#include <Formats/GraphUtil.h>
#include <filesystem>

const int PORT = 1234;
const std::string filename = "hierarchy.graphml";
const std::string cytoscape_dir = "/home/luke/Cytoscape/";

CytoManager::CytoManager(): cli(("localhost:" + std::to_string(PORT)).c_str()) { }

void CytoManager::display_hierarchy(const Hierarchy& hierarchy) {
	GraphUtil graph_util;
	std::ofstream out(filename, std::ofstream::out | std::ofstream::trunc);
	graph_util.hierarchy_to_graphml(hierarchy, out, filename);
	out.close();

	std::string file_path = std::filesystem::current_path() / filename;
	cli.Post("/v1/commands/network/destroy", "{ network: \"" + filename + "\"}", "application/json");
	cli.Post("/v1/commands/network/import file", "{ file: \"" + file_path + "\"}", "application/json");
	cli.Post("/v1/commands/layout/apply preferred", "{ networkSelected: \"" + filename + "\"}", "application/json");
}

void CytoManager::launch_cytoscape() {
	if (cytoscape_launched) {
		return;
	}

	system((cytoscape_dir + "cytoscape.sh -R " + std::to_string(PORT) + " &").c_str());

	std::string session_filename = cytoscape_dir + "files/crab_hierarchy.cys";
	httplib::Params params{ { "body", "{ file: \"" + session_filename + "\"}" } };
	int failsafe = 0;
	while (true) {
		sleep(1);

		if (std::filesystem::exists(session_filename)) {
			auto result = cli.Post("/v1/commands/session/open", params);
			if (result.error() == httplib::Error::Success) {
				std::cout << "sucCUess" << std::endl;
				break;
			}
		} else {
			auto result = cli.Post("/v1/commands/session/new");
			if (result.error() == httplib::Error::Success) {
				std::cout << "success" << std::endl;
				cli.Post("/v1/commands/session/save as", params);
				break;
			}
		}

		failsafe++;
		if (failsafe > 10) {
			std::cout << "Failed to launch cytoscape" << std::endl;
			return;
		}
	}

	cytoscape_launched = true;
}
