#ifndef WHAT_IMGUIMANAGER_H
#define WHAT_IMGUIMANAGER_H

#include "Log.h"
#include "SimulationGui.h"
#include "EventDiscerningGui.h"
#include <imgui.h>

enum class Context {
	Simulation,
	EventDiscerning,
};

class ImGuiManager {
public:
	ImGuiManager(Brain& brain, CausalClimber& climber, Log& log);

	void initialize();
	void render();
	void cleanup();

	ImVec2 get_display_size();

private:
	ImGuiIO* io = nullptr;

	Context cur_context = Context::EventDiscerning;
	SimulationGui sim_gui;
	EventDiscerningGui ed_gui;
};


#endif //WHAT_IMGUIMANAGER_H
