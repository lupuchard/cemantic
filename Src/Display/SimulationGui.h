#ifndef WHAT_SIMULATIONGUI_H
#define WHAT_SIMULATIONGUI_H

#include "Brain.h"
#include "BaseGui.h"

class SimulationGui : public BaseGui {
public:
	SimulationGui(Brain& brain, Log& log):
			BaseGui(brain.get_knowledge(), brain.get_sim(), log),  brain(brain), sim(brain.get_sim()) {
		show(Panel::Log);
		show(Panel::Knowledge);
		show(Panel::Goals);
		show(Panel::Sim);
		show(Panel::Hierarchy);
		show(Panel::Explore);
	}

	void render_menu_bar() override;
	void render_content() override;

private:
	void show_goal_panel();
	void show_goal(const Goal& goal);

	void show_hierarchy();
	void show_cluster(const Cluster& cluster, int id);
	void show_cluster_item(const Cluster& cluster, int index);

	void show_action_choices();

	Brain& brain;
	const Sim& sim;

	int step_amount_selected = 0;

	const Cluster* selected_cluster = nullptr;
	int selected_cluster_item = -1;
};

#endif //WHAT_SIMULATIONGUI_H
