#ifndef CRAB_HASH_H
#define CRAB_HASH_H

#include <cstdint>
#include <string>
#include <unordered_set>

template<typename T, int I = 1>
struct Hash {
	uint64_t val;
	Hash() : val(0) {}
	Hash(uint64_t val) : val(val) {}
	inline Hash<T, I>& operator=(uint64_t val) {
		this->val = val;
		return *this;
	}
	explicit operator uint64_t() const { return val; }
	inline bool operator==(Hash<T, I> const& rhs) const { return val == rhs.val; }
	inline bool operator!=(Hash<T, I> const& rhs) const { return val != rhs.val; }
	inline bool operator<(Hash<T, I> const& rhs) const { return val < rhs.val; }
	inline bool operator>(Hash<T, I> const& rhs) const { return val > rhs.val; }
	inline bool operator<=(Hash<T, I> const& rhs) const { return val <= rhs.val; }
	inline bool operator>=(Hash<T, I> const& rhs) const { return val >= rhs.val; }
};

// Foo.Bar = Baz

// ???.Bar = Baz
/*template<typename T>
using TraitHash = Hash<T, 2>;

// ???.Bar = ???
template<typename T>
using TraitTypeHash = Hash<T, 3>;*/

template<typename T>
using ModHash = Hash<T, 4>;

template<typename T>
using MultiHash = Hash<T, 5>;

template<typename T>
using MultiTraitHash = Hash<T, 6>;

template<typename T>
struct MultiHasher {
	size_t operator()(const std::unordered_set<T>& set) const {
		bool first = true;
		uint64_t hash;
		for (const T& val : set) {
			if (first) {
				hash = chci(val);
			} else {
				hash = chc(hash, val);
			}
		}
		return hash;
	}
};

template<typename P, typename S>
struct MultiPointerHasher {
	size_t operator()(const S& set) const {
		bool first = true;
		uint64_t hash;
		for (const P& val : set) {
			if (first) {
				hash = chci(val->get_trait_hash());
			} else {
				hash = chc(hash, val->get_trait_hash());
			}
		}
		return hash;
	}
};

template<typename U, typename V>
inline uint64_t combine_hash(U hash1, V hash2) {
	uint64_t hash = (uint64_t)hash1 ^ ((uint64_t)hash2 + 0x9e3779b9 + ((uint64_t)hash1 << 6) + ((uint64_t)hash1 >> 2));
	return hash == 0 ? 1 : hash;
}

template<typename U>
inline constexpr uint64_t chci(U hash1) {
	return (uint64_t)hash1 + 65536;
}

template<typename U, typename V>
inline constexpr uint64_t chc(U hash1, V hash2) {
	return (uint64_t)hash1 * ((uint64_t)hash2 + 65536);
}

template<typename U, typename V>
inline constexpr uint64_t combine_hash_mix(U hash1, V hash2) {
	return (uint64_t)hash1 * ((uint64_t)hash2 + 65536);
}

constexpr uint64_t _tpi(int num) {
	return (uint64_t)num * 100000000000001;
}
static_assert(chc(chc(chc(chci(_tpi(12)), _tpi(14)), _tpi(16)), _tpi(18)) ==
              chc(chc(chc(chci(_tpi(14)), _tpi(18)), _tpi(12)), _tpi(16)), "Basic order test");

namespace std {
	template<typename T, int I>
	struct hash<Hash<T, I>> {
		size_t operator()(const Hash<T, I>& hash) const {
			return hash.val;
		}
	};

	template<typename T1, typename T2>
	struct hash<std::pair<T1, T2>> {
		size_t operator()(const std::pair<T1, T2>& pair) const {
			return combine_hash(std::hash<T1>()(pair.first), std::hash<T2>()(pair.second));
		}
	};
}



#endif //CRAB_HASH_H
