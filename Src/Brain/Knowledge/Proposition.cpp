/*#include "Proposition.h"
#include <set>
#include <algorithm>

// TODO: should be sorted
Proposition::Proposition(Vec<Vec<Boolean>> c): clauses(std::move(c)) {
	// Sort &  duplicate terms in clauses
	for (auto& clause : clauses) {
		std::sort(clause.begin(), clause.end());
		for (size_t i = 0; i < clause.size() - 1; i++) {
			if (clause[i].same_base(clause[i + 1])) {
				if (clause[i].is_negated() != clause[i + 1].is_negated()) {
					clause.clear();
					break;
				} else {
					clause.erase(clause.begin() + i + 1);
				}
			}
		}
	}

	// Remove duplicate clauses
	cleanup(clauses.size());

	total_terms = 0;
	for (auto& clause : clauses) {
		total_terms += clause.size();
	}
}

Proposition::Proposition(Boolean boolean): total_terms(1) {
	clauses.push_back(std::vector<Boolean> { std::move(boolean) });
}

bool boolean_eq(const Boolean& bool1, const Boolean& bool2) {
	return bool1.same_base(bool2);
}

bool clause_sorter(const std::vector<Boolean>& clause1, const std::vector<Boolean>& clause2) {
	for (size_t i = 0; i < std::min(clause1.size(), clause2.size()); i++) {
		auto hash1 = clause1[i].get_hash();
		auto hash2 = clause2[i].get_hash();
		if (hash1 < hash2) {
			return true;
		} else if (hash1 > hash2) {
			return false;
		}
	}
	return clause1.size() > clause2.size();
}

void Proposition::or_bool(Boolean b) {
	if (clauses.empty() && empty_result) return;
	hash = 0;

	// for OR, add a new clause for b
	clauses.emplace_back().push_back(b);
	total_terms++;
	if (clauses.size() == 1) return;

	// ...but then we need to do some cleanup
	cleanup(1);
}

void Proposition::and_bool(Boolean b) {
	if (clauses.empty() && !empty_result) return;
	hash = 0;

	if (clauses.empty()) {
		clauses.emplace_back().push_back(b);
		total_terms++;
		return;
	}

	// for AND, b gets added to every clause
	//auto b_hash = b.get_hash();
	for (size_t i = 0; i < clauses.size(); i++) {
		insert_into_clause(b, clauses[i]);
	}

	sort_clauses();
}

void Proposition::and_prop(Proposition& prop) {
	if (clauses.empty() || prop.clauses.empty()) {
		if ((clauses.empty() && empty_result) || (prop.clauses.empty() && !prop.empty_result)) {
			*this = prop;
		}
		return;
	}

	auto copy = prop.clauses.size() > 1 ? clauses : Vec<Vec<Boolean>>();
	for (size_t i = 0; i < prop.clauses.size(); i++) {
		if (i > 0) {
			clauses.insert(clauses.end(), copy.begin(), copy.end());
		}

		for (size_t j = 0; j < clauses.size(); j++) {
			auto& dest_clause = clauses[copy.size() * i + j];
			dest_clause.insert(dest_clause.end(), prop.clauses[i].begin(), prop.clauses[i].end()); // TODO: sorted insert
		}
	}

	cleanup((prop.clauses.size() - 1) * clauses.size());
}

bool Proposition::insert_into_clause(const Boolean& term, Vec<Boolean>& clause) {
	for (size_t j = 0; j < clause.size(); j++) {
		if (term.same_base(clause[j])) {
			// TODO: if two terms have the same item and key, but different values, should evaluate to false

			if (!term.is_negated() == clause[j].is_negated()) {
				// if same and inverted, delete the clause
				clause.clear();
				total_terms -= clause.size();
			}

			// if same and not inverted, do nothing
			return false;
		} else if (term < clause[j]) {
			total_terms++;
			clause.insert(clause.begin() + j, term);
			return true;
		}
	}

	total_terms++;
	clause.push_back(term);
	return true;
}

// TODO: this should somehow cancel out: (A and B) or !A or !B
// (A and B) or !A  ->  !A or B
// (A and B) or A   ->  A
// SHIT lots of simplification rules im missing....
bool Proposition::insert_clause(Vec<Boolean> clause) {
	for (size_t i = 0; i < clauses.size(); i++) {
		if (clauses[i] == clause) {
			return false;
		} else if (clause_sorter(clauses[i], clause)) {
			total_terms += clause.size();
			clauses.insert(clauses.begin() + i, clause);
			return true;
		}
	}

	total_terms += clause.size();
	clauses.push_back(clause);
	return true;
}

Proposition Proposition::get_negation() const {
	Proposition new_prop;
	for (const Vec<Boolean>& clause : clauses) {
		Vec<Boolean> negated_clause = clause;
		for (Boolean& term : negated_clause) {
			term.set_negated(!term.is_negated());
		}

		new_prop.and_disjunctive_clause(negated_clause);
	}

	return new_prop;
}

// TODO: also need to sort and remove duplicates
void Proposition::and_disjunctive_clause(Vec<Boolean> clause) {
	hash = 0;

	if (clauses.empty()) {
		for (Boolean term : clause) {
			clauses.push_back({term});
		}
	} else {
		auto copy = clause.size() > 1 ? clauses : Vec<Vec<Boolean>>();

		for (Vec<Boolean>& r : clauses) {
			insert_into_clause(clause[0], r);
		}

		for (size_t i = 1; i < clause.size(); i++) {
			for (size_t j = 0; j < copy.size(); j++) {
				clauses.push_back(copy[j]);
				insert_into_clause(clause[i], clauses.back());
			}
		}
	}

	sort_clauses();
}

uint64_t Proposition::get_hash() const {
	if (hash) return hash;

	if (clauses.empty()) {
		hash = (uint64_t)empty_result + 1;
	} else {
		hash = clauses.size();
		for (auto& clause : clauses) {
			uint64_t clause_hash = clause.size();
			for (const Boolean& boolean : clause) {
				clause_hash = combine_hash(clause_hash, boolean.get_hash());
			}
			hash = combine_hash(hash, clause_hash);
		}
	}

	return hash;
}

uint64_t Proposition::get_hash(ItemId to_replace) const {
	if (clauses.empty()) {
		return (uint64_t)empty_result + 1;
	}

	size_t h = clauses.size();
	for (auto& clause : clauses) {
		uint64_t clause_hash = clause.size();
		for (const Boolean& boolean : clause) {
			clause_hash = combine_hash(clause_hash, boolean.get_hash(to_replace));
		}
		h = combine_hash(h, clause_hash);
	}
	return h;
}

void Proposition::get_all_items(std::vector<ItemId>& out) const {
	for (auto& clause : clauses) {
		for (const Boolean& boolean : clause) {
			boolean.get_all_items(out);
		}
	}
}

int Proposition::set_item(int idx, ItemId new_item) {
	for (auto& clause : clauses) {
		for (Boolean& boolean : clause) {
			idx = boolean.set_item(idx, new_item);
			if (idx == -1) return -1;
		}
	}
	return idx;
}

Proposition Proposition::single_out(ItemId item) const {
	Proposition new_proposition;
	new_proposition.empty_result = true;

	for (auto& clause : clauses) {
		bool first_term = true;
		for (const Boolean& term : clause) {
			if (has_item(term, item)) {
				if (first_term) {
					new_proposition.clauses.emplace_back();
					first_term = false;
				}
				new_proposition.clauses.back().push_back(term);
			}
		}
	}

	return new_proposition;
}

std::string Proposition::to_string(const Visible& visible) const {
	std::string str;
	for (size_t i = 0; i < clauses.size(); i++) {
		for (size_t j = 0; j < clauses[i].size(); j++) {
			str += clauses[i][j].to_string(visible);
			if (j != clauses[i].size() - 1) {
				str += " & ";
			}
		}
		if (i != clauses.size() - 1) {
			str += " | ";
		}
	}
	return str;
}

bool Proposition::operator==(const Proposition& other) const {
	if (get_hash() != other.get_hash()) return false;

	if (clauses.size() != other.clauses.size()) return false;
	for (size_t i = 0; i < clauses.size(); i++) {
		if (clauses[i].size() != other.clauses[i].size()) return false;
		for (size_t j = 0; j < clauses[i].size(); j++) {
			if (clauses[i][j] != other.clauses[i][j]) return false;
		}
	}

	return true;
}

bool Proposition::same(const Proposition& other, ItemId to_replace1, ItemId to_replace2) const {
	if (clauses.size() != other.clauses.size()) return false;
	for (size_t i = 0; i < clauses.size(); i++) {
		if (clauses[i].size() != other.clauses[i].size()) return false;
		for (size_t j = 0; j < clauses[i].size(); j++) {
			if (!clauses[i][j].eq(other.clauses[i][j], to_replace1, to_replace2)) return false;
		}
	}

	return true;
}
*/
