#ifndef WHAT_ITEMID_H
#define WHAT_ITEMID_H

#include <cstdint>
#include <cstddef>
#include <cassert>
#include <limits>
#include <string>
#include <unordered_map>
#include "TypeUtil.h"

// id 0 is null
// id 1 to 63 are variables
const int NUM_RESERVED_ITEM_IDS = 16;

struct ItemId {
	uint32_t val;
	ItemId() : val(0) { }
	explicit ItemId(std::string) : val(0) { }
	explicit ItemId(uint32_t val) : val(val) { assert((int32_t)val > 0); }
	explicit operator uint32_t() const { return val; }
	explicit operator uint64_t() const { return val; }

	inline bool is_var() const { return val > 0 && val < NUM_RESERVED_ITEM_IDS; }
	inline bool is_null() const { return val == 0; }

	auto operator<=>(const ItemId& other) const = default;
};

const ItemId ACTION_TARGET(1);

enum class Tk : int16_t {
	Null,
	Unknown,
	Self,

	// Boolean
	True,
	False,

	// Numeric
	One,
	Two,
	Three,
	Four,
	Five,

	// Kinds
	Hub,
	Shed,
	Key,
	Lever,
	Return,

	// Attributes
	Active,
	Stat,
	Kind,
	State,
	Vacant,

	// Children
	Parent,

	// Testing Attributes
	Radius,
	Mass,
	Distance,
	Orbiting,

	// Testing Values,
	VeryHigh,
	High,
	Medium,
	Low,
	VeryLow,

	COUNT,
};

inline std::string to_string(Tk token) {
	switch (token) {
		case Tk::Null:     return "Null";
		case Tk::Unknown:  return "Unknown";
		case Tk::True:     return "True";
		case Tk::False:    return "False";
		case Tk::One:      return "One";
		case Tk::Two:      return "Two";
		case Tk::Three:    return "Three";
		case Tk::Four:     return "Four";
		case Tk::Five:     return "Five";
		case Tk::Hub:      return "Hub";
		case Tk::Shed:     return "Shed";
		case Tk::Key:      return "Key";
		case Tk::Lever:    return "Lever";
		case Tk::Return:   return "Return";
		case Tk::Active:   return "Active";
		case Tk::Stat:     return "Stat";
		case Tk::Kind:     return "Kind";
		case Tk::State:    return "State";
		case Tk::Vacant:   return "Vacant";
		case Tk::Radius:   return "Radius";
		case Tk::Mass:     return "Mass";
		case Tk::Distance: return "Distance";
		case Tk::Orbiting: return "Orbiting";
		case Tk::VeryHigh: return "VeryHigh";
		case Tk::High:     return "High";
		case Tk::Medium:   return "Medium";
		case Tk::Low:      return "Low";
		case Tk::VeryLow:  return "VeryLow";
		default: return "?" + std::to_string((int)token) + "?";
	}
}

template<typename T>
T rep(T t, T to_replace, T replacement = T()) {
	return t == to_replace ? replacement : t;
}

// Replace every instance of to_replace with replacement
template<typename T>
T rep_all(const T& t, ItemId to_replace, ItemId replacement = ItemId()) {
	T t_copy = t;

	Vec<ItemId> items;
	t_copy.get_all_items(items);
	for (size_t i = 0; i < items.size(); i++) {
		if (items[i] == to_replace) {
			t_copy.set_item(i, replacement);
		}
	}

	return t_copy;
}

template<typename T>
bool has_item(const T& t, ItemId item) {
	std::vector<ItemId> items;
	t.get_all_items(items);
	return std::count(items.begin(), items.end(), item) > 0;
}

namespace std {
	template<>
	struct hash<ItemId> {
		size_t operator()(const ItemId& hash) const {
			return hash.val;
		}
	};
}

inline std::unordered_map<ItemId, std::string> item_names; // basically for debugging

#endif //WHAT_ITEMID_H
