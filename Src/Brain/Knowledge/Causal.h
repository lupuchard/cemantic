#ifndef WHAT_CAUSAL_H
#define WHAT_CAUSAL_H

#include "CnfFormula.h"
#include "VarUtil.h"
#include "Action.h"

class MutCausal {
public:
	MutCausal(Boolean event, CnfFormula condition = CnfFormula()):
		event(event), condition(condition) { }

	inline Hash<MutCausal> get_hash() const {
		return combine_hash(event.get_hash(), condition.get_hash());
	}

	inline Hash<MutCausal> get_hash(ItemId to_replace) const {
		return combine_hash(event.get_hash(to_replace), condition.get_hash(to_replace));
	}

	inline bool operator==(const MutCausal& other) const {
		return event == other.event && condition == other.condition;
	}
	inline bool operator!=(const MutCausal& other) const {
		return !(*this == other);
	}

	inline bool compare(const MutCausal& other) const {
		if (get_hash() < other.get_hash()) {
			return true;
		} else if (get_hash() > other.get_hash() || *this == other) {
			return false;
		}

		// If the function hasn't returned yet, then there's been a hash collision.
		// This should be extremely rare, so performance for the rest of the function isn't a major concern.

		if (event != other.event) {
			return event.get_hash() < other.event.get_hash();
		}

		assert(condition.get_hash() != other.condition.get_hash());
		return condition.get_hash() < other.condition.get_hash();
	}

	inline const Boolean& get_event() const {
		return event;
	}
	inline Boolean& get_event() {
		return event;
	}

	inline Vec<ItemId> get_all_items() const {
		Vec<ItemId> items;
		get_all_items(items);
		return items;
	}

	inline void get_all_items(Vec<ItemId>& out) const {
		event.get_all_items(out);
		condition.get_all_items(out);
	}

	// Returns -1 if successfully set. Returns (idx - num_items) if failed to set.
	inline int set_item(int idx, ItemId new_item) {
		idx = event.set_item(idx, new_item);
		return idx >= 0 ? condition.set_item(idx, new_item) : -1;
	}

	inline std::string to_string(const Visible& visible) const {
		std::string str = "=> " + event.to_string(visible);
		if (condition.num_clauses() > 0) {
			str += " ~ " + condition.to_string(visible);
		}
		return str;
	}

	inline const CnfFormula& get_condition() const {
		return condition;
	}
	inline CnfFormula& get_condition() {
		return condition;
	}

private:
	Boolean event;
	CnfFormula condition;
	// TODO: cache hash?
};

class MutGenericCausal {
public:
	MutGenericCausal(const MutCausal& causal, ItemId generified): causal(causal), generified(generified) { }

	inline Hash<MutCausal> get_hash() const {
		//if (hash == 0) {
			return causal.get_hash(generified).val;
		//}

		//return hash;
	}

	inline bool same(const MutGenericCausal& other) const {
		if (get_hash() != other.get_hash()) {
			return false;
		}

		return get_event().eq(other.get_event(), generified, other.generified) &&
		       get_condition().same(other.get_condition(), generified, other.generified);
	}

	inline bool compare(const MutGenericCausal& other) const {
		if (get_hash() < other.get_hash()) {
			return true;
		} else if (get_hash() > other.get_hash() || same(other)) {
			return false;
		}

		// If the function hasn't returned yet, then there's been a hash collision.
		// This should be extremely rare, so performance for the rest of the function isn't a major concern.

		if (!get_event().eq(other.get_event(), generified, other.generified)) {
			return rep_all(get_event(), generified) < rep_all(other.get_event(), other.generified);
		}

		// TODO: lol this is also unfinished
		assert(get_condition().get_hash(generified) != other.get_condition().get_hash(other.generified));
		return get_condition().get_hash(generified) < other.get_condition().get_hash(other.generified);
	}

	inline MutCausal convert() const {
		MutCausal new_causal = causal;
		Vec<ItemId> items = new_causal.get_all_items();
		for (size_t i = 0; i < items.size(); i++) {
			if (items[i] == generified) {
				new_causal.set_item(i, ItemId());
			}
		}
		return new_causal;
	}

	inline Vec<ItemId> get_all_items() const {
		// TODO: need to consider the generified causal?
		return causal.get_all_items();
	}

	inline std::string to_string(const Visible& visible) const {
		return causal.to_string(visible);
	}

	inline const Boolean& get_event() const {
		return causal.get_event();
	}

	inline const CnfFormula& get_condition() const {
		return causal.get_condition();
	}

	inline ItemId get_item() const {
		return generified;
	}

private:
	const MutCausal& causal;
	ItemId generified;
	//mutable uint64_t hash = 0;
};

inline bool generic_causal_same(const MutGenericCausal& causal1, const MutCausal& causal2) {
	if (causal1.get_hash() != causal2.get_hash()) {
		return false;
	}

	return rep_all(causal1.get_event(), causal1.get_item()) < causal2.get_event() &&
	       causal1.get_condition().same(causal2.get_condition(), causal1.get_item(), ItemId());
}

inline bool generic_causal_compare(const MutCausal& causal1, const MutCausal& causal2) {
	return causal1.compare(causal2);
}

inline bool generic_causal_compare(const MutGenericCausal& causal1, const MutGenericCausal& causal2) {
	return causal1.compare(causal2);
}

inline bool generic_causal_compare(const MutGenericCausal& causal1, const MutCausal& causal2) {
	if (causal1.get_hash() < causal2.get_hash()) {
		return true;
	} else if (causal1.get_hash() > causal2.get_hash() || generic_causal_same(causal1, causal2)) {
		return false;
	}

	// If the function hasn't returned yet, then there's been a hash collision.
	// This should be extremely rare, so performance for the rest of the function isn't a major concern.

	if (!causal1.get_event().eq(causal2.get_event(), causal1.get_item(), ItemId())) {
		return rep_all(causal1.get_event(), causal1.get_item()) < causal2.get_event();
	}

	// TODO: lol this is also unfinished
	assert(causal1.get_condition().get_hash(causal1.get_item()) != causal2.get_condition().get_hash());
	return causal1.get_condition().get_hash(causal1.get_item()) < causal2.get_condition().get_hash();
}

inline bool generic_causal_compare(const MutCausal& causal1, const MutGenericCausal& causal2) {
	if (causal1.get_hash() < causal2.get_hash()) {
		return true;
	} else if (causal1.get_hash() > causal2.get_hash() || generic_causal_same(causal2, causal1)) {
		return false;
	}

	// If the function hasn't returned yet, then there's been a hash collision.
	// This should be extremely rare, so performance for the rest of the function isn't a major concern.

	if (!causal1.get_event().eq(causal2.get_event(), ItemId(), causal2.get_item())) {
		return causal1.get_event() < rep_all(causal2.get_event(), causal2.get_item());
	}

	// TODO: lol this is also unfinished
	assert(causal1.get_condition().get_hash(ItemId()) != causal2.get_condition().get_hash(causal2.get_item()));
	return causal1.get_condition().get_hash(ItemId()) < causal2.get_condition().get_hash(causal2.get_item());
}

inline const MutCausal& generic_causal_convert(const MutCausal& causal) {
	return causal;
}

inline MutCausal generic_causal_convert(const MutGenericCausal& causal) {
	return causal.convert();
}

typedef InfoWrapper<MutCausal, Hash<MutCausal>> Causal;
typedef InfoWrapper<MutGenericCausal, Hash<MutCausal>> GenericCausal;

namespace std {
	template<>
	struct hash<MutCausal> {
		size_t operator()(const MutCausal& causal) const {
			return (size_t)causal.get_hash();
		}
	};
}

#endif //WHAT_CAUSAL_H
