#ifndef WHAT_TRAIT_H
#define WHAT_TRAIT_H

#include "StrUtil.h"
#include "Hash.h"
#include "VarUtil.h"
#include "Visible.h"
#include <memory>
#include <compare>
#include <cassert>

struct TraitVal {
	int32_t val;

	TraitVal(): val(0) { }
	TraitVal(int32_t val): val(val) { }
	TraitVal(ItemId item): val(-item.val) { assert(val < 0); }
	TraitVal(Tk tk):  val((int32_t)tk) { }

	inline bool is_null() const { return val == 0; }
	inline bool is_item() const { return val < 0; }
	inline bool is_tk() const { return val >= 0; }

	inline ItemId item() const {
		assert(is_item());
		return ItemId(-val);
	}

	inline Tk tk() const {
		assert(is_tk());
		return (Tk)val;
	}

	auto operator<=>(const TraitVal& other) const = default;

	inline std::string to_string(const Visible& visible) const {
		if (is_tk()) {
			return ::to_string(tk());
		} else {
			return visible.item_name(item());
		}
	}
};

// TODO: Do we need both Trait and Attributes? Both do the same thing - they store the values of attributes.
//       The difference is Trait stores only one attribute, and Attributes stores all of them.
//       Should this be unified?

class MutTrait {
public:
	MutTrait(): key(Tk::Null), value() { }
	MutTrait(Tk key, Tk tk): key(key), value(tk) { }
	MutTrait(Tk key, ItemId child): key(key), value(child) { }
	MutTrait(Tk key, TraitVal val): key(key), value(val) { }

	inline Hash<MutTrait> get_hash() const {
		return ((uint64_t)value.val << 32) | (uint64_t)key;
	}

	inline ModHash<MutTrait> get_hash(ItemId to_repl) const {
		if (value.is_item()) {
			return ((uint64_t)TraitVal(rep(value.item(), to_repl)).val << 32) | (uint64_t)key;
		} else {
			return (uint64_t)get_hash();
		}
	}

	inline bool eq(const MutTrait& other, ItemId to_replace1, ItemId to_replace2) const {
		if (value.is_item()) {
			if (!other.value.is_item()) {
				return false;
			}

			return MutTrait(key, rep(value.item(), to_replace1)) == MutTrait(other.key, rep(other.value.item(), to_replace2));
		}

		return *this == other;
	}

	auto operator<=>(const MutTrait& other) const = default;

	inline bool compare_type(const MutTrait& other) const {
		return key == other.key;
	}

	inline Tk get_key() const {
		return key;
	}

	inline TraitVal get_value() const {
		return value;
	}

	inline std::string to_string(const Visible& visible) const {
		return fmt::format("{} = {}", ::to_string(key), value.to_string(visible));
	}

	inline std::string to_string(const Visible& visible, ItemId item) const {
		return visible.item_name(item) + "." + to_string(visible);
	}

	inline Vec<ItemId> get_all_items() const {
		if (value.is_item()) {
			return Vec<ItemId> { value.item() };
		} else {
			return Vec<ItemId>();
		}
	}

	inline int set_item(int idx, ItemId new_item) {
		if (idx == 0 && value.is_item()) {
			value = TraitVal(new_item);
			return -1;
		} else {
			return idx;
		}
	}

	/*inline void get_all_items_mut(Vec<ItemId*>& out) const {
		if (kind == Child) {
			out.push_back((ItemId*)&value);
		}
	}*/

private:
	Tk key;
	TraitVal value;
};

typedef InfoWrapper<MutTrait, Hash<MutTrait>> Trait;


class TraitMap {
public:
	inline const Trait& insert(ItemId item, Trait trait) {
		auto iter = map.emplace(std::make_pair(item, trait.v().get_key()), std::make_unique<Trait>(trait));
		return *iter.first->second;
	}

	inline const Trait* get(ItemId item, Tk attrib) const {
		auto iter = map.find(std::make_pair(item, attrib));
		return iter == map.end() ? nullptr : iter->second.get();
	}

	inline Trait* get(ItemId item, Tk attrib) {
		auto iter = map.find(std::make_pair(item, attrib));
		return iter == map.end() ? nullptr : iter.value().get();
	}

	inline auto remove(const Map<Pair<ItemId, Tk>, CpBox<Trait>>::const_iterator& iter) {
		return map.erase(iter);
	}

	inline size_t remove(ItemId item, Tk attrib) {
		return map.erase(std::make_pair(item, attrib));
	}

	inline auto size()  const { return map.size(); }
	inline auto begin() const { return map.begin(); }
	inline auto end()   const { return map.end(); }

	inline std::string to_string(const Visible& visible) const {
		std::string str = "{ ";
		bool first = true;
		for (auto& elem : map) {
			if (!first) str += ", ";
			first = false;
			str += elem.second->to_string(visible, elem.first.first);
		}
		return str + " }";
	}

	/*static TraitMap from(const Map<ItemId, CpBox<_Attributes>>& attributes) {
		TraitMap trait_map;
		for (auto& pair : attributes) {
			trait_map.insert(pair.first, MutTrait(Attribute::Active, (*pair.second)[Attribute::Active]));
			trait_map.insert(pair.first, MutTrait(Attribute::Stat, (*pair.second)[Attribute::Stat]));
			trait_map.insert(pair.first, MutTrait(Attribute::Kind, (*pair.second)[Attribute::Kind]));
			trait_map.insert(pair.first, MutTrait(Attribute::Loc, (*pair.second)[Attribute::Loc]));
		}
		return trait_map;
	}*/

	static TraitMap from(const Map<ItemId, Map<Tk, MutTrait>>& state) {
		TraitMap trait_map;
		for (auto& pair : state) {
			for (auto& trait : pair.second) {
				trait_map.insert(pair.first, Trait::make(trait.second));
			}
		}
		return trait_map;
	}

private:
	Map<Pair<ItemId, Tk>, CpBox<Trait>> map;
};

namespace std {
	template<>
	struct hash<MutTrait> {
		size_t operator()(const MutTrait& trait) const {
			return (size_t)trait.get_hash();
		}
	};
}

#endif //WHAT_TRAIT_H
