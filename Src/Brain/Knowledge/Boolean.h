#ifndef PROJECT_BOOLEAN_H
#define PROJECT_BOOLEAN_H

#include "ItemId.h"
#include "Hash.h"
#include "TypeUtil.h"
#include "Trait.h"
#include <memory>

/// Represents a knowledge-type boolean condition
class Boolean {
public:
	Boolean(ItemId item, MutTrait trait, bool negate = false): item(item), negate(negate), trait(trait) { }
	Boolean(ItemId item, Tk key, TraitVal val, bool negate = false): item(item), negate(negate), trait(key, val) { }
	Boolean(ItemId item, Tk key, Tk val, bool negate = false): item(item), negate(negate), trait(key, val) { }
	Boolean(ItemId item, Tk key, ItemId val, bool negate = false): item(item), negate(negate), trait(key, val) { }

	inline ItemId get_item() const {
		return item;
	}

	inline const MutTrait& get_trait() const {
		return trait;
	}

	inline bool is_negated() const {
		return negate;
	}

	inline void set_negated(bool negated) {
		negate = negated;
	}

	// Ignores negate
	inline bool same_base(const Boolean& other) const {
		return item == other.item && trait == other.trait;
	}

	bool eq(const Boolean& other, ItemId to_replace1, ItemId to_replace2) const;

	inline int operator<=>(const Boolean& other) const {
		int item_diff = item.val - other.item.val;
		if (item_diff != 0) return item_diff;

		int key_diff = (int)trait.get_key() - (int)other.trait.get_key();
		if (key_diff != 0) return key_diff;

		int val_diff = trait.get_value().val - other.trait.get_value().val;
		if (val_diff != 0) return val_diff;

		return negate - other.negate;
	}

	inline bool operator==(const Boolean& other) const = default;

	inline Hash<Boolean> get_hash() const {
		uint64_t part = combine_hash(trait.get_hash(), item.val);
		return negate ? ~part : part;
	}
	inline ModHash<Boolean> get_hash(ItemId to_replace) const {
		uint64_t part = combine_hash(trait.get_hash(to_replace), rep(item, to_replace).val);
		return negate ? ~part : part;
	}

	std::string to_string(const Visible& visible) const;

	inline Vec<ItemId> get_all_items() const {
		Vec<ItemId> out;
		get_all_items(out);
		return out;
	}
	inline void get_all_items(Vec<ItemId>& out) const {
		out.push_back(item);
		auto items = trait.get_all_items();
		out.insert(out.end(), items.begin(), items.end());
	}

	inline int set_item(int idx, ItemId new_item) {
		if (idx == 0) {
			item = new_item;
			return -1;
		}

		return trait.set_item(idx - 1, new_item);
	}

private:
	ItemId item;
	bool negate = false;
	MutTrait trait;
};

static_assert(sizeof(Boolean) == 16, "Boolean is 16 bytes"); // TODO can lower to 12 by removing used_for_hash

namespace std {
	template<>
	struct hash<Boolean> {
		size_t operator()(const Boolean& boolean) const {
			return (size_t)boolean.get_hash();
		}
	};
}

#endif //PROJECT_BOOLEAN_H
