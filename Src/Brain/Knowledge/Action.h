#ifndef WHAT_ACTION_H
#define WHAT_ACTION_H

struct Action {
	ItemId target;

	Action(): target(ItemId()) { }
	Action(ItemId target): target(target) { }

	inline void get_all_items(Vec<ItemId>& items) const {
		items.push_back(target);
	}

	inline Vec<ItemId> get_all_items() const {
		return { target };
	}

	inline std::string to_string(const Visible& visible) const {
		return visible.item_name(target);
	}

	inline bool operator==(const Action& other) const {
		return target == other.target;
	}

	inline bool operator!=(const Action& other) const {
		return !operator==(other);
	}

	inline bool same(const Action& other, ItemId to_replace1, ItemId to_replace2) const {
		return rep(target, to_replace1) == rep(other.target, to_replace2);
	}

	inline bool compare(const Action& other) const {
		return target < other.target;
	}

	inline bool compare(const Action& other, ItemId to_replace1, ItemId to_replace2) const {
		return rep(target, to_replace1) < rep(other.target, to_replace2);
	}

	inline Hash<Action> get_hash() const {
		return (Hash<Action>)target.val;
	}

	inline Hash<Action> get_hash(ItemId to_replace) const {
		return (Hash<Action>)rep(target, to_replace).val;
	}

	inline int set_item(int idx, ItemId item) {
		if (idx == 0) {
			target = item;
			return -1;
		}

		return idx - 1;
	}
};

namespace std {
	template<>
	struct hash<Action> {
		size_t operator()(const Action& action) const {
			return (size_t)action.get_hash();
		}
	};
}

#endif //WHAT_ACTION_H
