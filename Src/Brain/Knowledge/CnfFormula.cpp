#include "CnfFormula.h"

CnfFormula::CnfFormula(const Vec<Vec<Boolean>>& in_clauses) {
	for (auto clause : in_clauses) {
		std::sort(clause.begin(), clause.end());

		// Remove duplicates
		for (size_t i = 0; i < clause.size() - 1; i++) {
			if (clause[i].same_base(clause[i + 1])) {
				if (clause[i].is_negated() == clause[i + 1].is_negated()) {
					clause.erase(clause.begin() + i + 1);
					i--;
				} else {
					clause.clear();
					break;
				}
			}
		}

		if (!clause.empty()) {
			insert_clause(std::move(clause));
		}
	}
}

CnfFormula::CnfFormula(Boolean boolean) {
	clauses.push_back({ std::move(boolean) });
}

void CnfFormula::and_bool(Boolean boolean) {
	if (clauses.empty() && !empty_result) return;
	hash = 0;

	insert_clause({ std::move(boolean) });
}

void CnfFormula::and_prop(const CnfFormula& prop) {
	if (clauses.empty()) {
		if (prop.clauses.empty()) {
			empty_result = empty_result && prop.empty_result;
		} else if (empty_result) {
			*this = prop;
		}

		return;
	} else if (prop.clauses.empty()) {
		if (!prop.empty_result) {
			*this = prop;
		}

		return;
	}

	hash = 0;

	for (auto& clause : prop.clauses) {
		insert_clause(clause);
		if (clauses.empty() && !empty_result) return;
	}
}

CnfFormula CnfFormula::get_disjunction(Boolean b) const {
	if (clauses.empty() && empty_result) return *this;

	CnfFormula output;
	for (auto clause : clauses) {
		insert_into_clause(b, clause);
		if (!clause.empty()) output.insert_clause(clause);
	}

	return output;
}

CnfFormula CnfFormula::get_negation() const {
	CnfFormula output;
	output.empty_result = !empty_result;

	for (auto clause : clauses) {
		for (Boolean& term : clause) {
			term.set_negated(!term.is_negated());
		}

		output.or_conjunctive_clause(clause);
	}

	return output;
}

void CnfFormula::get_all_items(std::vector<ItemId>& out) const {
	for (auto& clause : clauses) {
		for (const Boolean& boolean : clause) {
			boolean.get_all_items(out);
		}
	}
}

int CnfFormula::set_item(int idx, ItemId new_item) {
	for (auto& clause : clauses) {
		for (Boolean& boolean : clause) {
			idx = boolean.set_item(idx, new_item);
			if (idx == -1) return -1;
		}
	}
	return idx;
}

uint64_t CnfFormula::get_hash() const {
	if (hash) return hash;

	if (clauses.empty()) {
		hash = (uint64_t)empty_result + 1;
	} else {
		hash = clauses.size();
		for (auto& clause : clauses) {
			uint64_t clause_hash = clause.size();
			for (const Boolean& boolean : clause) {
				clause_hash = combine_hash(clause_hash, boolean.get_hash());
			}
			hash = combine_hash(hash, clause_hash);
		}
	}

	return hash;
}

uint64_t CnfFormula::get_hash(ItemId to_replace) const {
	if (clauses.empty()) {
		return (uint64_t)empty_result + 1;
	}

	size_t h = clauses.size();
	for (auto& clause : clauses) {
		uint64_t clause_hash = clause.size();
		for (const Boolean& boolean : clause) {
			clause_hash = combine_hash(clause_hash, boolean.get_hash(to_replace));
		}
		h = combine_hash(h, clause_hash);
	}
	return h;
}

CnfFormula CnfFormula::single_out(ItemId item) const {
	CnfFormula output;
	for (auto& clause : clauses) {
		bool first_term = true;
		for (const Boolean& term : clause) {
			if (has_item(term, item)) {
				if (first_term) {
					output.clauses.emplace_back();
					first_term = false;
				}
				output.clauses.back().push_back(term);
			}
		}
	}

	return output;
}

std::string CnfFormula::to_string(const Visible& visible) const {
	std::string str = "(";
	for (size_t i = 0; i < clauses.size(); i++) {
		for (size_t j = 0; j < clauses[i].size(); j++) {
			str += clauses[i][j].to_string(visible);
			if (j != clauses[i].size() - 1) {
				str += " | ";
			}
		}
		if (i != clauses.size() - 1) {
			str += ") & (";
		}
	}
	return str + ")";
}

bool CnfFormula::operator==(const CnfFormula& other) const {
	if (get_hash() != other.get_hash()) return false;

	if (clauses.size() != other.clauses.size()) return false;
	for (size_t i = 0; i < clauses.size(); i++) {
		if (clauses[i].size() != other.clauses[i].size()) return false;
		for (size_t j = 0; j < clauses[i].size(); j++) {
			if (clauses[i][j] != other.clauses[i][j]) return false;
		}
	}

	return true;
}

bool CnfFormula::same(const CnfFormula& other, ItemId to_replace1, ItemId to_replace2) const {
	if (clauses.size() != other.clauses.size()) return false;
	for (size_t i = 0; i < clauses.size(); i++) {
		if (clauses[i].size() != other.clauses[i].size()) return false;
		for (size_t j = 0; j < clauses[i].size(); j++) {
			if (!clauses[i][j].eq(other.clauses[i][j], to_replace1, to_replace2)) return false;
		}
	}

	return true;
}

bool clause_sorter(const std::vector<Boolean>& clause1, const std::vector<Boolean>& clause2) {
	for (size_t i = 0; i < std::min(clause1.size(), clause2.size()); i++) {
		if (clause1[i] < clause2[i]) {
			return true;
		} else if (clause1[i] > clause2[i]) {
			return false;
		}
	}
	return clause1.size() > clause2.size();
}

void CnfFormula::insert_clause(Vec<Boolean> new_clause) {
	if (clauses.empty()) {
		clauses.push_back(std::move(new_clause));
		return;
	}

	// Check for duplicates
	// (A or B) and A  =>  A
	for (size_t i = 0; i < clauses.size(); i++) {
		auto& clause = clauses[i];
		size_t k = 0;
		for (size_t j = 0; j < clause.size(); j++) {
			if (new_clause[k] == clause[j]) {
				k++;
				if (k == new_clause.size()) {
					if (clause.size() == new_clause.size()) {
						return;
					} else {
						// Replace with new_clause
						total_terms -= clause.size();
						clauses.erase(clauses.begin() + i);
						i--;
						break;
					}
				}
			}
		}
	}

	// Check for negation
	// (A or B) and !A  =>  B and !A
	Vec<Vec<Boolean>> clauses_to_insert;
	if (new_clause.size() == 1) {
		for (size_t i = 0; i < clauses.size(); i++) {
			for (size_t j = 0; j < clauses[i].size(); j++) {
				auto& term = clauses[i][j];
				if (term.same_base(new_clause[0])) {
					assert(term.is_negated() == !new_clause[0].is_negated());

					if (clauses[i].size() == 1) {
						clauses.clear();
						total_terms = 0;
						empty_result = false;
						return;
					}

					// Remove term then reinsert
					total_terms -= clauses[i].size();
					clauses[i].erase(clauses[i].begin() + j);
					clauses_to_insert.push_back(std::move(clauses[i]));
					clauses.erase(clauses.begin() + i);
					i--;
					break;
				}
			}
		}
	}

	// Check for group negation
	// !A and !B and (A or B) => False
	size_t k = 0;
	for (auto& clause : clauses) {
		if (clause.size() != 1) continue;
		if (clause[0].same_base(new_clause[k]) && clause[0].is_negated() != new_clause[k].is_negated()) {
			k++;
			if (k == new_clause.size()) {
				clauses.clear();
				empty_result = false;
				return;
			}
		}
	}

	// Then insert
	bool inserted = false;
	total_terms += new_clause.size();
	for (size_t i = 0; i < clauses.size(); i++) {
		if (clause_sorter(clauses[i], new_clause)) {
			clauses.insert(clauses.begin() + i, std::move(new_clause));
			inserted = true;
			break;
		}
	}
	if (!inserted) clauses.push_back(std::move(new_clause));


	for (auto clause : clauses_to_insert) {
		insert_clause(std::move(clause));
		if (clauses.empty() && !empty_result) return;
	}
}

void CnfFormula::insert_into_clause(const Boolean& term, Vec<Boolean>& clause) {
	for (size_t j = 0; j < clause.size(); j++) {
		if (term.same_base(clause[j])) {
			// TODO: if two terms have the same item and key, but different values, should evaluate to false

			if (!term.is_negated() == clause[j].is_negated()) {
				// if same and inverted, delete the clause
				clause.clear();
			}

			// if same and not inverted, do nothing
			return;
		} else if (term < clause[j]) {
			clause.insert(clause.begin() + j, term);
			return;
		}
	}

	clause.push_back(term);
}

void CnfFormula::or_conjunctive_clause(Vec<Boolean> clause) {
	if (clauses.empty()) {
		for (Boolean term : clause) {
			clauses.push_back({term});
		}
	} else {
		auto previous = clauses;
		clauses.clear();
		total_terms = 0;
		for (size_t i = 0; i < clause.size(); i++) {
			for (size_t j = 0; j < previous.size(); j++) {
				auto copy = previous[j];
				insert_into_clause(clause[i], copy);
				if (!copy.empty()) insert_clause(copy);
			}
		}
	}
}
