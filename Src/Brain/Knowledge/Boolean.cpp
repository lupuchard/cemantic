#include "Boolean.h"
#include <fmt/core.h>

bool Boolean::eq(const Boolean& other, ItemId to_replace1, ItemId to_replace2) const {
	if (rep(item, to_replace1) != rep(other.item, to_replace2) || negate != other.negate) return false;
	return trait.eq(other.trait, to_replace1, to_replace2);
}

std::string Boolean::to_string(const Visible& visible) const {
	return fmt::format("{}{}.{}", (negate ? "!" : ""), visible.item_name(item), trait.to_string(visible));
}
