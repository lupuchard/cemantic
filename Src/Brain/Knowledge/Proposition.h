/*#ifndef PROJECT_PROPOSITION_H
#define PROJECT_PROPOSITION_H

#include <vector>
#include "Boolean.h"

// DNF aka Or(And(Not(Bool)))
class Proposition {
public:
	Proposition() = default;
	Proposition(Vec<Vec<Boolean>> clauses);
	explicit Proposition(Boolean boolean);

	inline size_t num_terms() const {
		return total_terms;
	}

	inline size_t num_clauses() const {
		return clauses.size();
	}

	inline size_t num_terms(size_t clause) const {
		assert(clause < clauses.size());
		return clauses[clause].size();
	}

	inline const Boolean& get_term(size_t clause, size_t term) const {
		assert(clause < clauses.size() && term < clauses[clause].size());
		return clauses[clause][term];
	}


	class Iterator : std::iterator<std::forward_iterator_tag, Boolean, std::ptrdiff_t, Boolean*, Boolean&> {
	public:
		Iterator(const Proposition& proposition, size_t clause, size_t term):
			proposition(proposition), clause(clause), term(term) { }

		inline Iterator& operator++() {
			term++;
			if (term >= proposition.num_terms(clause)) {
				term = 0;
				clause++;
			}
			return *this;
		}

		inline const Boolean& operator*() {
			return proposition.get_term(clause, term);
		}

		inline const Boolean* operator->() {
			return &operator*();
		}

		inline bool operator==(const Iterator& other) const {
			return &proposition == &other.proposition && clause == other.clause && term == other.term;
		}

		inline bool operator!=(const Iterator& other) const {
			return !operator==(other);
		}

	private:
		const Proposition& proposition;
		size_t clause;
		size_t term;
	};

	inline Iterator begin() const {
		return Iterator(*this, 0, 0);
	}
	inline Iterator end() const {
		return Iterator(*this, clauses.size(), 0);
	}


	void or_bool(Boolean b);
	void and_bool(Boolean b);
	void and_prop(Proposition& prop);
	Proposition get_negation() const;
	void and_disjunctive_clause(Vec<Boolean> clause);

	void get_all_items(Vec<ItemId>& out) const;
	int set_item(int idx, ItemId item);
	inline bool get_empty_result() const { return empty_result; }

	// Return the proposition including only terms containing this item.
	Proposition single_out(ItemId item) const;

	uint64_t get_hash() const;
	uint64_t get_hash(ItemId to_replace) const;
	std::string to_string(const Visible& visible) const;

	bool operator==(const Proposition& other) const;
	inline bool operator!=(const Proposition& other) const { return !operator==(other); }
	bool same(const Proposition& other, ItemId to_replace1, ItemId to_replace2) const;

private:
	//Vec<size_t> remove_duplicates(size_t clause);
	bool insert_clause(Vec<Boolean> clause);
	bool insert_into_clause(const Boolean& term, Vec<Boolean>& clause);
	//void cleanup(int num_new_clauses);
	//void sort_clauses();

	Vec<Vec<Boolean>> clauses;
	uint32_t total_terms = 0;
	bool empty_result = true; // What this proposition evaluates to if there are no clauses.
	mutable uint64_t hash = 0;
};

#endif //PROJECT_PROPOSITION_H
*/