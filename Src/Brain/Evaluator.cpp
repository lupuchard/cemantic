#include "Evaluator.h"
#include "MathUtil.h"
#include <ItemInfo.h>

Evaluator::Evaluator(const Visible& visible): visible(&visible), assignments{} { }

Evaluator::Evaluator(const Assignment& assignment): assignments{} {
	use_assignment(assignment);
}

Evaluator::Evaluator(const Visible& visible, const Assignment& assignment): visible(&visible), assignments{} {
	use_assignment(assignment);
}

float Evaluator::evaluate_bool(const CnfFormula& prop) {
	if (prop.num_clauses() == 0) {
		return (float)prop.get_empty_result();
	}

	float result = 1;
	for (size_t i = 0; i < prop.num_clauses(); i++) {
		float clause_result = 0;
		for (size_t j = 0; j < prop.num_terms(i); j++) {
			clause_result = 1 - (1 - clause_result) * (1 - evaluate_bool(prop.get_term(i, j)));
			if (clause_result == 1) break;
		}
		result *= clause_result;
		if (result == 0) break;
	}
	return result;
}

float Evaluator::evaluate_bool(const CnfFormula& prop, const TraitMap& alteration) {
	test_alteration = &alteration;
	float result = evaluate_bool(prop);
	test_alteration = nullptr;
	return result;
}

float Evaluator::evaluate_bool(const CnfFormula &prop, const Moment &moment) {
	if (prop.num_clauses() == 0) {
		return 1;
	}

	test_moment = &moment;
	float result = evaluate_bool(prop);
	test_moment = nullptr;
	return result;
}

float Evaluator::evaluate_bool(const Boolean& boolean) {
	Pair<TraitVal, float> val = get_trait(boolean.get_item(), boolean.get_trait().get_key());
	if (boolean.is_negated() != (val.first == boolean.get_trait().get_value())) {
		return val.second;
	} else {
		return 1 - val.second;
	}
}

float Evaluator::evaluate_bool(const Boolean& boolean, const TraitMap& alteration) {
	test_alteration = &alteration;
	float result = evaluate_bool(boolean);
	test_alteration = nullptr;
	return result;
}

float Evaluator::evaluate_bool(const Boolean& boolean, const Moment& moment) {
	test_moment = &moment;
	float result = evaluate_bool(boolean);
	test_moment = nullptr;
	return result;
}

/*std::pair<float, float> Evaluator::evaluate_number(Quickmath& math) {
	float result = 0;
	float total_confidence = 0;

	for (size_t i = 0; i < math.num_terms(); i++) {
		float term_result = math.get_factor(i);

		for (size_t j = 0; j < math.num_values(i); j++) {
			std::pair<float, float> value = evaluate_number(math.get_value(i, j));
			term_result *= value.first;
			total_confidence += value.second;
		}

		result += term_result;
	}

	return std::pair(result, total_confidence / math.num_values());
}

std::pair<float, float> Evaluator::evaluate_number(const Number& num) {
	switch (num.get_kind()) {
		case Number::Literal:
			return std::pair(num.get_value(), 1);
		case Number::Stat: {
			return get_trait(num.get_item(), Attribute::Stat);
		}
		default:
			assert(false);
	}
	return std::pair<float, float>();
}*/

float Evaluator::evaluate_impact(const Boolean& event, const CnfFormula& prop) {
	float max_impact = -std::numeric_limits<float>::infinity();
	for (size_t i = 0; i < prop.num_clauses(); i++) {
		float cur_difficulty = evaluate_difficulty(prop, i);
		test_mutation = &event;
		float new_difficulty = evaluate_difficulty(prop, i);
		test_mutation = nullptr;
		max_impact = std::max(max_impact, cur_difficulty - new_difficulty);
	}
	return max_impact;
}

float Evaluator::evaluate_difficulty(const CnfFormula& prop, size_t clause) {
	/// TODO: ha this can be improved

	float difficulty = 0;
	for (size_t j = 0; j < prop.num_terms(clause); j++) {
		difficulty += (1 - evaluate_bool(prop.get_term(clause, j)));
	}

	return difficulty;
}

std::pair<TraitVal, float> Evaluator::get_trait(ItemId item, Tk key) {
	if (item.val < NUM_RESERVED_ITEM_IDS) {
		assert(!assignments[item.val].is_null());
		item = assignments[item.val];
	}

	if (test_mutation != nullptr) {
		if (test_mutation->get_item() == item && test_mutation->get_trait().get_key() == key) {
			assert(!test_mutation->is_negated());
			return std::make_pair(test_mutation->get_trait().get_value(), 1);
		}
	}

	if (test_alteration != nullptr) {
		const Trait* trait = test_alteration->get(item, key);
		if (trait != nullptr) {
			return std::make_pair(trait->v().get_value(), 1);
		}
		// TODO: ackshually, if the trait isn't found at this moment, we should refer to a nearby moment, not defer to SemanticMemory
	}

	if (test_moment != nullptr) {
		auto iter = test_moment->state.find(item);
		if (iter != test_moment->state.end()) {
			auto result = iter->second.find(key);
			// TODO: issue: need to actually distinguish between attribute known to have been null and attribute of unknown value
			return std::make_pair(result == iter->second.end() ? Tk::Null : result->second.get_value(), 1);
		}
	}

	if (visible != nullptr) {
		ItemId child = visible->item_child(item, key);
		if (child == ItemId()) {
			return std::make_pair(visible->item_attribute(item, key), 1);
		}
		return std::make_pair(child, 1);
	}

	return std::make_pair(Tk::Null, 0);
}

void Evaluator::use_assignment(ItemId var, ItemId val) {
	assert(var.val < NUM_RESERVED_ITEM_IDS);
	assignments[var.val] = val;
}

void Evaluator::use_assignment(const Assignment& assignment) {
	for (size_t i = 1; i < assignment.length(); i++) {
		assignments[i] = assignment[(ItemId)i];
	}
}

void Evaluator::clear_assignments() {
	for (size_t i = 0; i < NUM_RESERVED_ITEM_IDS; i++) {
		assignments[i] = ItemId();
	}
}
