#ifndef WHAT_GRAPHUTIL_H
#define WHAT_GRAPHUTIL_H

#include <Hierarchy.h>

class GraphUtil {
public:
	struct VertexProperties {
		VertexProperties() { }
		VertexProperties(std::string name, std::string kind, double confidence):
			name(std::move(name)), kind(std::move(kind)), confidence(confidence) { }
		std::string name;
		std::string kind;
		double confidence;
	};

	struct EdgeProperties {
		double weight;
	};

	struct GraphProperties {
		std::string name;
	};

	void hierarchy_to_graphml(const Hierarchy& hierarchy, std::ofstream& out, const std::string& graph_name);

private:
	typedef std::pair<int, int> Edge;

	void get_edges(const Visible& visible, const Cluster& cluster);
	void index_clusters(const Cluster& cluster);

	std::vector<Edge> my_edges;
	std::vector<double> edge_weights;
	std::unordered_map<const Cluster*, int> cluster_id_map;

	std::vector<VertexProperties> vertex_info;

	int next_cluster_id = 0;
	int next_item_id = 0;
};


#endif //WHAT_GRAPHUTIL_H
