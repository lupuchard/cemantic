#include "GraphUtil.h"
#include <boost/graph/graphml.hpp>

typedef boost::adjacency_list<
	boost::vecS, boost::vecS, boost::directedS,
	GraphUtil::VertexProperties,
	GraphUtil::EdgeProperties,
	GraphUtil::GraphProperties
> Graph;

void GraphUtil::hierarchy_to_graphml(const Hierarchy& hierarchy, std::ofstream& out, const std::string& graph_name) {
	my_edges.clear();
	cluster_id_map.clear();
	next_cluster_id = 0;
	next_item_id = 0;

	const Cluster& root = hierarchy.get_root();
	index_clusters(root);
	vertex_info.reserve(cluster_id_map.size() + next_item_id);
	next_item_id = 0;
	get_edges(hierarchy.get_visible(), root);

	Graph g(my_edges.begin(), my_edges.end(), cluster_id_map.size() + next_item_id, my_edges.size());

	boost::graph_traits<Graph>::vertex_iterator v, v_end;
	for (boost::tie(v, v_end) = vertices(g); v != v_end; ++v) {
		VertexProperties& props = vertex_info[*v];
		put(&VertexProperties::name, g, *v, props.name);
		put(&VertexProperties::confidence, g, *v, props.confidence);
		put(&VertexProperties::kind, g, *v, props.kind);
	}

	boost::graph_traits<Graph>::edge_iterator e, e_end;
	int idx = 0;
	for (boost::tie(e,e_end) = edges(g); e != e_end; ++e) {
		put(&EdgeProperties::weight, g, *e, edge_weights[idx++]);
	}

	set_property(g, &GraphProperties::name, graph_name);

	boost::dynamic_properties dp;
	dp.property("kind", get(&VertexProperties::kind, g));
	dp.property("name", get(&VertexProperties::name, g));
	dp.property("confidence", get(&VertexProperties::confidence, g));
	dp.property("weight", get(&EdgeProperties::weight, g));
	dp.property("name", boost::ref_property_map<Graph*, std::string>(get_property(g, &GraphProperties::name)));

	write_graphml(out, g, dp, true);
}

void GraphUtil::get_edges(const Visible& visible, const Cluster& cluster) {
	int cluster_id = cluster_id_map[&cluster];
	vertex_info.emplace_back(fmt::format("Cluster {}", cluster_id), "cluster", 1);
	auto items = cluster.subitems();
	if (items == nullptr) {
		auto& triple = *cluster.triple();
		for (int i = 0; i < 3; i++) {
			if (triple[i] != nullptr) {
				my_edges.emplace_back(cluster_id, cluster_id_map[triple[i].get()]);
				edge_weights.push_back(triple[i]->lambda);
				get_edges(visible, *triple[i]);
			}
		}
	} else {
		for (auto& item_pair : *items) {
			if (item_pair.second <= 0) continue;
			int item_id = next_item_id++ + cluster_id_map.size();
			my_edges.emplace_back(cluster_id, item_id);
			edge_weights.push_back(0);
			//Item& item = Item::get(item_pair.first);
			//std::string kind = item.info() == nullptr ? "variable" : to_string(item.info()->kind());
			vertex_info.emplace_back(visible.item_name(item_pair.first), "TODO:kind", item_pair.second);
		}
	}
}

void GraphUtil::index_clusters(const Cluster& cluster) {
	cluster_id_map[&cluster] = next_cluster_id++;
	auto items = cluster.subitems();
	if (items == nullptr) {
		auto& triple = *cluster.triple();
		for (int i = 0; i < 3; i++) {
			if (triple[i] != nullptr) {
				index_clusters(*triple[i]);
			}
		}
	} else {
		next_item_id += items->size();
	}
}
