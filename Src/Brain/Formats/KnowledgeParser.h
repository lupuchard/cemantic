/*#ifndef PROJECT_KNOWLEDGEPARSER_H
#define PROJECT_KNOWLEDGEPARSER_H

#include "Knowledge/Trait.h"
#include "Knowledge/Causal.h"
#include "Knowledge/Quickmath.h"
#include "ItemInfo.h"
#include <unordered_map>

// Loads knowledge from puzzle files (e.g. Data/puzzle1.txt)
class KnowledgeParser {
public:

	struct ParseResult {
		ItemId root;
		Proposition goal;
		Vec<Pair<ItemId, Box<MutTrait>>> traits;
		Vec<Box<MutCausal>> causals;
	};

	ParseResult parse_file(std::ifstream& stream);

private:
	struct Token {
		std::string str;
		enum Type { NONE, IDENT, NUM, SYMBOL };
		Type type;
		int line_num;
		Token(std::string str, Type type, int line_num):
			str(std::move(str)), type(type), line_num(line_num) { }
		Token():
			type(Type::NONE), line_num(0) { }
	};

	struct Expr {
		Expr(Token token, Token delim): token(std::move(token)), delim(std::move(delim)) { }
		Token token, delim;
		std::vector<Expr> args;
	};

	struct ParsedItem {
		ItemId item;
		int quantity;
		bool locked = false;
		ParsedItem(ItemId item, int quantity): item(item), quantity(quantity) { }
	};

	std::string trim(const std::string& str, int& lines_after);
	void print_err(const std::string& message, int line_num = -1);
	bool tokenize(const std::string& str, long pos, long to_end);
	void finish_token(Token::Type type, int line_num);

	Pair<ItemId, Box<MutTrait>> parse_trait(const std::string& file, long pos, long to_semi);
	Box<MutCausal> parse_causal(const std::string& file, long pos, long to_semi);
	Proposition parse_prop(const std::string& file, long pos, long to_end);
	Boolean parse_bool(const std::string& file, long pos, long to_end);
	Expr parse_expr(size_t& token_idx, Token delim = Token());
	ItemId parse_item(const Token& token);
	ItemId parse_item(const std::string& str, int line_num = -1);
	Tk parse_tk(const std::string& str);
	std::string substr(const std::string& str, long from, long to);
	int parse_int(const std::string& str);
	void add_child(Item& parent, Item& child, bool locked);

	ParsedItem parse_item_info(const std::string& file, long pos, long& to_end);
	Item& parse_item_info(BasicItemKind kind, const std::vector<std::string>& arguments);
	Item* parse_item_label(const std::string& label, BasicItemKind expected_kind = BasicItemKind::None);

	template <class UnaryPredicate>
	long find_if(const std::string& str, long pos, long end, UnaryPredicate pred, bool error = true);
	long find(const std::string& str, long pos, long end, char character, bool error = true);
	long find_last(const std::string& str, long pos, long end, char character, bool error);
	std::vector<std::string> split(const std::string& str, long from, long to, char character);

	int line_num;
	std::vector<Token> tokens;
	std::string cur_str;

	std::unordered_map<std::string, ItemId> categories;
	std::unordered_map<std::string, ItemId> item_label_map;
};


#endif //PROJECT_KNOWLEDGEPARSER_H*/
