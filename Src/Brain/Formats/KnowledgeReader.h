/*#ifndef WHAT_KNOWLEDGEREADER_H
#define WHAT_KNOWLEDGEREADER_H

#include "Knowledge/Causal.h"
#include "Knowledge/Trait.h"
#include "Goal.h"
#include "Log.h"
#include "BoxUtil.h"

#include <hopscotch_map.h>
#include <spacy/spacy>
#include <unordered_map>
#include <variant>

enum class Dep {
	UNKNOWN,
	acomp,
	advcl,
	advmod,
	aux,
	auxpass,
	prep,
	det,
	//nmod,
	nsubj,
	//nsubjpass,
	obj,
	poss,
	punct
};

enum class Aux {
	None,
	Must,
	Will
};

enum class Preposition {
	On,
	To
};

enum class Det {
	None,
	A,
	The
};

// Reads knowledge from natural language
class KnowledgeReader {
public:

	struct ReadResults {
		std::vector<Box<Goal>> goals;
		Vec<Pair<ItemId, Box<MutTrait>>> traits;
		Vec<Box<MutCausal>> causals;
	};

	static inline KnowledgeReader create(Log& log) {
		return KnowledgeReader(log);
	}

	ReadResults read(const std::string& text);

	static constexpr uint64_t hash(const char* str);

private:
	KnowledgeReader(Log& log);

	//typedef std::variant<BasicItemKind, Attribute> RefType;

	struct Noun {
		Det det = Det::None;
		std::string text;
		Tk ref; //RefType ref = BasicItemKind::None;
		int coref = -1;
		float quantity = std::numeric_limits<float>::quiet_NaN();
		MaybeBox<Noun> possessive;
	};

	struct Clause {
		std::variant<BasicAction::Kind, Tk> root;
		Aux aux = Aux::None;
		Box<Clause> condition;

		MaybeBox<Noun> subject;
		MaybeBox<Noun> object;
		std::map<Preposition, MaybeBox<Noun>> p_objects;

		const MaybeBox<Noun>& target() const {
			return object.get() == nullptr ? subject : object;
		}
	};

	Clause parse_clause(const Spacy::Token& token, const Clause* parent = nullptr);
	MaybeBox<Noun> parse_noun(const Spacy::Token& token);
	float parse_quantity(const Spacy::Token& token);

	template<typename T>
	T lookup(const Spacy::Token& token, const tsl::hopscotch_map<std::string, T>& vocab_map, const std::string& key_type);
	Dep lookup_dep(const Spacy::Token& token);

	void parse_goal(const Clause& clause, ReadResults& results);
	void parse_causal(const Clause& clause, ReadResults& results);
	Proposition parse_proposition(const Clause& clause, Tk attrib, ReadResults& results);
	Boolean parse_boolean(const Clause& clause, Tk attrib, ReadResults& results);
	BasicAction parse_action(const Clause& clause, BasicAction::Kind kind, ReadResults& results);
	ItemId expect_singular_item(const Noun* noun, ReadResults& results);
	ItemId expect_singular_item(const Clause& clause, Preposition prep, ReadResults& results);
	//ItemId expect_singular_category(const Noun* noun, ReadResults& results);
	//ItemId get_category(BasicItemKind kind, ReadResults& results);

	void set_up_corefs();
	void set_nn_coref(Noun& noun, const Spacy::Token& token);
	void set_pronoun_coref(Noun& noun, const Spacy::Token& token);
	int cur_coref_cluster = 0;
	tsl::hopscotch_map<Tk, int> definite_coref_map;
	tsl::hopscotch_map<int, const Noun*> coref_cluster_map;
	tsl::hopscotch_map<int, ItemId> local_corefs;

	Log& log;
	Spacy::Spacy spacy;
	Spacy::Nlp nlp;

	//std::unordered_map<BasicItemKind, ItemId> category_map;
	//tsl::hopscotch_map<std::string, ItemId> variable_map;
	//Noun* prev_noun = nullptr;
};


#endif //WHAT_KNOWLEDGEREADER_H*/
