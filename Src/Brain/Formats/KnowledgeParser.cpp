/*#include <iostream>
#include <fstream>
#include <algorithm>
#include "KnowledgeParser.h"

std::string gulp(std::ifstream &in) {
	std::string ret;
	char buffer[4096];
	while (in.read(buffer, sizeof(buffer))) {
		ret.append(buffer, sizeof(buffer));
	}
	ret.append(buffer, (unsigned long)in.gcount());
	return ret;
}

std::string remove_comments(const std::string& in) {
	std::string ret;
	size_t pos = 0;
	while (pos < in.size()) {
		size_t next = in.find('\n', pos);
		if (in[pos] == '/' && in[pos + 1] == '/') {
			ret += '\n';
		} else {
			ret += in.substr(pos, next - pos + 1);
		}
		pos = next + 1;
	}
	return ret;
}

std::string KnowledgeParser::trim(const std::string& str, int& lines_after) {
	size_t start = std::string::npos;
	for (size_t i = 0; i < str.size(); i++) {
		if (str[i] == '\n') {
			line_num++;
		} else if (!isspace(str[i])) {
			start = i;
			break;
		}
	}

	if (start == std::string::npos) {
		return "";
	}

	size_t end = str.size();
	for (size_t i = str.size() - 1; i > start; i--) {
		if (str[i] == '\n') {
			lines_after++;
		} else if (!isspace(str[i])) {
			end = i;
			break;
		}
	}

	return str.substr(start, end - start + 1);
}

KnowledgeParser::ParseResult KnowledgeParser::parse_file(std::ifstream& stream) {
	std::string file = remove_comments(gulp(stream));
	auto parse_result = ParseResult();
	size_t pos = 0;
	line_num = 1;
	while (true) {
		long to_semi = find(file, pos, file.size(), ';', false);
		if (to_semi == -1) break;
		long to_colon = find(file, pos, to_semi, ':');
		if (to_colon == -1) break;

		std::string statement = trim(file.substr(pos, to_colon - pos), line_num);
		if (statement == "Trait") {
			parse_result.traits.push_back(parse_trait(file, to_colon + 1, to_semi));
		} else if (statement == "Causal") {
			parse_result.causals.push_back(parse_causal(file, to_colon + 1, to_semi));
		} else if (statement == "Goal") {
			parse_result.goal = parse_prop(file, to_colon + 1, to_semi);
		} else if (statement == "Layout") {
			long end = to_semi;
			parse_result.root = parse_item_info(file, to_colon + 1, end).item;
		} else if (statement == "Category") {
			std::string category_name = trim(file.substr(to_colon + 1, to_semi - to_colon - 1), line_num);
			//categories[category_name] = Item::create(category_name).id();
			assert(false);
		} else {
			print_err("Invalid statement '" + statement + "'");
		}

		if (to_semi == (long)file.size()) break;
		pos = (size_t)(to_semi + 1);
	}

	return parse_result;
}

bool is_symbol(char c) {
	return c == ')' || c == '(' || c == ',';
}

bool is_ident(char c) {
	return isalpha(c) || c == '$';
}

void KnowledgeParser::finish_token(Token::Type type, int line_num) {
	if (!cur_str.empty()) {
		tokens.emplace_back(std::move(cur_str), type, line_num);
		cur_str = "";
	}
}

bool KnowledgeParser::tokenize(const std::string& str, long pos, long to_end) {
	tokens.clear();
	cur_str = "";
	Token::Type cur_type = Token::NONE;
	for (long i = pos; i < to_end; i++) {
		char c = str[i];

		if (c == '/' && str[i + 1] == '/') {
			i = find(str, i, to_end, '\n');
			if (cur_type == Token::IDENT) {
				finish_token(cur_type, line_num);
			}
			line_num++;
			cur_type = Token::NONE;
			continue;
		}

		switch (cur_type) {
			case Token::NONE:
				if (isspace(c)) {
					line_num += c == '\n';
					continue;
				} else if (is_ident(c)) {
					cur_type = Token::IDENT;
				} else if (isdigit(c)) {
					cur_type = Token::NUM;
				} else if (is_symbol(c)) {
					cur_type = Token::SYMBOL;
				} else {
					print_err("Bad character: " + std::string(1, c), line_num);
					return false;
				}
				break;
			case Token::IDENT:
			case Token::NUM:
				if (isspace(c)) {
					finish_token(cur_type, line_num);
					cur_type = Token::NONE;
					line_num += c == '\n';
					continue;
				} else if (is_symbol(c)) {
					finish_token(cur_type, line_num);
					cur_type = Token::SYMBOL;
				} else if (!(isalnum(c) || c == '_' || c == '#' || c == '$')) {
					print_err("Bad character: " + std::string(1, c), line_num);
					return false;
				}
				break;
			case Token::SYMBOL:
				finish_token(cur_type, line_num);
				if (isspace(c)) {
					line_num += c == '\n';
					continue;
				} else if (is_ident(c)) {
					cur_type = Token::IDENT;
				} else if (isdigit(c)) {
					cur_type = Token::NUM;
				} else if (is_symbol(c)) {
					cur_type = Token::SYMBOL;
				} else {
					print_err("Bad character: " + std::string(1, c), line_num);
					return false;
				}
				break;
		}
		cur_str.push_back(c);
	}

	if (cur_type != Token::NONE) {
		finish_token(cur_type, line_num);
	}
	return true;
}

void KnowledgeParser::print_err(const std::string& message, int line_num) {
	std::cout << "Line " << (line_num == -1 ? this->line_num : line_num) << ": " << message << std::endl;
}

Pair<ItemId, Box<MutTrait>> KnowledgeParser::parse_trait(const std::string& file, long pos, long to_semi) {
	long to_eq = find(file, pos, to_semi, '=');
	if (to_eq == -1) return std::make_pair(ItemId(), nullptr);
	long to_tilda = find(file, to_eq, to_semi, '~', false);
	if (to_tilda == -1) to_tilda = to_semi;

	bool negate = (file[to_eq - 1] == '!');
	long to_dot = find(file, pos, to_eq, '.');
	ItemId item = parse_item(trim(file.substr((size_t)pos, to_dot - pos), line_num), line_num);
	Tk attrib = parse_tk(trim(file.substr((size_t)to_dot + 1, to_eq - (size_t)negate - (to_dot + 1)), line_num));
	std::string rhs = file.substr((size_t)to_eq + 1, (size_t)to_tilda - (to_eq + 1));
	double value;
	if (attrib == Tk::Kind) {
		value = (double)parse_item_kind(trim(rhs, line_num));
	} else {
		value = parse_int(rhs);
	}

	return std::make_pair(item, std::make_unique<MutTrait>(attrib, value, negate));
}

Box<MutCausal> KnowledgeParser::parse_causal(const std::string& file, long pos, long to_semi) {
	long to_eq = find(file, pos, to_semi, '=');
	if (to_eq == -1) return nullptr;
	long to_tilda = find(file, to_eq, to_semi, '~', false);
	if (to_tilda == -1) to_tilda = to_semi;
	assert(file[to_eq + 1] == '>');

	tokenize(file, pos, to_eq);
	size_t token_idx = 0;
	Expr expr = parse_expr(token_idx);

	BasicAction cause(BasicAction::None);
	if (expr.token.str == "use") {
		cause = BasicAction(BasicAction::Use, parse_item(expr.args[0].token), parse_item(expr.args[1].token));
	} else if (expr.token.str == "visit") {
		cause = BasicAction(BasicAction::Visit, parse_item(expr.args[0].token));
	} else if (expr.token.str == "imminent") {
		cause = BasicAction(BasicAction::Imminent);
	} else {
		print_err("Not a known action: " + expr.token.str);
		return nullptr;
	}

	Event effect = parse_event(file, to_eq + 2, to_tilda);

	if (to_tilda != to_semi) {
		Proposition condition = parse_prop(file, to_tilda + 1, to_semi);
		return std::make_unique<MutCausal>(cause, effect, condition);
	}
	return std::make_unique<MutCausal>(cause, effect, Proposition());
}

Event KnowledgeParser::parse_event(const std::string& file, long pos, long to_end) {
	if (file[pos] != 'm') {
		return Event(parse_bool(file, pos, to_end));
	}

	tokenize(file, pos, to_end);
	size_t token_idx = 0;
	Expr expr = parse_expr(token_idx);

	if (expr.token.str == "modify") {
		return Event(parse_number(expr.args[0]), parse_number(expr.args[1]));
	} else {
		print_err("Unknown event function: '" + expr.token.str + "'");
	}

	return Event();
}

bool is_cond(char character) {
	return character == '|' || character == '&';
}

Proposition KnowledgeParser::parse_prop(const std::string& file, long pos, long to_end) {
	// TODO: this is not accurate

	Proposition prop;
	bool done = false;
	bool prev_and = false;
	while (!done) {
		long to_cond = find_if(file, pos, to_end, is_cond, false);
		if (to_cond == -1) {
			done = true;
			to_cond = to_end;
		}

		Boolean boolean = parse_bool(file, pos + 1, to_cond);
		if (prev_and) {
			prop.and_bool(boolean);
		} else {
			prop.or_bool(boolean);
		}

		prev_and = file[to_cond] == '&';
		pos = to_cond + 1;
	}

	return prop;
}

Boolean KnowledgeParser::parse_bool(const std::string& file, long pos, long to_end) {
	bool negate = false;
	if (file[pos] == '!') {
		negate = true;
		pos++;
	}

	tokenize(file, pos, to_end);
	size_t token_idx = 0;
	Expr expr = parse_expr(token_idx);

	if (expr.token.str == "true") {
		return Boolean::new_literal(!negate);
	} else if (expr.token.str == "false") {
		return Boolean::new_literal(negate);
	} else if (expr.token.str == "equals") {
		std::vector<Quickmath> quickmath = parse_quickmath(expr);
		return Boolean::math_equals(quickmath[0].get_value(0, 0), quickmath[1].get_value(0, 0), negate);
	} else if (expr.token.str == "lt") {
		std::vector<Quickmath> quickmath = parse_quickmath(expr);
		return Boolean::less_than(quickmath[0].get_value(0, 0), quickmath[1].get_value(0, 0), negate);
	} else if  (expr.token.str == "is_active") {
		return Boolean::new_item_active(parse_item(expr.args[0].token), negate);
	} else {
		print_err("Unknown boolean function: '" + expr.token.str + "'");
	}

	return Boolean::new_literal(true);
}

ItemId KnowledgeParser::parse_item(const KnowledgeParser::Token& token) {
	return parse_item(token.str, token.line_num);
}

ItemId KnowledgeParser::parse_item(const std::string& str, int line_num) {
	if (line_num == -1) line_num = this->line_num;

	auto iter = categories.find(str);
	if (iter != categories.end()) {
		return iter->second;
	} else {
		long to_hash = find(str, 0, str.size(), '#', false);
		if (to_hash == -1) {
			auto iter = item_label_map.find(str);
			if (iter == item_label_map.end()) {
				print_err("Not a known item: " + str, line_num);
				return ItemId();
			}
			return iter->second->id();
		} else {
			assert(false);
			return ItemId();
		}
	}
}

std::vector<Quickmath> KnowledgeParser::parse_quickmath(const Expr& expr) {
	// TODO: this is not accurate

	std::vector<Quickmath> math;

	for (const Expr& arg : expr.args) {
		Number num = parse_number(arg);
		std::string delim = arg.delim.str;

		if (delim.empty() || delim == ",") {
			math.emplace_back();
			math.back().add_value(num);
		} else if (delim == "+") {
			math.back().add_value(num);
		} else if (delim == "*") {
			math.back().mul_value(num);
		} else {
			print_err("What is this: '" + delim + "'", arg.delim.line_num);
		}
	}

	return math;
}

Number KnowledgeParser::parse_number(const Expr& expr) {
	if (expr.token.str == "stat") {
		return Number(parse_item(expr.args[0].token));
	} else {
		return Number(parse_int(expr.token.str));
	}
}

BasicItemKind KnowledgeParser::parse_item_kind(const std::string& str) {
	static std::unordered_map<std::string, BasicItemKind> item_kind_map = {
		{"Hub", BasicItemKind::Hub},
		{"Shed", BasicItemKind::Shed},
		{"Key", BasicItemKind::Key},
		{"Lever", BasicItemKind::Lever}
	};
	auto iter = item_kind_map.find(str);
	if (iter == item_kind_map.end()) {
		print_err("Unknown item type: " + str, line_num);
		return BasicItemKind::None;
	}
	return iter->second;
}

Attribute KnowledgeParser::parse_attribute(const std::string& str) {
	static std::unordered_map<std::string, Attribute> attribute_map = {
		{"active", Attribute::Active},
		{"stat", Attribute::Stat},
		{"kind", Attribute::Kind}
	};
	auto iter = attribute_map.find(str);
	if (iter == attribute_map.end()) {
		print_err("Unknown attribute: " + str, line_num);
		return Attribute::None;
	}
	return iter->second;
}

KnowledgeParser::Expr KnowledgeParser::parse_expr(size_t& token_idx, Token delim) {
	Token& token = tokens[token_idx];
	if (token_idx < tokens.size() - 1 && tokens[token_idx + 1].str == "(") {
		Expr new_expr(token, std::move(delim));
		if (tokens[token_idx + 2].str != ")") {
			Token next_delim;
			while (true) {
				token_idx += 2;
				new_expr.args.push_back(parse_expr(token_idx, next_delim));
				next_delim = tokens[token_idx + 1];
				if (!(next_delim.str == "," || next_delim.str == "+" || next_delim.str == "*")) {
					break;
				}
			}
		}
		token_idx++;
		return new_expr;

	} else {
		return Expr(token, Token());
	}
}

template <class UnaryPredicate>
long KnowledgeParser::find_if(const std::string& str, long pos, long end, UnaryPredicate pred, bool error) {
	auto iter = std::find_if(str.begin() + pos, str.begin() + end, pred);
	if (iter >= str.begin() + end) {
		if (error) print_err("Unexpected character", line_num);
		return -1;
	}
	return iter - str.begin();
}

long KnowledgeParser::find(const std::string& str, long pos, long end, char character, bool error) {
	auto iter = std::find(str.begin() + pos, str.begin() + end, character);
	if (iter >= str.begin() + end) {
		if (error) print_err("Expected '" + std::string(1, character) + "'", line_num);
		return -1;
	}
	return iter - str.begin();
}

long KnowledgeParser::find_last(const std::string& str, long pos, long end, char character, bool error) {
	auto rbegin = std::make_reverse_iterator(str.begin() + end);
	auto rend = std::make_reverse_iterator(str.begin() + pos);
	auto iter = std::find(rbegin, rend, character);
	if (iter >= rend) {
		if (error) print_err("Expected '" + std::string(1, character) + "'", line_num);
		return -1;
	}
	return (iter + 1).base() - str.begin();
}

std::string KnowledgeParser::substr(const std::string& str, long from, long to) {
	return trim(str.substr((size_t)from, (size_t)(to - from)), line_num);
}

int KnowledgeParser::parse_int(const std::string& str) {
	try {
		return (uint32_t)std::stoi(str);
	} catch (std::invalid_argument&) {
		print_err("Not an integer: " + str, line_num);
		return 0;
	}
}

std::vector<std::string> KnowledgeParser::split(const std::string& str, long from, long to, char character) {
	std::vector<std::string> result;
	long prev = from;
	long to_comma = find(str, from, to, character, false);
	while (to_comma != -1) {
		result.push_back(substr(str, prev, to_comma));
		prev = to_comma + 1;
		to_comma = find(str, prev, to, character, false);
	}
	result.push_back(substr(str, prev, to));
	if (result.back().empty()) result.pop_back();
	return result;
}

bool not_alnum(char c) {
	return !(isalnum(c) || isspace(c));
}
bool is_end(char c) {
	return c == ',' || c == '}' || c == ';';
}
bool is_brack_or_end(char c) {
	return c == '(' || c == '[' || c == '{' || is_end(c);
}

KnowledgeParser::ParsedItem KnowledgeParser::parse_item_info(const std::string& file, long pos, long& to_end) {
	long to_notalnum = find_if(file, pos, to_end, not_alnum);
	BasicItemKind kind = parse_item_kind(trim(substr(file, pos, to_notalnum), line_num));

	long to_brack = find_if(file, to_notalnum, to_end, is_brack_or_end, false);
	if (to_brack == -1 || is_end(file[to_brack])) to_brack = to_notalnum;

	std::string label;
	if (file[to_brack] == '[') {
		long to_close = find(file, to_brack, to_end, ']', true);
		if (to_close == -1) return ParsedItem(nullptr, 0);
		label = substr(file, to_brack + 1, to_close);
		to_brack = find_if(file, to_close, to_end, is_brack_or_end, false);
		if (to_brack == -1) to_brack = to_end;
	}

	std::vector<std::string> arguments;
	if (file[to_brack] == '(') {
		long to_close = find(file, to_brack, to_end, ')', true);
		if (to_close == -1) return ParsedItem(nullptr, 0);
		arguments = split(file, to_brack + 1, to_close, ',');
		to_brack = find_if(file, to_close, to_end, is_brack_or_end, false);
		if (to_brack == -1) to_brack = to_end;
	}

	Item& item = parse_item_info(kind, arguments);
	if (!label.empty()) item_label_map[label] = &item;

	if (file[to_brack] == '{') {
		std::vector<ParsedItem> children;
		while (file[to_brack] != '}') {
			long child_end = to_end;
			children.push_back(parse_item_info(file, to_brack + 1, child_end));
			to_brack = child_end;
		}
		std::random_shuffle(children.begin(), children.end()); // TODO: use rando
		for (ParsedItem& child : children) {
			if (child.item == nullptr) continue;
			add_child(item, *child.item, child.locked);
			for (int i = 1; i < child.quantity; i++) {
				add_child(item, child.item->clone(), child.locked);
			}
		}
		to_brack++;
	}

	to_end = find_if(file, to_brack, to_end + 1, is_end, true);

	ParsedItem parsed_item(&item, 1);
	long to_not = find(file, to_brack, to_end + 1, '!', false);
	if (to_not != -1) parsed_item.locked = true;
	long to_times = find(file, to_brack, to_end + 1, '*', false);
	if (to_times != -1) parsed_item.quantity = parse_int(substr(file, to_times + 1, to_end + 1));

	return parsed_item;
}

Item* KnowledgeParser::parse_item_label(const std::string& label, BasicItemKind expected_kind) {
	auto iter = item_label_map.find(label);
	if (iter == item_label_map.end()) {
		print_err("Item not found: " + label);
	} else if (expected_kind != BasicItemKind::None && iter->second->info()->kind() != BasicItemKind::Hub) {
		print_err("Item expected to be " + to_string(expected_kind));
	} else {
		return iter->second;
	}
	return nullptr;
}

Item& KnowledgeParser::parse_item_info(BasicItemKind kind, const std::vector<std::string>& arguments) {
	switch (kind) {
		case BasicItemKind::Hub:
			return Item::create<Hub>(arguments.empty() ? 0 : parse_int(arguments[0]));
		case BasicItemKind::Shed:
			return Item::create<Shed>();
		case BasicItemKind::Key:
			if (arguments.empty() || arguments[0] == "NULL") {
				return Item::create<Key>(nullptr);
			}
			return Item::create<Key>((Hub*)parse_item_label(arguments[0], BasicItemKind::Hub));
		case BasicItemKind::Lever:
			if (arguments.size() < 2 || arguments[0] == "NULL") {
				return Item::create<Lever>(nullptr, 0);
			}
			return Item::create<Lever>(parse_item_label(arguments[0])->info(), parse_int(arguments[1]));
		default:
			assert(false);
			return Item::create<Shed>();
	}

}

void KnowledgeParser::add_child(Item& parent, Item& child, bool locked) {
	switch (parent.info()->kind()) {
		case BasicItemKind::Hub:
			if (locked) {
				((Hub*)parent.info())->add_locked(*child.info());
			} else {
				((Hub*)parent.info())->add_exit(*child.info());
			}
			break;
		case BasicItemKind::Shed:
			((Shed*)parent.info())->add_item(*child.info());
			break;
		case BasicItemKind::Key:
		case BasicItemKind::Lever:
			print_err(to_string(parent.info()->kind()) + "s can't have children");
			break;
		default: assert(false);
	}
}*/
