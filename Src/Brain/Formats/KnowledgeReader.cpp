/*#include "KnowledgeReader.h"
#include "Knowledge/ItemId.h"
#include <iostream>
#include <variant>

using namespace Spacy;

KnowledgeReader::KnowledgeReader(Log& log): log(log), nlp(spacy.load("en_core_web_sm")) {
	set_up_corefs();
}

void KnowledgeReader::set_up_corefs() {
	//assert(neuralcoref.get() != nullptr);
	//std::vector<PyObjectPtr> args = std::vector<PyObjectPtr>({nlp.get_pyobject()});
	//Python::call_method<PyObjectPtr>(neuralcoref, "add_to_pipe", args);
}

KnowledgeReader::ReadResults KnowledgeReader::read(const std::string& text) {
	auto doc = nlp.parse(text);
	//assert(nlp.vocab().strings().add("Test hash!") == h("Test hash!"));

	ReadResults read_results;

	std::vector<Clause> clauses;
	for (Span& sentence : doc.sents()) {
		clauses.push_back(parse_clause(sentence.root()));

		if (clauses.back().aux == Aux::Must) {
			parse_goal(clauses.back(), read_results);
		} else if (clauses.back().aux == Aux::Will) {
			parse_causal(clauses.back(), read_results);
		}
	}

	definite_coref_map.clear();
	coref_cluster_map.clear();
	local_corefs.clear();

	return read_results;
}

const tsl::hopscotch_map<std::string, Dep> dep_vocab {
	{ "acomp",     Dep::acomp },
	{ "advcl",     Dep::advcl },
	{ "advmod",    Dep::advmod },
	{ "aux",       Dep::aux },
	{ "auxpass",   Dep::auxpass },
	{ "prep",      Dep::prep },
	{ "det",       Dep::det },
	//{ "nmod",      Dep::nmod },
	{ "nsubj",     Dep::nsubj },
	{ "nsubjpass", Dep::nsubj },
	{ "dobj",      Dep::obj },
	{ "pobj",      Dep::obj },
	{ "poss",      Dep::poss },
	{ "punct",     Dep::punct }
};

const tsl::hopscotch_map<std::string, std::variant<BasicAction::Kind, Tk>> action_vocab {
//	{ "visit", BasicAction::Visit },
	{ "use",   BasicAction::Use },
	{ "pull",  BasicAction::Touch },

	{ "activate", Tk::Active }
//	{ "find",     Boolean::ItemFound },
//	{ "reduce",   Boolean::LessThan },
//	{ "set",      Boolean::MathEquals }
};

const tsl::hopscotch_map<std::string, Tk> adjective_vocab {
	{ "active", Tk::Active },
//	{ "lower",  Boolean::LessThan },
//	{ "equal",  Boolean::MathEquals }
};

const tsl::hopscotch_map<std::string, Aux> aux_vocab {
	{ "must", Aux::Must },
	{ "will", Aux::Will }
};

// nullptr_t is a pronoun of unknown owner
const tsl::hopscotch_map<std::string, Tk> item_vocab {
	{ "hub",   Tk::Hub },
	{ "shed",  Tk::Shed },
	{ "key",   Tk::Key },
	{ "lever", Tk::Lever },

	{ "activeness", Tk::Active },
	{ "stat",       Tk::Stat },
	{ "kind",       Tk::Kind }
};

const tsl::hopscotch_map<std::string, Preposition> prep_vocab {
	{ "on", Preposition::On },
	{ "to", Preposition::To }
};

const tsl::hopscotch_map<std::string, Det> det_vocab {
	{ "a", Det::A },
	{ "an", Det::A },
	{ "the", Det::The }
};

const std::string& prep_to_string(Preposition prep) {
	switch (prep) {
		case Preposition::On: {
			static std::string s("On");
			return s;
		}
		case Preposition::To: {
			static std::string s("To");
			return s;
		}
		default: {
			assert(false);
			static std::string s;
			return s;
		}
	}
}

KnowledgeReader::Clause KnowledgeReader::parse_clause(const Token& token, const Clause* parent) {
	Clause clause;
	if (parent != nullptr) {
		clause.subject = parent->subject.ref();
		clause.object = parent->object.ref();
	}

	bool be = (token.lemma_() == "be");
	if (!be) clause.root = lookup(token, action_vocab, "Verb");

	auto children = token.children();
	for (const Token& child : children) {
		switch (lookup_dep(child)) {
			case Dep::acomp:  clause.root = lookup(child, adjective_vocab, "Adjective"); assert(be); break;
			case Dep::aux:    clause.aux = lookup(child, aux_vocab, "Aux"); break;
			case Dep::advcl:  clause.condition.reset(new Clause(parse_clause(child))); break;
			case Dep::nsubj:  clause.subject = parse_noun(child); break;
			case Dep::obj:    clause.object  = parse_noun(child); break;
			//case Dep::obl:    clause.object2 = parse_noun(child, Dep::obl); break;
			//case Dep::cop: assert(child.lemma_() == "be"); break;
			case Dep::advmod:  assert(child.lemma_() == "when"); break;
			case Dep::auxpass: assert(child.lemma_() == "be"); break;
			case Dep::prep: {
				clause.p_objects[lookup(child, prep_vocab, "Preposition")] = parse_noun(child.children()[0]);
			} break;
			case Dep::punct: break;
			default: assert(false); break;
		}
	}

	return clause;
}

MaybeBox<KnowledgeReader::Noun> KnowledgeReader::parse_noun(const Token& token) {
	MaybeBox<Noun> noun(std::make_unique<Noun>());

	std::string tag = token.tag_();
	if (tag== "NN") {
		// Nominal Noun
		noun->ref = lookup(token, item_vocab, "Item");
		set_nn_coref(*noun, token);
	} else if (tag == "CD") {
		// Cardinal
		noun->quantity = parse_quantity(token);
	} else if (tag == "PRP") {
		assert(token.lemma_() == "you");
	} else if (tag == "PRP$") {
		set_pronoun_coref(*noun, token);
	} else {
		assert(false);
	}

	noun->text = token.text();

	for (const Token& child : token.children()) {
		switch (lookup_dep(child)) {
			//case Dep::nmod: noun.item = lookup(child, item_vocab, "Item"); break;
			case Dep::det: {
				noun->text = child.text() + " " + noun->text;
			} break;
			case Dep::poss: {
				noun->possessive = parse_noun(child);
				noun->text = child.text() + " " + noun->text;
			} break;
			case Dep::prep: {
				assert(child.lemma_() == "of");
				const auto& grandchildren = child.children();
				noun->ref = lookup(grandchildren[0], item_vocab, "Item");
				noun->text += " " + child.text();
				noun->text += " " + grandchildren[0].text();
			} break;
			default: assert(false); break;
		}
	}

	return noun;
}

void KnowledgeReader::set_nn_coref(Noun& noun, const Token& token) {
	//PyObjectPtr ext = Python::get_attr_value<PyObjectPtr>(token.get_pyobject(), "_");
	//if (Python::get_attr_value<bool>(ext, "in_coref")) {
	//	std::vector<PyObjectPtr> clusters = Python::get_attr_vector<PyObjectPtr>(ext, "coref_clusters");
	//	assert(clusters.size() == 1);
	//	return Python::get_attr_value<int>(clusters[0], "i");
	//}
	//return -1;

	// Get determinant
	Det det = Det::None;
	for (const Token& child : token.children()) {
		switch (lookup_dep(child)) {
			case Dep::det: {
				det = lookup(child, det_vocab, "Article");
			} break;
			default: break;
		}
	}

	switch (det) {
		case Det::A: {
			noun.coref = ++cur_coref_cluster;
			coref_cluster_map[noun.coref] = &noun;
		} break;
		case Det::The: {
			// "the" shares a coref with all other "the"s of the same ref
			auto iter = definite_coref_map.find(noun.ref);
			if (iter == definite_coref_map.end()) {
				noun.coref = ++cur_coref_cluster;
				coref_cluster_map[noun.coref] = &noun;
				definite_coref_map[noun.ref] = noun.coref;
			} else {
				noun.coref = iter->second;
			}
		} break;
		default:
			assert(false);
	}
}

void KnowledgeReader::set_pronoun_coref(Noun& noun, const Token& token) {
	if (cur_coref_cluster == 0) {
		log.error("Mystery pronoun: " + token.lemma_());
		assert(false);
		return;
	}

	noun.coref = cur_coref_cluster;
	const Noun* corefed = coref_cluster_map[noun.coref];
	noun.ref = corefed->ref;
	noun.quantity = corefed->quantity;
	noun.possessive = corefed->possessive.ref();
}

template<typename T>
T KnowledgeReader::lookup(const Token& token, const tsl::hopscotch_map<std::string, T>& vocab_map, const std::string& t) {
	auto iter = vocab_map.find(token.lemma_());
	if (iter == vocab_map.end()) {
		log.error(t + " not known: " + token.lemma_());
		assert(false);
		return T();
	}

	return iter->second;
}

Dep KnowledgeReader::lookup_dep(const Token& token) {
	auto iter = dep_vocab.find(token.dep_());
	if (iter == dep_vocab.end()) {
		log.error("Unrecognized dep: " + token.dep_());
		assert(false);
		return Dep::UNKNOWN;
	}

	return iter->second;
}

// murmor hash, by Austin Appleby, & in the public domain i guess
constexpr uint64_t KnowledgeReader::hash(const char* str) {
	const int seed = 1;
	const uint64_t m = 0xc6a4a7935bd1e995;
	const int r = 47;
	int len = (int)strlen(str);

	uint64_t h = seed ^ (len * m);

	const uint64_t* data = (const uint64_t *)(&str[0]);
	const uint64_t* end = data + (len / 8);

	while(data != end) {
		uint64_t k = *data++;

		k *= m;
		k ^= k >> r;
		k *= m;

		h ^= k;
		h *= m;
	}

	const unsigned char * data2 = (const unsigned char*)data;

	switch(len & 7) {
		case 7: h ^= uint64_t(data2[6]) << 48;
		case 6: h ^= uint64_t(data2[5]) << 40;
		case 5: h ^= uint64_t(data2[4]) << 32;
		case 4: h ^= uint64_t(data2[3]) << 24;
		case 3: h ^= uint64_t(data2[2]) << 16;
		case 2: h ^= uint64_t(data2[1]) << 8;
		case 1: h ^= uint64_t(data2[0]);
			h *= m;
	};

	h ^= h >> r;
	h *= m;
	h ^= h >> r;

	return h;
}

const tsl::hopscotch_map<std::string, int> unit_names {
	{ "zero",       0 },
	{ "one",        1 },
	{ "two",        2 },
	{ "three",      3 },
	{ "four",       4 },
	{ "five",       5 },
	{ "six",        6 },
	{ "seven",      7 },
	{ "eight",      8 },
	{ "nine",       9 },
	{ "ten",       10 },
	{ "eleven",    11 },
	{ "twelve",    12 },
	{ "thirteen",  13 },
	{ "fourteen",  14 },
	{ "fifteen",   15 },
	{ "sixteen",   16 },
	{ "seventeen", 17 },
	{ "eighteen",  18 },
	{ "nineteen",  19 }
};

float KnowledgeReader::parse_quantity(const Token& token) {
	try {
		return stof(token.lemma_());
	} catch (std::invalid_argument& e) {
		return lookup(token, unit_names, "Number");
	} catch (std::out_of_range& e) { // how???
		return std::numeric_limits<float>::infinity();
	}

	assert(false);
	return 0;
}

void KnowledgeReader::parse_goal(const Clause& clause, ReadResults& results)  {
	Box<Goal> goal;
	Visitor v {
		[&](const BasicAction::Kind kind) {
			goal = std::make_unique<ActionGoal>(parse_action(clause, kind, results));
		},
		[&](const Tk kind) {
			goal = std::make_unique<RealizeGoal>(parse_proposition(clause, kind, results));
		},
	};

	if (clause.condition) {
		// you must X when Y? hard
		assert(false);
	}

	std::visit(v, clause.root);
	results.goals.push_back(std::move(goal));
}

void KnowledgeReader::parse_causal(const Clause& clause, ReadResults& results) {
	local_corefs.clear();

	// Causals have to be a boolean, for now
	Tk kind;
	Visitor v1 {
		[&](const BasicAction::Kind kind) {
			assert(false);
		},
		[&](const Tk tk) {
			kind = tk;
		},
	};
	std::visit(v1, clause.root);
	Boolean event = parse_boolean(clause, kind, results);

	// If the condition is an action, it's the "cause". If it's a boolean, it's the "condition".
	BasicAction action;
	Proposition condition;
	Visitor v2 {
		[&](const BasicAction::Kind kind) {
			action = parse_action(clause, kind, results);
		},
		[&](const Tk kind) {
			action = BasicAction(BasicAction::Imminent);
			condition = parse_proposition(*clause.condition, kind, results);
		},
	};
	std::visit(v2, clause.condition->root);

	// Add more conditions based on coref kinds
	for (auto& pair : local_corefs) {
		Tk kind = coref_cluster_map[pair.first]->ref;
		if (kind != Tk::Null) {
			Boolean new_bool(pair.second, Tk::Kind, kind);
			condition.and_bool(new_bool);
		} else {
			assert(false);
		}
	}

	Box<MutCausal> causal = std::make_unique<MutCausal>(action, event, condition);

	results.causals.push_back(std::move(causal));
}

BasicAction KnowledgeReader::parse_action(const Clause& clause, BasicAction::Kind kind, ReadResults& results) {
	switch (kind) {
		case BasicAction::Touch: {
			return BasicAction(kind, expect_singular_item(clause.object.get(), results));
		}
		case BasicAction::Use: {
			ItemId item1 = expect_singular_item(clause.object.get(), results);
			ItemId item2 = expect_singular_item(clause, Preposition::On, results);
			return BasicAction(kind, item1, item2);
		}
		default:
			assert(false);
			return BasicAction();
	}
}

Proposition KnowledgeReader::parse_proposition(const Clause& clause, Tk attrib, ReadResults& results) {
	return Proposition(parse_boolean(clause, attrib, results));
}

Boolean KnowledgeReader::parse_boolean(const Clause& clause, Tk attrib, ReadResults& results) {
	switch (attrib) {
		case Tk::Active: // "Activate"
			return Boolean(expect_singular_item(clause.target().get(), results), Tk::Active, Tk::True);
		default:
			assert(false);
			return Boolean(ItemId(0), Tk::Null, Tk::Null);
	}
}

ItemId KnowledgeReader::expect_singular_item(const Noun* noun, ReadResults& results) {
	if (noun == nullptr) {
		log.error("Expected noun");
		assert(false);
		return ItemId();
	}

	if (noun->quantity == 1 || std::isnan(noun->quantity)) {
		auto iter = local_corefs.find(noun->coref);
		if (iter == local_corefs.end()) {
			ItemId var =  (ItemId)(local_corefs.size() + 1);
			local_corefs[noun->coref] = var;
			return var;
		} else {
			return iter->second;
		}
	}

	log.error("Expected singular item: {}", noun->text);
	assert(false);
	return ItemId();
}

ItemId KnowledgeReader::expect_singular_item(const Clause& clause, Preposition prep, ReadResults& results) {
	auto iter = clause.p_objects.find(prep);
	if (iter != clause.p_objects.end()) {
		return expect_singular_item(iter->second.get(), results);
	}

	log.error("Expected preposition '{}': {}", prep_to_string(prep), clause.object->text);
	assert(false);
	return ItemId();
}*/
