#include <utility>
#include <set>

#include "SemanticMemory.h"
#include "VarUtil.h"

template<class Map, class Key, class Know>
void SemanticMemory::add(Map& map, Key& key, Know& knowledge) {
	map[key].insert(&knowledge).second;
}

template <class Map, class Key, class Know>
void SemanticMemory::remove(Map& map, Key& key, Know& knowledge) {
	auto iter = map.find(key);
	if (iter == map.end()) assert(false);

	auto& set = iter.value();
	set.erase(&knowledge);
	if (set.empty()) {
		map.erase(key);
	}
}

template<class Map, class Key>
auto& SemanticMemory::get(const Map& map, Key& key) const {
	static const auto empty = typename Map::mapped_type();
	auto iter = map.find(key);
	if (iter == map.end()) return empty;
	return iter->second;
}

template<class Map, class Key>
auto SemanticMemory::get_ptr(const Map& map, Key& key) const {
	auto iter = map.find(key);
	if (iter == map.end()) return nullptr;
	return &iter->second;
}

/*void SemanticMemory::add_trait(Trait trait, ItemId item_id) {
	Item& item = Item::get(item_id);

	Trait* existing = trait_map.get(item_id, trait.v().get_key());
	if (existing != nullptr) {
		//const Trait& existing = iter->second;
		if (existing->same(trait)) {
			existing->boost_confidence(trait.get_confidence());
			return;
		} else {
			// TODO: we should delete old_trait maybe?
			item.remove_trait(*existing);
			Trait old_trait = std::move(*existing);
			*existing = std::move(trait);
			item.add_trait(*existing);
			if (hierarchy) hierarchy->update(item_id, &old_trait, *existing);
		}
	} else {
		const Trait& new_trait = trait_map.insert(item_id, std::move(trait));
		item.add_trait(new_trait);
		if (hierarchy) hierarchy->update(item_id, nullptr, new_trait);
	}

	if (trait_map.size() > 10 && hierarchy == nullptr) {
		hierarchy = std::make_unique<Hierarchy>(Hierarchy::create(log));
	}
}*/

void SemanticMemory::add_causal(Causal causal) {
	auto iter = causal_set.find(causal);
	if (iter != causal_set.end()) {
		(*iter)->boost_confidence(causal.get_confidence());
		return;
	}

	Box<Causal> new_causal = std::make_unique<Causal>(std::move(causal));

	add(event_to_cause, new_causal->v().get_event(), *new_causal);
	//add(cause_to_event, new_causal->v().get_cause(), *new_causal);

	/*Vec<ItemId> item_vec = new_causal->v().get_all_items();
	auto item_set = std::unordered_set<ItemId>(item_vec.begin(), item_vec.end());
	for (ItemId item : item_set) {
		if (!item.is_var()) {
			Item::get(item).add_causal(*new_causal);
		}
	}*/

	//if (trait_map.size() > 10) {
		if (hierarchy == nullptr) {
			hierarchy = std::make_unique<Hierarchy>(Hierarchy::create(visible, log));
		} else {
			hierarchy->update(*new_causal);
		}
	//}

	causal_set.insert(std::move(new_causal));
}

// TODO: this function
void SemanticMemory::remove_causal(Causal& causal) {
	assert(false);
	/*auto iter = causal_set.find(causal);
	if (iter == causal_set.end()) return;

	remove(event_to_cause, causal.v().get_event(), causal);
	remove(cause_to_event, causal.v().get_cause(), causal);

	Vec<ItemId> item_vec = causal.v().get_all_items();
	auto item_set = std::unordered_set<ItemId>(item_vec.begin(), item_vec.end());
	for (ItemId item : item_set) {
		Item::get(item).remove_causal(causal);
	}

	// TODO: do we need to update Hierarchy upon remove?

	causal_set.erase(iter);*/
}

Vec<Causal> SemanticMemory::get_causes(const Boolean& event) const {
	Vec<Causal> causes;

	Vec<Boolean> generic_forms = get_generic_forms(event);
	for (Boolean& event_form : generic_forms) {
		for (Causal* cause : get(event_to_cause, event_form)) {
			MutCausal causal = *cause;
			assign_from(causal, causal.get_event(), event);
			causes.push_back(Causal::make(causal));
		}
	}

	// For every category in every cluster in every item in the event, look up more causes.
	// TODO: why?
	/*std::vector<ItemId> items;
	event.get_all_items(items);
	for (ItemId item_id : items) {
		Item& item = Item::get(item_id);
		for (auto& pair : item.get_clusters()) {
			for (Item* category : pair.first->categories()) {
				Event category_event = event;
				replace_item(category_event, item_id, category->id());
				auto& more_causes = get(event_to_cause, category_event);
				for (Causal* category_causal : more_causes) {
					Box<Causal> causal_copy = std::make_unique<Causal>(*category_causal);
					replace_item<Causal>(*causal_copy, category->id(), item_id);
					causal_copy->set_confidence(causal_copy->get_confidence() * pair.second);
					causes.push_back(MaybeBox(std::move(causal_copy)));
				}
			}
		}
	}*/

	// TODO: cache result?
	return causes;
}

KnowledgeRefSet<Causal> SemanticMemory::get_effects(const Action& cause) const {
	assert(false);
	//return get(cause_to_event, cause);
}

/*const Trait* SemanticMemory::get_trait(ItemId item, Tk attrib) const {
	return trait_map.get(item, attrib);

	const KnowledgeSet<EqualsKnowledge>& set = get(attribute_map, std::make_pair(item, attrib));

	EqualsKnowledge* best = nullptr;
	float highest_confidence = 0;
	for (EqualsKnowledge* knowledge : set) {
		if (knowledge->get_confidence() > highest_confidence) {
			best = knowledge;
			highest_confidence = knowledge->get_confidence();
		}
	}

	return best;
}

const TraitMap& SemanticMemory::get_all_traits() const {
	return trait_map;
}*/

const CausalSet& SemanticMemory::get_all_causals() const {
	return causal_set;
}

const Hierarchy* SemanticMemory::get_hierarchy() const {
	return hierarchy.get();
}
