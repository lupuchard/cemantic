#ifndef WHAT_HIERARCHY_H
#define WHAT_HIERARCHY_H

#include "Knowledge/Causal.h"
#include "Knowledge/Trait.h"
#include "Log.h"
#include "Visible.h"
#include <armadillo>
#include <variant>

/// A parent node in the hierarchy.
/// Can either contain 2-3 Cluster children, or any number of Item children.
struct Cluster {
	Cluster(float lambda): lambda(lambda) { }
	Cluster(float lambda, ItemId item, float item_confidence):
		children(Vec<Pair<ItemId, float>>{std::make_pair(item, item_confidence)}), lambda(lambda) { }

	std::variant<std::array<Box<Cluster>, 3>, Vec<Pair<ItemId, float>>> children;
	Vec<Box<Trait>> traits;
	Vec<Box<Causal>> causals;
	Cluster* parent = nullptr;
	float lambda = 0;
	float inserted_dist = 0; // increased with each post-classification insert, when high enough we reclassify cluster

	inline const Trait* get_same_trait_type(const MutTrait& other) {
		if (traits.empty()) {
			return nullptr;
		}

		int64_t left = 0;
		int64_t right = traits.size() - 1;
		while (left <= right) {
			int64_t mid = (left + right) / 2;
			if (traits[mid]->v().compare_type(other)) {
				left = mid + 1;
			} else if (other.compare_type(*traits[mid])) {
				right = mid - 1;
			} else {
				return traits[mid].get();
			}
		}

		return nullptr;
	}

	inline Vec<Pair<ItemId, float>>* subitems() {
		return std::get_if<Vec<Pair<ItemId, float>>>(&children);
	}
	inline const Vec<Pair<ItemId, float>>* subitems() const {
		return std::get_if<Vec<Pair<ItemId, float>>>(&children);
	}

	inline std::array<Box<Cluster>, 3>* triple() {
		return std::get_if<std::array<Box<Cluster>, 3>>(&children);
	}
	inline const std::array<Box<Cluster>, 3>* triple() const {
		return std::get_if<std::array<Box<Cluster>, 3>>(&children);
	}

	/*inline std::vector<Item*> categories() {
		// TODO: store categories?
		std::vector<Item*> cats;
		auto items = subitems();
		if (items == nullptr) {
			return cats;
		}
		for (auto& pair : *items) {
			Item& item = Item::get(pair.first);
			if (item.is_category()) {
				cats.push_back(&item);
			}
		}
		return cats;
	}*/

	/// True if children are items, false if children are clusters.
	inline bool item_cluster() const {
		return std::holds_alternative<Vec<Pair<ItemId, float>>>(children);
	}
};

class Hierarchy {
public:
	struct Link {
		Link(ItemId item1, ItemId item2, float conf1, float conf2, int32_t link1, int32_t link2, float lambda):
			item1(item1), item2(item2), conf1(conf1), conf2(conf2), link1(link1), link2(link2), lambda(lambda) { }
		ItemId item1, item2;
		float conf1, conf2;
		int32_t link1, link2;
		float lambda;
	};

	static Hierarchy create(Visible& visible, Log& log);
	void insert(ItemId new_item);
	//void update(ItemId item, Trait* old_trait, const Trait& new_trait);
	void update(Causal& causal);

	const Cluster& get_root() const;

	inline const Visible& get_visible() const { return visible; }

private:
	Hierarchy(Visible& visible, Log& log): visible(visible), log(log) { }

	struct ItemData {
		ItemData(ItemId id): id(id) { }

		inline void add_cluster(Cluster& cluster, size_t idx) {
			clusters.emplace(&cluster, idx);

			if (umbrella_cluster == nullptr) {
				umbrella_cluster = &cluster;
			}
		}
		inline void clear_clusters() {
			clusters.clear();
			umbrella_cluster = nullptr;
		}

		inline void update_traits(const Visible& visible) {
			traits.clear();
			for (Tk token : visible.item_attributes(id)) {
				traits.push_back(Trait::make(token, visible.item_attribute(id, token)));
			}
			for (Tk token : visible.item_children(id)) {
				traits.push_back(Trait::make(token, visible.item_child(id, token)));
			}
			std::sort(traits.begin(), traits.end());
		}

		Vec<Trait> traits;
		Vec<GenericCausal> causals;
		std::map<Cluster*, size_t> clusters;
		Cluster* umbrella_cluster = nullptr;
		ItemId id;
	};

	ItemData& item_data(ItemId item);
	ItemData& new_item_data(ItemId item);
	void prepare_data();
	void prepare_data(Cluster& cluster);
	void prepare_data(Cluster& cluster, std::unordered_map<ItemData*, float>& data_set);
	void remove(int32_t link);
	void insert(ItemData& new_item, Cluster& cluster, bool should_fuzz);
	void reclassify(ItemData* new_item, Cluster& cluster);
	bool add_dist(Cluster& cluster, ItemData& item, float dist);

	int32_t slink_clusterize();
	int32_t clink_clusterize();
	int32_t to_linkage_matrix(const arma::uvec& pies, const arma::fvec& lambdas);
	Cluster to_cluster(int32_t link);
	void clusterization_postprocessing(Cluster& root);
	void determine_guardianship(Cluster& cluster);
	void fuzz_items(Cluster& cluster);
	float fuzz_items(ItemData& item, Cluster& cluster);
	void fuzz_items(Vec<Pair<float, Cluster*>>& insertions, ItemData& item, Cluster& cluster, Cluster* from);
	int32_t repr_to_link(uint32_t repr, const Vec<int32_t>& lm_indices, ItemId& item, float& conf);

	void gen_cluster_knowledge(Cluster& cluster);
	template<typename C1, typename C2>
	Vec<Box<Trait>> trait_intersect(const C1& traits1, const C2& traits2);
	template<typename C1, typename C2>
	Vec<Box<Causal>> causal_intersect(const C1& causals1, const C2& causals2);
	template<typename C1, typename C2>
	Vec<Box<Trait>> trait_union(const C1& traits1, const C2& traits2, float weight1);
	template<typename C1, typename C2>
	Vec<Box<Causal>> causal_union(const C1& traits1, const C2& traits2, float weight1);

	void merge_traits(Vec<Box<Trait>>& traits, const Trait& trait1, const Trait& trait2, float confidence);

	template<typename T>
	void push_knowledge(Vec<Box<T>>& knowledge_set, const T& knowledge, float conf);

	float dist(const ItemData& item1, const ItemData& item2);
	float dist(const ItemData& item, const Cluster& cluster);

	Visible& visible;
	Log& log;
	Box<Cluster> root_cluster;
	Map<ItemId, Box<ItemData>> item_data_map;
	Vec<Pair<ItemData*, float>> data;
	Vec<Box<Link>> linkage_matrix;
	std::vector<size_t> unused_links;
};

#endif //WHAT_HIERARCHY_H
