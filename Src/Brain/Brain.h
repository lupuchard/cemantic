#ifndef CRAB_BRAIN_H
#define CRAB_BRAIN_H

#include "SemanticMemory.h"
#include "Rando.h"
#include "Sim.h"
#include "Goal.h"
#include "EpisodicMemory.h"
#include "Evaluator.h"
#include "Log.h"

class Brain {
public:
	Brain(Log& log, Sim& sim, SemanticMemory& knowledge, std::unique_ptr<Goal> base_goal);
	void execute();
	bool execute_one();

	const SemanticMemory& get_knowledge() const;
	const Sim& get_sim() const;

	const Goal& get_base_goal() const;
	const Goal* get_current_goal() const;


	inline const std::vector<Action>& were_action_choices() const { return w_action_choices; }
	const arma::fvec& were_action_weights() const { return w_action_weights; }

private:
	bool pop_goal();
	void push_subgoal(Box<Goal> goal);
	Box<Goal> get_subgoal(RealizeGoal& goal);
	ItemId perform_action(Action action);
 	bool goal_complete(RealizeGoal& goal);

 	Action choose_action();
	std::pair<float, const Boolean*> compute_ease(const CnfFormula& prop);

	void find_patterns(const Action& action);
	const Moment& remember_moment(const Action& prev_action, const Action& next_action);

	//void examine_items(Shed& shed);
	void examine_action_result(MemoryDiff& memory_diff, const Moment& prev_moment, const Action& action);
	bool did_action_occur(const MutCausal& causal, MemoryDiff& memory_diff);

	//void add_basic_knowledge(ItemId item);
	//void find_categories_for_item(ItemId item);

	Box<Goal> base_goal;
	Goal* current_goal = nullptr;

	Rando rando;
	Sim& sim;
	SemanticMemory& sem;
	EpisodicMemory memory;
	Evaluator evaluator;

	bool done = false;
	size_t iteration = 0;
	Action prev_action;

	Vec<Action> w_action_choices;
	arma::fvec w_action_weights;

	Log& log;
};


#endif //CRAB_BRAIN_H
