#include "Brain.h"
#include <Formats/KnowledgeParser.h>
#include <fmt/core.h>

Brain::Brain(Log& log, Sim& sim, SemanticMemory& knowledge, Box<Goal> base_goal):
	base_goal(std::move(base_goal)), current_goal(this->base_goal.get()),
	rando(/*(uint64_t)time(nullptr)*/ 14), sim(sim), sem(knowledge), memory(sim), evaluator(sim), log(log) {}

const SemanticMemory& Brain::get_knowledge() const {
	return sem;
}

const Sim& Brain::get_sim() const {
	return sim;
}

const Goal& Brain::get_base_goal() const {
	return *base_goal;
}

const Goal* Brain::get_current_goal() const {
	return current_goal;
}

void Brain::execute() {
	while (!done) {
		done = execute_one();
	}
}

bool Brain::execute_one() {
	iteration++;
	log.info("ITERATION {}", iteration);
	//add_basic_knowledge(sim.get_current_location());

	assert(current_goal != nullptr);
	if (current_goal->get_kind() == Goal::DoAction) {
		Action action = ((ActionGoal*)current_goal)->get();
		remember_moment(prev_action, action); // form memory immediately before taking next action

		ItemId needed = perform_action(action);
		if (!needed.is_null()) {
			assert(false);
			//push_subgoal(std::make_unique<RealizeGoal>(Boolean::new_item_found(needed)));
			return false;
		}
		if (pop_goal()) return true;

		// verify current patterns, generate new patterns
		find_patterns(action);
		prev_action = action;
	} else {
		RealizeGoal& realize_goal = *(RealizeGoal*)current_goal;

		// check if goal complete
		if (goal_complete(realize_goal)) {
			log.info("Goal complete!");
			if (pop_goal()) return true;
		} else {
			push_subgoal(get_subgoal(realize_goal));
		}
	}

	// TODO: issue
	// Issue: Need to construct knowledge for every observed property of every observed item
	// Now for real, because it's needed to property categorize (Hub#1 should be a $Hub)

	return false;
}

bool Brain::pop_goal() {
	Goal* parent = current_goal->get_parent();
	if (parent != nullptr) {
		parent->remove_subgoal(*current_goal);
		current_goal = parent;
		return false;
	} else {
		log.info("We done!");
		return true;
	}
}

void Brain::push_subgoal(std::unique_ptr<Goal> goal) {
	goal->set_parent(*current_goal);
	Goal* temp = goal.get();
	current_goal->add_subgoal(std::move(goal));
	current_goal = temp;
	log.info("New goal: {}", current_goal->to_string(sim));
}


struct PossibleSubgoal {
	explicit PossibleSubgoal(Causal cause): causal(std::move(cause)) { }
	Causal causal;
	double weight = 0;
	const Boolean* hardest_term_of_easiest_clause_in_condition = nullptr;
};
Box<Goal> Brain::get_subgoal(RealizeGoal& goal) {
	// For every unsolved boolean in a proposition, get the cause
	const CnfFormula& prop = goal.get();
	std::vector<PossibleSubgoal> causes;
	for (const Boolean& term : prop) {
		if (rando.rand_float() > evaluator.evaluate_bool(term)) {
			for (auto& cause : sem.get_causes(term)) {
				causes.emplace_back(std::move(cause));
			}
		}
	}
	if (causes.empty()) {
		return std::make_unique<ActionGoal>(choose_action());
	}

	// To pick from the list of knowledges, we have to consider:
	//   Confidence in the knowledge
	//   Ease of completing the preconditions

	// TODO: clean up / place in Evaluator (call this section choose_subgoal())
	double total_weight = 0.1; // 0.1 adds effective low-confidence wander as possible subgoal
	for (auto& possible_goal : causes) {
		float confidence = possible_goal.causal.get_confidence();
		std::pair<float, const Boolean*> ease_and_term = compute_ease(possible_goal.causal.v().get_condition());
		possible_goal.weight = confidence * ease_and_term.first;
		possible_goal.hardest_term_of_easiest_clause_in_condition = ease_and_term.second;
		total_weight += possible_goal.weight;
	}
	double choice = rando.rand_float() * total_weight;

	double cumulative_weight = 0;
	for (auto& possible_goal : causes) {
		cumulative_weight += possible_goal.weight;
		if (choice < cumulative_weight) {
			//sem.add_causal(std::make_unique<Causal>(possible_goal.causal));

			if (
				possible_goal.hardest_term_of_easiest_clause_in_condition != nullptr &&
				rando.rand_float() > evaluator.evaluate_bool(possible_goal.causal.v().get_condition())
			) {
				return std::make_unique<RealizeGoal>(*possible_goal.hardest_term_of_easiest_clause_in_condition);
			} else {
				//return std::make_unique<ActionGoal>(possible_goal.causal->get_cause());
			}
		}
	}

	return std::make_unique<ActionGoal>(choose_action());
}

Action Brain::choose_action() {
	// Enumerate all possible actions
	Vec<Action> possible_actions;
	for (ItemId item : sim.all_items()) {
		if (item.is_null()) continue;
		possible_actions.push_back(Action(item));
	}

	assert(!possible_actions.empty());

	CnfFormula& goal = ((RealizeGoal*)current_goal)->get();

	// Estimate value of each action
	arma::fvec action_values(possible_actions.size());
	for (size_t i = 0; i < action_values.size(); i++) {
		Action& action = possible_actions[i];
		float impact = 0;
		float max_confidence = 0;
		const KnowledgeRefSet<Causal>& causals = sem.get_effects(action);
		for (const Causal* causal : causals) {
			float confidence = causal->get_confidence() * evaluator.evaluate_bool(causal->v().get_condition());
			if (confidence > 0) {
				impact += std::max(evaluator.evaluate_impact(causal->v().get_event(), goal) * confidence, 0.f);
				max_confidence = std::max(max_confidence, causal->get_confidence());
			}
		}
		impact += (1 - max_confidence) / 100; // TODO: more sophisticate than this
		action_values[i] = impact;
	}

	// Pick among most valuable actions
	action_values.transform([](float val) { return val * val; });
	action_values = arma::normalise(action_values, 1);

	w_action_choices = possible_actions;
	w_action_weights = action_values;

	return possible_actions[rando.weighted_random_select(action_values)];
}

// returns ease and the hardest term of the easiest clause
std::pair<float, const Boolean*> Brain::compute_ease(const CnfFormula& prop) {
	/// TODO: ha this can be improved

	float ease = 0;
	float unsolved_of_easiest_clause = std::numeric_limits<float>::infinity();
	const Boolean* hardest_term_of_easiest_clause = nullptr;
	for (size_t i = 0; i < prop.num_clauses(); i++) {
		float unsolved_in_clause = 0;
		float confidence_of_hardest_term = 1;
		const Boolean* hardest_term = nullptr;
		for (size_t j = 0; j < prop.num_terms(i); j++) {
			const Boolean& term = prop.get_term(i, j);
			float confidence = evaluator.evaluate_bool(term);
			unsolved_in_clause += (1 - confidence);
			if (confidence < confidence_of_hardest_term) {
				confidence_of_hardest_term = confidence;
				hardest_term = &term;
			}
		}

		if (unsolved_in_clause == 0) {
			return std::make_pair(1, nullptr);
		}

		if (unsolved_in_clause < unsolved_of_easiest_clause) {
			unsolved_of_easiest_clause = unsolved_in_clause;
			hardest_term_of_easiest_clause = hardest_term;
		}

		ease += (1 / unsolved_in_clause);
	}

	return std::make_pair(std::min(ease, 1.f), hardest_term_of_easiest_clause);
}

ItemId Brain::perform_action(Action action) {
	/*std::vector<ItemId*> items;
	action.get_all_items_mut(items);
	for (ItemId* item_id : items) {
		Item& item = Item::get(*item_id);
		if (item.is_category()) {
			std::vector<ItemId> basic_items;
			get_basic_items(item, basic_items);
			if (basic_items.empty()) {
				log.push_back("Can't perform action: " + action.to_string() + ", need '" + item.to_string() + "'");
				return *item_id;
			} else if (basic_items.size() == 1) {
				*item_id = basic_items[0];
			} else {
				// TODO: choose by proximity, ease? instead of random?
				return basic_items[rando.rand(0, basic_items.size())];
			}
		}
	}*/

	log.info("Performing action: {}", action.to_string(sim));
	sim.perform_action(action);
	return ItemId();
}

bool Brain::goal_complete(RealizeGoal& goal) {
	return evaluator.evaluate_bool(goal.get()) > 0.8f;
}

void Brain::find_patterns(const Action& action) {
	/*if (action.get_kind() == BasicAction::Visit) {
		BasicItemInfo& loc = sim.get_current_location();
		if (Item::get(loc.id()).get_traits().empty()) {
			add_basic_knowledge(loc);
			if (loc.kind() == BasicItemKind::Shed) {
				examine_items((Shed&)loc);
			}
		}
	} else {*/
		std::unique_ptr<MemoryDiff> memory_diff = memory.get_diff(memory.current());

		/*for (ItemId item : memory_diff->get_changed_items(Attribute::Active)) {
			sem.add_trait(Trait::make(Attribute::Active, Item::get(item).info()->active() ? 1.f : 0.f, 1), item);
		}
		for (ItemId item : memory_diff->get_changed_items(Attribute::Stat)) {
			sem.add_trait(Trait::make(Attribute::Stat, Item::get(item).info()->stat(), 1), item);
		}
		for (ItemId item : memory_diff->get_changed_items(Attribute::Kind)) {
			sem.add_trait(Trait::make(Attribute::Kind, (float)Item::get(item).info()->kind(), 1), item);
		}*/

		examine_action_result(*memory_diff, memory.current(), action);
	//}
}

// Try every item at the location with every global
/*void Brain::examine_items(Shed& shed) {
	for (BasicItemInfo* item : shed.children()) {
		find_categories_for_item(Item::get(item->id()));
		add_basic_knowledge(*item);
	}
}

void Brain::add_basic_knowledge(BasicItemInfo& item) {
	sem.add_trait(Trait::make(Attribute::Active, item.active() ? 1.f : 0.f, 1), item.id());
	sem.add_trait(Trait::make(Attribute::Stat, item.stat(), 1), item.id());
	sem.add_trait(Trait::make(Attribute::Kind, (float)item.kind(), 1), item.id());
}

void Brain::find_categories_for_item(Item& item) {

}*/

// look up expected result of 'action'
void Brain::examine_action_result(MemoryDiff& memory_diff, const Moment& prev_moment, const Action& action) {

	bool any_failed = false;
	auto effects = sem.get_effects(action);
	for (Causal* parent_knowledge : effects) {
		if (evaluator.evaluate_bool(parent_knowledge->v().get_condition(), TraitMap::from(prev_moment.state)) < 0.1f) continue; // TODO: magic number
		if (did_action_occur(*parent_knowledge, memory_diff)) continue;

		any_failed = true;

		// Expected result did not occur, find knowledge that lead us to believe it
		// would by picking out the lowest confidence related knowledge and delete it

		std::vector<Causal*> related_knowledge;
		related_knowledge.push_back(parent_knowledge);
		//get_related_globals_knowledge(*parent_knowledge, related_knowledge);

		float min = 1;
		Causal* bad_knowledge = nullptr;
		for (Causal* related : related_knowledge) {
			if (related->get_confidence() < min) {
				min = related->get_confidence();
				bad_knowledge = related;
			}
		}

		if (bad_knowledge == nullptr) {
			log.info("WARNING: Result unexpected, but no bad knowledge found!");
		} else {
			log.info("Result unexpected, removing {}", bad_knowledge->to_string(sim));
			sem.remove_causal(*bad_knowledge);
		}
	}


	// Find new knowledge
	if (any_failed || effects.empty()) {
		// TODO: replace with CausalClimber
		/*EventDiscerner event_discerner(memory, log);
		Vec<MutCausal> new_causals = event_discerner.discern_event(action);
		for (auto& causal : new_causals) {
			log.info("New knowledge discerned: {}", causal.to_string(sim));
			sem.add_causal(Causal(causal));
		}*/
	}
}

bool Brain::did_action_occur(const MutCausal& knowledge, MemoryDiff& memory_diff) {
	const Boolean& event = knowledge.get_event();

	//if (event.get_kind() == Event::Realize) {

		// TODO: were we going to use the memory_diff for this?
		// we CAN'T use the memory_diff for this because evaluate_bool does computmations?
		// unless we implement passing a memory_diff into the evaluate methods?
		return evaluator.evaluate_bool(event) > 0.9f; // TODO: magic number
	/*} else if (event.get_kind() == Event::Modify) {
		auto& modify = *event.get_modify();
		ItemId item = modify.first.get_item();
		const _Attributes* bef_item = memory_diff.get_before(item);
		const _Attributes* aft_item = memory_diff.get_after(item);
		if (aft_item != nullptr) {

			double diff = (*aft_item)[Attribute::Stat] - (*bef_item)[Attribute::Stat];
			std::pair<float, float> expected_diff = evaluator.evaluate_number(modify.second);

			double accuracy = 0;
			if (diff > expected_diff.first) {
				accuracy = expected_diff.first / diff;
			} else {
				accuracy = diff / expected_diff.first;
			}

			// what to do in case of low confidence?
			//   just ignore and utilize when failed?

			return accuracy > 0.9f; // TODO: the same magic number
		}
	}*/

	assert(false);
	return false;
}

const Moment& Brain::remember_moment(const Action& prev_action, const Action& next_action) {
	// relevant items include:
	//   current
	//   goals
	//   next action

	/*std::vector<ItemId> relevant;
	BasicItemInfo& current_loc = sim.get_current_location();
	relevant.push_back(&current_loc);
	for (BasicItemInfo* child : current_loc.children()) {
		relevant.push_back(child);
	}

	relevant.push_back(&sim.get_current_location());*/

	// TODO: select only relevant items
	std::vector<ItemId> items;
	next_action.get_all_items(items);

	/*Goal* goal = current_goal;
	while (goal != nullptr) {
		goal->get_all_items(items);
		goal = goal->get_parent();
	}
	for (ItemId item : items) {
		if (!item.is_null()) {
			relevant.push_back(item);
		}
	}*/

	return memory.remember_moment(prev_action, items);
}
