#ifndef CRAB_SEMANTICMEMORY_H
#define CRAB_SEMANTICMEMORY_H

#include <unordered_set>
#include <hopscotch_map.h>
#include <hopscotch_set.h>
#include "Knowledge/Trait.h"
#include "Knowledge/Causal.h"
#include "Hierarchy.h"
#include "BoxUtil.h"
#include "EpisodicMemory.h"

//using TraitMap = tsl::hopscotch_map<Pair<ItemId, Attribute>, Trait>;
using CausalSet = tsl::hopscotch_set<Box<Causal>, PointerHash<Box<Causal>>, PointerSame<Box<Causal>>>;

template <class Know>
using KnowledgeRefSet = std::unordered_set<Know*, PointerHash<Know*>, PointerSame<Know*>>;

template <class Key, class Know, class Hash = std::hash<Key>, class Equal = std::equal_to<Key>>
using KnowledgeMap = tsl::hopscotch_map<Key, KnowledgeRefSet<Know>, Hash, Equal>;

class SemanticMemory {
public:
	SemanticMemory(Visible& visible, Log& log): visible(visible), log(log) { }

	//void add_trait(Trait trait, ItemId item);
	void add_causal(Causal causal);

	void remove_causal(Causal& causal);

	Vec<Causal> get_causes(const Boolean& event) const;
	KnowledgeRefSet<Causal> get_effects(const Action& cause) const;

	//const Trait* get_trait(ItemId item, Tk attrib) const;

	//const TraitMap& get_all_traits() const;
	const CausalSet& get_all_causals() const;

	const Hierarchy* get_hierarchy() const;

private:
	template<class Map, class Key, class Know> void add(Map& map, Key& key, Know& knowledge);
	template<class Map, class Key, class Know> void remove(Map& map, Key& key, Know& knowledge);
	template<class Map, class Key> auto& get(const Map& map, Key& key) const;
	template<class Map, class Key> auto get_ptr(const Map& map, Key& key) const;

	template<typename T>
	static void replace_item(T& knowledge, ItemId to_replace, ItemId replacement) {
		std::vector<ItemId*> items;
		knowledge.get_all_items_mut(items);
		for (ItemId* item : items) {
			if (*item == to_replace) {
				*item = replacement;
			}
		}
	}

	Visible& visible;
	Log& log;

	//TraitMap trait_map;

	CausalSet causal_set;
	KnowledgeMap<Boolean, Causal> event_to_cause;
	//KnowledgeMap<BasicAction, Causal> cause_to_event;

	std::unique_ptr<Hierarchy> hierarchy;
};

#endif //CRAB_SEMANTICMEMORY_H
