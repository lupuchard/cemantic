#ifndef WHAT_SOLUTIONPATH_H
#define WHAT_SOLUTIONPATH_H

#include "Evaluator.h"
#include "WeightedVec.h"
#include "SemanticMemory.h"

struct Node {
	Node() { }
	Node(Node& parent, Causal causal, Assignment assignment, float confidence):
		causal(std::make_unique<Causal>(causal)), assignment(std::move(assignment)), parent(&parent), confidence(confidence), depth(parent.depth + 1) { }

	CpBox<Causal> causal = nullptr;
	Assignment assignment;

	TraitMap state;
	Node* parent = nullptr;
	Vec<CpBox<Node>> children;
	float confidence = 1;

	float depth = 0;
	float h_score = -1;
};

class SolutionPath {
public:
	SolutionPath(const SemanticMemory& sem, const Visible& visible): sem(sem), visible(visible), evaluator(visible) { }

	// TODO: write a test for this
	Vec<Action> find_path(const CnfFormula& goal);

	// TODO: delete this
	// Transform current goal based on what this action could take us from
	void apply_action(Node& node, const Action& action);

	void apply_causal(Node& node, float confidence, Causal causal);

private:
	void create_root(const CnfFormula& goal);
	float calc_heuristic(const Node& node);
	bool dest_reached(const Node& node);
	void find_relevant_causals(Node& node);

	template<typename T, typename F>
	void apply_possibilities(T& list, size_t num_new_possibilities, F apply_possibility) {
		size_t prev_size = list.size();
		list.resize(prev_size * num_new_possibilities);
		for (size_t i = 0; i < prev_size; i++) {
			for (size_t j = 0; j < num_new_possibilities; j++) {
				size_t idx = j * prev_size + i;
				if (i > 0) list[idx] = list[i];
				apply_possibility(list[idx], j);
			}
		}
	}

	template<typename T, typename F>
	void apply_possibilities(WeightedVec<T>& list, size_t num_new_possibilities, F apply_possibility) {
		size_t prev_size = list.size();
		list.resize(prev_size * num_new_possibilities);
		for (size_t i = 0; i < prev_size; i++) {
			for (size_t j = 0; j < num_new_possibilities; j++) {
				size_t idx = j * prev_size + i;
				if (i > 0) {
					list[idx] = list[i];
					list.weight(idx) = list.weight(i);
				}
				apply_possibility(list[idx], list.weight(i), j);
			}
		}
	}

	const SemanticMemory& sem;
	const Visible& visible;
	Evaluator evaluator;
	Box<Node> root;
	std::unordered_set<Node*> open_nodes;
};

#endif //WHAT_SOLUTIONPATH_H
