#include "SolutionPath.h"
#include "Evaluator.h"

const float CAUSAL_THRESH = 0.1f;
const float CAUSAL_COMBINATION_THRESH = 0.2f;

Vec<Action> SolutionPath::find_path(const CnfFormula& goal) {
	create_root(goal);
	open_nodes.insert(root.get());

	Node* cur_node = nullptr;
	for (size_t i = 0; i < 100 && !open_nodes.empty(); i++) { // TODO: magic number
		float lowest_f = std::numeric_limits<float>::infinity();
		cur_node = nullptr;
		for (Node* node : open_nodes) {
			float f = node->h_score + node->depth * 0.9; // TODO: magic number
			if (f < lowest_f) {
				lowest_f = f;
				cur_node = node;
			}
		}

		//std::cout << cur_node->state.to_string() << std::endl;
		if (cur_node == nullptr || dest_reached(*cur_node)) {
			break;
		}

		find_relevant_causals(*cur_node);
	}

	assert(cur_node != nullptr);
	Vec<Action> path;
	if (cur_node == nullptr) return path;
	while (cur_node->parent != nullptr) {
		assert(false); //path.push_back(cur_node->causal->v().get_cause()); TODO: how will this work now?
		cur_node = cur_node->parent;
	}
	return path;
}

void SolutionPath::create_root(const CnfFormula& goal) {
	assert(goal.num_clauses() == 1); // TODO: allow OR's
	root = std::make_unique<Node>();

	for (size_t i = 0; i < goal.num_terms(0); i++) {
		auto& term = goal.get_term(0, i);
		root->state.insert(term.get_item(), term.get_trait());
	}

	calc_heuristic(*root);
}

// Works backwards
float SolutionPath::calc_heuristic(const Node& node) {
	float dist = 0;
	for (auto& entry : node.state) {
		ItemId item = entry.first.first;
		const Trait& trait = *entry.second;
		dist += 1 - evaluator.evaluate_bool(Boolean(item, trait.v()));

		/*const Trait* cur = node.state.get(item, attribute);
		if (cur != nullptr && cur->same(trait)) {
			dist += 1 - cur->get_confidence();
		} else {
			dist += 1;
		}*/
	}

	return dist;
}

// We work backwards, so dest checks for current state
bool SolutionPath::dest_reached(const Node& node) {
	float total_confidence = node.confidence;
	for (auto& entry : node.state) {
		ItemId item = entry.first.first;
		const Trait& trait = *entry.second;
		total_confidence *= evaluator.evaluate_bool(Boolean(item, trait.v())); // TODO Should evaluator output be cached (would it ever change during pathing?)
		if (total_confidence == 0) {
			return false;
		}
	}

	return total_confidence > 0.1; // TODO: magic number
}

void SolutionPath::apply_action(Node& node, const Action& action) {
	// applying every combination of causals would mean adding lotsa nodes
	// maybe only add a combination if
	// 1. each individual causal confidence is above 10%
	// 2. net confidence is above 20%
	// 3. the result does not overlap with another
	// results could be dynamically adjusted as pather gets more desperate?

	// TODO: Note that this all ignores probabilistic relationships, but I guess confidence is not really meant to represent that? Still something to consider.
	/*Vec<Pair<Vec<Causal*>, float>> causal_combinations;
	causal_combinations.emplace_back();
	causal_combinations[0].second = 1;
	for (Causal* causal : mem.get_effects(action)) {
		if (causal->get_confidence() < CAUSAL_THRESH) {
			// add to none, but still apply confidence
			for (auto& pair : causal_combinations) {
				pair.second *= (1 - causal->get_confidence());
			}
			continue;
		}

		if (causal->get_confidence() > 1 - CAUSAL_THRESH) {
			// add to all
			for (auto& pair : causal_combinations) {
				pair.first.push_back(causal);
				pair.second *= causal->get_confidence();
			}
			continue;
		}

		// otherwise, consider the results of both adding and not adding
		size_t prev_size = causal_combinations.size();
		for (size_t i = 0; i < prev_size; i++) {
			causal_combinations.push_back(causal_combinations[i]);
			causal_combinations[i].first.push_back(causal);
			causal_combinations[i].second *= causal->get_confidence();
			causal_combinations[prev_size + i].second *= (1 - causal->get_confidence());
		}
	}

	if (causal_combinations.empty()) return;

	// Use all combinations with confidence above threshold.
	bool used_any = false;
	for (auto& pair : causal_combinations) {
		if (pair.second > CAUSAL_COMBINATION_THRESH) {
			apply_causals(node, pair.second, action, pair.first);
			used_any = true;
		}
	}

	// Or if all combinations were below the threshold, just use the best one anyway.
	if (!used_any) {
		float best_confidence = 0;
		const Vec<Causal*>* best_causals = nullptr;
		for (auto& pair : causal_combinations) {
			if (pair.second > best_confidence) {
				best_confidence = pair.second;
				best_causals = &pair.first;
			}
		}

		if (best_causals != nullptr) {
			apply_causals(node, best_confidence, action, *best_causals);
		}
	}*/
	// TODO: forgot to check preconditions
	// TODO: First assignment happens here


}

// Searches for causals that satisfy aspects of the node's current state.
void SolutionPath::find_relevant_causals(Node& node) {
	for (auto iter = node.state.begin(); iter != node.state.end();) {
		ItemId item = iter->first.first;
		const Trait& trait = *iter->second;
		Boolean boolean(item, trait.v());

		float confidence = evaluator.evaluate_bool(boolean);
		if (confidence > 0.9) { // TODO: magic number
			// I think the idea was that if this is already true, we need not store this in the state.
			iter = node.state.remove(iter);
		} else {
			auto causes = sem.get_causes(boolean);
			for (auto& causal : causes) {
				apply_causal(node, causal.get_confidence() * trait.get_confidence(), causal);
			}
			++iter;
		}
	}
}

void SolutionPath::apply_causal(Node& node, float confidence, Causal causal) {
	Vec<CpBox<Node>> child_nodes;
	child_nodes.push_back(std::make_unique<Node>(node, causal, Assignment(), confidence * node.confidence));
	child_nodes.back()->state = node.state;

	Boolean realize = causal.v().get_event();
	Trait trait = realize.get_trait();
	child_nodes.back()->state.remove(realize.get_item(), trait.v().get_key());

	size_t max_var = causal.get_num_vars();
	if (max_var > 0) {
		auto& event = causal.v().get_event();

		WeightedVec<Assignment> possible_assignments(Assignment(max_var), 1);
		for (ItemId var_id : event.get_all_items()) {
			//Item& var = Item::get(var_id);
			//if (var.info() != nullptr) continue;

			// Variable, assign to valid existing item or create new
			// Step 1: Remove all clauses without this variable from the condition proposition
			// TODO: It may be possible for a single term to have multiple variables. We don't handle this yet
			//       and it would probably either require ignoring such terms the first time (until one of the
			//       two variables is assigned) or something more sophisticated.
			CnfFormula prop = causal.v().get_condition().single_out(var_id);

			// Step 2: Iterate through every known item, and add to list if it satisfies proposition
			Vec<Pair<ItemId, float>> possible_singles;
			for (auto& item : visible.all_items()) {
				if (item.is_null()) continue;
				evaluator.use_assignment(var_id, item);
				float confidence = evaluator.evaluate_bool(prop) > 0.1;
				if (confidence > 0.1) {
					possible_singles.emplace_back(item, confidence);
				}
			}

			if (possible_singles.empty()) {
				// If it's impossible to satisfy the variable, we can't perform this action.
				return;
			}

			// TODO Step 3: (Optional for now) Cache result of that search
			// TODO Step 4: (Optional for now) Include possibility that item wouldn't exist yet, or would need to be modified from existing item

			// Step 5: Apply new possible assignments
			apply_possibilities(possible_assignments, possible_singles.size(), [&](auto& assignment, float& weight, size_t idx) {
				assignment[var_id] = possible_singles[idx].first;
				weight *= possible_singles[idx].second;
			});
		}

		// Verify final assignments and update confidence
		possible_assignments.filter([&](Assignment& assignment, float weight) {
			evaluator.use_assignment(assignment);
			return evaluator.evaluate_bool(event, node.state) > 0.1;
		});
		apply_possibilities(child_nodes, possible_assignments.size(), [&](auto& new_node, size_t idx) {
			new_node->assignment = possible_assignments[idx];
			new_node->confidence *= possible_assignments.weight(idx);
		});
	}

	// Every clause would also branch into a separate possibility.
	auto& condition = causal.v().get_condition();
	if (condition.num_clauses() >= 1) {
		apply_possibilities(child_nodes, condition.num_clauses(), [&](auto& new_node, size_t idx) {
			for (size_t i = 0; i < condition.num_terms(idx); i++) {
				const Boolean& term = condition.get_term(idx, i);
				ItemId item = new_node->assignment.fill(term.get_item());
				new_node->state.insert(item, term.get_trait());
			}
		});
	}

	for (auto& child : child_nodes) {
		child->h_score = calc_heuristic(*child);
		open_nodes.insert(child.get());
	}
	node.children.insert(node.children.end(), std::make_move_iterator(child_nodes.begin()), std::make_move_iterator(child_nodes.end()));
	open_nodes.erase(&node);

	// TODO: Collapse duplicate children
	// TODO: Apply other causals resulting from same action as this one
}
