#ifndef CRAB_EPISODICMEMORY_H
#define CRAB_EPISODICMEMORY_H

#include <hopscotch_map.h>
#include <unordered_set>
#include <unordered_map>
#include "MathUtil.h"
#include "BoxUtil.h"
#include "Knowledge/Causal.h"

struct Moment {
	Moment(Action action, int instant, size_t index, float importance = 1):
		action(action), importance(importance), instant(instant), index(index) { }

	Action action; // Action taken AFTER this moment
	Map<ItemId, Map<Tk, MutTrait>> state; // TODO: should there be a confidence attached to this?
	float importance = 0;
	int instant = 0;
	size_t index = 0;

	inline TraitVal get_state(Pair<ItemId, Tk> pair) const {
		auto iter = state.find(pair.first);
		if (iter == state.end()) return Tk::Unknown;
		auto iter2 = iter->second.find(pair.second);
		if (iter2 == iter->second.end()) return Tk::Unknown;
		return iter2->second.get_value();
	}

	inline Vec<Pair<ItemId, Tk>> get_items() const {
		Vec<Pair<ItemId, Tk>> changes;
		for (auto& key_value : state) {
			for (auto elem : key_value.second) {
				changes.emplace_back(key_value.first, elem.first);
			}
		}
		return changes;
	}
};

class MemoryDiff {
public:
	explicit MemoryDiff(const Moment& before): before(before) { }
	virtual ~MemoryDiff() = default;

	inline const Action& action() const {
		return before.action;
	}

	inline const Moment& get_before() const {
		return before;
	}

	inline const Map<Tk, MutTrait>* get_before(ItemId item) const {
		auto iter = before.state.find(item);
		if (iter == before.state.end()) return nullptr;
		return &iter->second;
	}

	inline TraitVal get_before(ItemId item, Tk tk) const {
		auto iter = before.state.find(item);
		if (iter == before.state.end()) return TraitVal();
		auto iter2 = iter->second.find(tk);
		if (iter2 == iter->second.end()) return TraitVal();
		return iter2->second.get_value();
	}

	inline TraitVal get_before(Pair<ItemId, Tk> item_attribute) const {
		return get_before(item_attribute.first, item_attribute.second);
	}

	virtual const Vec<Pair<ItemId, Tk>> get_after() = 0;
	virtual const Map<Tk, MutTrait>* get_after(ItemId item) = 0;
	virtual const TraitVal get_after(ItemId item, Tk tk) = 0;

	inline TraitVal get_after(Pair<ItemId, Tk> item_attribute) {
		return get_after(item_attribute.first, item_attribute.second);
	}

	const Vec<Pair<ItemId, Tk>>& get_changes();

	inline Vec<Pair<ItemId, Tk>> get_items() {
		return before.get_items(); // TODO: also include after?
	}

private:
	const Moment& before;
	Vec<Pair<ItemId, Tk>> changes;
	bool changes_cached = false;
};

class MemoryDiffCur : public MemoryDiff {
public:
	explicit MemoryDiffCur(const Moment& before): MemoryDiff(before) { }

	inline const Vec<Pair<ItemId, Tk>> get_after() override {
		Vec<Pair<ItemId, Tk>> after;
		for (auto& elem : attributes) {
			for (auto& trait_pair : *elem.second) {
				after.emplace_back(elem.first, trait_pair.first);
			}
		}
		return after;
	}

	inline const Map<Tk, MutTrait>* get_after(ItemId item) override {
		auto iter = attributes.find(item);
		if (iter == attributes.end()) {
			attributes[item] = std::make_unique<Map<Tk, MutTrait>>();
			return attributes[item].get();
		}

		return iter->second.get();
	}

	inline const TraitVal get_after(ItemId item, Tk tk) override {
		auto iter = attributes.find(item);
		if (iter == attributes.end()) return ItemId();
		auto iter2 = iter->second->find(tk);
		if (iter2 == iter->second->end()) return ItemId();
		return iter2->second.get_value();
	}

private:
	std::unordered_map<ItemId, Box<Map<Tk, MutTrait>>> attributes;
};

class MemoryDiffHist : public MemoryDiff {
public:
	MemoryDiffHist(const Moment& before, const Moment& after): MemoryDiff(before), after(after) { }

	inline const Moment& get_after_moment() const {
		return after;
	}

	inline const Vec<Pair<ItemId, Tk>> get_after() override {
		return after.get_items();
	}

	inline const Map<Tk, MutTrait>* get_after(ItemId item) override {
		auto iter = after.state.find(item);
		if (iter == after.state.end()) return nullptr;
		return &iter->second;
	}

	inline const TraitVal get_after(ItemId item, Tk tk) override {
		auto iter = after.state.find(item);
		if (iter == after.state.end()) return ItemId();
		auto iter2 = iter->second.find(tk);
		if (iter2 == iter->second.end()) return ItemId();
		return iter2->second.get_value();
	}

	using MemoryDiff::get_after;

private:
	const Moment& after;
};

class EpisodicMemory {
public:
	EpisodicMemory(const Visible& visible);
	const Moment& remember_moment(const Action& action, const Vec<ItemId>& relevant_items);
	const Moment& current() const;

	inline size_t size() const { return memory.size(); }
	const Moment& get(size_t index) const;

	/// Each occurrence is the moment AFTER the action
	/// This will not be in chronological order (TODO: should it be?)
	Vec<MemoryDiffHist> get_occurrences() const;
	Vec<MaybeBox<Moment>> get_occurrences(const Action& action) const;
	//Vec<MemoryDiffHist> get_occurrences(BasicAction::Kind action) const;

	Box<MemoryDiff> get_diff(const Moment& prev) const;

	//Vec<MemoryDiffHist> get_changes() const; // TODO: well now we need a new way to filter which occurrences we look at

private:
	friend class MemoryDiff;

	const Visible& visible;
	int current_instant = 0;
	Vec<Box<Moment>> memory;

	// TODO: does not handle hash-collision cases, may or may not be an issue? (it's just for searching)
	Map<Hash<Action>, std::unordered_set<Pair<Moment*, Assignment>>> action_occurrence_map;
};


#endif //CRAB_EPISODICMEMORY_H
