#ifndef CRAB_EVALUATOR_H
#define CRAB_EVALUATOR_H

#include "Knowledge/Proposition.h"
#include "EpisodicMemory.h"

class Evaluator {
public:
	Evaluator(const Visible& visible);
	Evaluator(const Assignment& assignment);
	Evaluator(const Visible& visible, const Assignment& assignment);

	//float evaluate_knowledge(const Knowledge& knowledge);
	float evaluate_bool(const CnfFormula& prop);
	float evaluate_bool(const CnfFormula& prop, const TraitMap& alteration);
	float evaluate_bool(const CnfFormula& prop, const Moment& moment);

	float evaluate_bool(const Boolean& boolean);
	float evaluate_bool(const Boolean& boolean, const TraitMap& alteration);
	float evaluate_bool(const Boolean& boolean, const Moment& moment);

	float evaluate_impact(const Boolean& event, const CnfFormula& prop);
	float evaluate_difficulty(const CnfFormula& prop, size_t clause);

	void use_assignment(ItemId var, ItemId val);
	void use_assignment(const Assignment& assignment);
	void clear_assignments();

private:
	std::pair<TraitVal, float> get_trait(ItemId item, Tk attribute);

	const Visible* visible = nullptr;
	const Boolean* test_mutation = nullptr;
	const TraitMap* test_alteration = nullptr;
	const Moment* test_moment = nullptr;
	std::array<ItemId, NUM_RESERVED_ITEM_IDS> assignments;
};


#endif //CRAB_EVALUATOR_H
