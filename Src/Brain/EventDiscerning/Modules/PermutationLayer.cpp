#include "PermutationLayer.h"

PermutationLayer::PermutationLayer(Vec<Vec<fl::Index>> permutations, int input_size, int output_size)
		: permutations(permutations) {
	fl::Variable w(fl::detail::kaimingUniform(fl::Shape({input_size, output_size}), input_size, fl::dtype::f32), true);
	double bound = std::sqrt(1.0 / input_size);
	auto b = fl::uniform(fl::Shape({output_size}), -bound, bound, fl::dtype::f32, true);
	params_ = {w, b};
};

PermutationLayer::PermutationLayer(Vec<Vec<fl::Index>> permutations, const fl::Variable& w, const fl::Variable& b)
		: UnaryModule({w, b}), permutations(permutations) { }

fl::Variable rearrange(fl::Variable input, Vec<fl::Index>& permutation) {
	fl::Variable output(fl::Tensor({ (int)permutation.size() }), true);
	for (size_t i = 0; i < permutation.size(); i++) {
		output(i) = input(permutation[i]);
	}
	return output;
}

fl::Variable PermutationLayer::forward(const fl::Variable& input) {
	auto weights = params_[0].astype(input.type());
	auto bias = params_[1].astype(input.type());

	int num_output = weights.dim(0);
	fl::Variable output(fl::Tensor({ num_output, (int)permutations.size() }), true);

	for (int i = 0; i < num_output; i++) {
		for (size_t j = 0; j < permutations.size(); j++) {
			output({i, (int)j}) = rearrange(input, permutations[j]) * weights(i) + bias(i);
		}
	}

	return fl::mean(output, {1});
}

std::string PermutationLayer::prettyString() const {
	return "Permutation Layer";
}
