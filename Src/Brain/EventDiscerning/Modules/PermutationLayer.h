#ifndef WHAT_PERMUTATIONLAYER_H
#define WHAT_PERMUTATIONLAYER_H

#include <flashlight/fl/flashlight.h>
#include <flashlight/fl/tensor/Index.h>
#include "TypeUtil.h"

class PermutationLayer : public fl::UnaryModule {
public:
	PermutationLayer(Vec<Vec<fl::Index>> permutations, int input_size, int output_size);
	PermutationLayer(Vec<Vec<fl::Index>> permutations, const fl::Variable& w, const fl::Variable& b);

	fl::Variable forward(const fl::Variable& input) override;

	std::string prettyString() const override;

private:
	Vec<Vec<fl::Index>> permutations;
};


#endif //WHAT_PERMUTATIONLAYER_H
