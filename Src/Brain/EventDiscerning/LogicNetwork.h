#ifndef WHAT_LOGICNETWORK_H
#define WHAT_LOGICNETWORK_H

#include "TypeUtil.h"

enum class DataType {
	Boolean,
	Item,
	Tk,
	ItemTk,
	ActionKind,
	Moment
};

struct InputGroup {
	InputGroup(int quantity, bool invariant = false): quantity(quantity), invariant(invariant) { }
	int quantity;
	bool invariant;
};

struct InputType {
	InputType(int group, DataType type, bool relative = true): group(group), type(type), relative(relative) { }
	 int group;
	 DataType type;
	 bool relative;
};

struct LogicNetworkConfig {
	inline int add_group(int quantity, bool invariant = true) {
		groups.emplace_back(quantity, invariant);
		return groups.size() - 1;
	}

	inline int add_input(int group, DataType type, bool relative = true) {
		inputs.emplace_back(group, type, relative);
		return inputs.size() - 1;
	}

	Vec<InputGroup> groups;
	Vec<InputType> inputs;
	int layer_1_width = 4;
	int num_hidden_layers = 2;
	int hidden_layer_widths = 256;
};

class LogicNetwork {
public:

private:

};


#endif //WHAT_LOGICNETWORK_H
