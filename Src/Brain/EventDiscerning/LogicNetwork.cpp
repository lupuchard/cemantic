#include "LogicNetwork.h"

const float LEARNING_RATE = 0.0001;
const float MOMENTUM = 0.9;
const int NUM_EPOCHS = 100;
