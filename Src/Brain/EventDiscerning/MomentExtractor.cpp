#include "MomentExtractor.h"

struct OccurrenceValues {
	OccurrenceValues(ItemId item, Tk key, int32_t before, int32_t after):
		item(item), key(key), before(before), after(after) { }
	ItemId item;
	Tk key;
	int32_t before;
	int32_t after;
	auto operator<=>(const OccurrenceValues& other) const = default;
};

/*Vec<Box<MemoryDiff>> MomentExtractor::get_diffs_for_action(BasicAction::Kind action_kind) {
	Vec<const Moment*> occurrences = mem.get_occurrences(action_kind);
	Vec<Box<MemoryDiff>> diffs;
	diffs.reserve(occurrences.size());
	for (const Moment* aft : occurrences) {
		const Moment& bef = mem.get(aft->index - 1);
		if (aft->instant - 1 != bef.instant) continue;
		diffs.push_back(mem.get_diff(bef));
	}
	diffs.push_back(mem.get_diff(mem.current()));
	return diffs;
}*/

void MomentExtractor::read_diffs(const Vec<Box<MemoryDiff>>& diffs) {
	before.clear();
	after.clear();

	size_t i = 0;
	Set<uint64_t> occurrence_hashes;
	for (auto& occurrence : diffs) {
		Vec<Pair<ItemId, Tk>> changes = occurrence->get_items();
		if (changes.empty()) continue;

		Vec<OccurrenceValues> full_occurrence; // used to check for duplicates
		for (auto &pair: changes) {
			if (before.find(pair) == before.end()) {
				before[pair] = arma::tvec(diffs.size());
				after[pair] = arma::tvec(diffs.size());
			}
			int32_t before_value = occurrence->get_before(pair.first, pair.second).val;
			int32_t after_value = occurrence->get_after(pair.first, pair.second).val;
			full_occurrence.emplace_back(pair.first, pair.second, before_value, after_value);
		}

		// Hash occurrence
		std::sort(full_occurrence.begin(), full_occurrence.end());
		uint64_t hash = 0;
		for (auto& values : full_occurrence) {
			hash = combine_hash(hash, values.item.val);
			hash = combine_hash(hash, values.key);
			hash = combine_hash(hash, values.before);
			hash = combine_hash(hash, values.after);
		}

		// Add occurrence only if not duplicate
		// TODO: Increase weight for duplicates
		auto iter = occurrence_hashes.find(hash);
		if (iter == occurrence_hashes.end()) {
			occurrence_hashes.insert(hash);
			for (auto& values : full_occurrence) {
				auto pair = std::make_pair(values.item, values.key);
				before[pair][i] = values.before;
				after[pair][i] = values.after;
			}
			i++;
		}
	}

	assert(i > 0);

	// Trim
	for (auto iter = before.begin(); iter != before.end(); ++iter) {
		iter.value().resize(i);
	}
	for (auto iter = after.begin(); iter != after.end(); ++iter) {
		iter.value().resize(i);
	}
}

float MomentExtractor::calc_significance(const Action& action, MemoryDiff& moment) {
	float significance = 0;

	// Does moment involve action?
	if (moment.action()  == action) significance += 2;

	return significance;
}

float MomentExtractor::calc_significance(const Action& action, ItemId item, Tk attribute) {
	float significance = 0;

	// Does action contain item?
	const auto& action_items = action.get_all_items();
	if (std::find(action_items.begin(), action_items.end(), item) != action_items.end()) {
		significance += 1;
	}

	return significance;
}

float MomentExtractor::calc_significance(const Action& action, MemoryDiff& moment, ItemId item, Tk attribute) {
	float significance = 0;

	// Does value of attribute change in this moment?
	if (moment.get_before(item, attribute) != moment.get_after(item, attribute)) {
		significance += 1;
	}

	// TODO: Does value vary from event to event?
	// TODO: Is moment similar to already selected moments?

	return significance;
}

/*void MomentExtractor::extract_moments(size_t how_many, const BasicAction& action) {
	Vec<MemoryDiffCur> moments;
	Vec<Pair<ItemId, Tk>> traits;

	web.search(action, [&](const AssociationWeb::NodeVal& val) {
		if (how_many <= 0) return std::make_pair(ApplyAction::Terminate, 0.f);

		if (std::holds_alternative<const Moment*>(val)) {
			const auto& moment = *std::get<const Moment*>(val);
			MemoryDiffCur diff(moment);
			float significance = calc_significance(action, diff);
			for (auto& trait : traits) {
				significance += calc_significance(action, diff, trait.first, trait.second);
			}
			if (significance > 0) moments.push_back(std::move(diff));
			return std::make_pair(ApplyAction::None, significance);
		} else if (std::holds_alternative<Pair<ItemId, Tk>>(val)) {
			auto trait = std::get<Pair<ItemId, Tk>>(val);
			float significance = calc_significance(action, trait.first, trait.second);
			for (auto& moment : moments) {
				significance += calc_significance(action, moment, trait.first, trait.second);
			}
			if (significance > 0) traits.push_back(trait);
			return std::make_pair(ApplyAction::None, significance);
		} else {
			return std::make_pair(ApplyAction::Skip, 0.f);
		}
	});
}*/
