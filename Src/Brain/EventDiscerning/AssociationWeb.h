#ifndef WHAT_ASSOCIATIONWEB_H
#define WHAT_ASSOCIATIONWEB_H

#include <variant>
#include <queue>
#include "Knowledge/Causal.h"
#include "EpisodicMemory.h"

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

enum class ApplyAction {
	None,
	Skip,
	Terminate,
};

class AssociationWeb {
public:
	AssociationWeb() {
		nodes.emplace_back(); // So index 0 can be null
	}

	using NodeVal = std::variant<Action, const Moment*, ItemId, Pair<ItemId, Tk>>;
	struct Node {
		template<typename T> Node(T val): object(val) { }
		NodeVal object;
		std::unordered_set<size_t> associated;
	};

	template<class U, class V>
	inline void create_association(U u, V v) {
		size_t u_node = find_node(u);
		size_t v_node = find_node(v);
		if (nodes[u_node]->associated.contains(v)) return;
		nodes[u_node]->associated.insert(v_node);
		nodes[v_node]->associated.insert(u_node);
	}

	// apply returns Pair<ApplyAction, float>
	template<typename F>
	inline void search(const Action& start, F apply) const {
		auto iter = find_action.find(start);
		assert(iter != find_action.end());
		size_t node_idx = iter->second;
		assert(node_idx < nodes.size());
		assert(nodes[node_idx] != nullptr);

		auto cmp = [](Pair<Node*, float> lhs, Pair<Node*, float> rhs) { return lhs.second > rhs.second; };
		std::priority_queue<Pair<Node*, float>, Vec<Pair<Node*, float>>, decltype(cmp)> queue(cmp);

		queue.emplace(nodes[node_idx].get(), 0);
		Set<size_t> already_visited = { node_idx };
		while (!queue.empty()) {
			auto next = queue.top();
			for (size_t connected : next.first->associated) {
				if (already_visited.count(connected)) continue;

				auto pair = apply(nodes[connected]->object);
				if (pair.first == ApplyAction::Terminate) {
					goto done;
				} else if (pair.first == ApplyAction::Skip) {
					pair.second = next.second;
				}

				if (pair.second > 0) {
					queue.emplace(nodes[connected].get(), pair.second);
					already_visited.insert(connected);
				}
			}
		}
		done:
		return;
	}

private:
	inline size_t find_or_create_node(const Action& action) {
		return find_or_create_node(action, find_action);
	}

	inline size_t find_or_create_node(ItemId item) {
		return find_or_create_node(item, find_item);
	}

	template<typename T>
	inline size_t find_or_create_node(const T& val, Map<T, size_t> map) {
		auto iter = map.find(val);
		if (iter == map.end()) {
			nodes.push_back(std::make_unique<Node>(val));
			map[val] = nodes.size() - 1;
			return nodes.size() - 1;
		} else {
			return iter->second;
		}
	}

	Map<Action, size_t> find_action;
	Map<ItemId, size_t> find_item;

	Vec<Box<Node>> nodes;
};

#endif //WHAT_ASSOCIATIONWEB_H
