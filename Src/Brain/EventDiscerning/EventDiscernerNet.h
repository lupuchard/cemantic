#ifndef WHAT_EVENTDISCERNERNET_H
#define WHAT_EVENTDISCERNERNET_H

#include "EpisodicMemory.h"
#include "Knowledge/Causal.h"
#include <flashlight/fl/flashlight.h>

class EventDiscernerNet {
public:
	const int NUM_INPUT_MOMENTS = 6;
	const int NUM_INPUT_TRAITS = 6;
	const int NUM_SUBINPUTS = 2;
	const int LAYER_1_WIDTH = 4;
	const int NUM_HIDDEN_LAYERS = 2;
	const int HIDDEN_LAYER_WIDTHS = 256;
	const int NUM_OUTPUTS = 10;

	struct InputData {
		Vec<MemoryDiffCur> moments;
		Vec<Pair<ItemId, Tk>> traits;
	};

	EventDiscernerNet();
	void train(Vec<InputData>& data, const Vec<MutCausal>& output);
	MutCausal run(InputData& data);
private:
	struct RawInput {
		RawInput(MemoryDiffCur& moment, ItemId item, Tk attrib): moment(&moment), item(item), attribute(attrib) { }
		MemoryDiffCur* moment;
		ItemId item;
		Tk attribute;
	};

	fl::Tensor prep_input(InputData& data);
	fl::Tensor prep_output(const MutCausal& output);
	MutCausal parse_output(fl::Tensor& output);
	void setup_paired_inputs();
	void setup_network();
	Vec<Vec<fl::Index>> gen_permutations();
	TraitVal get_before(RawInput& raw_input);
	TraitVal get_after(RawInput& raw_input);

	Vec<Pair<size_t, size_t>> paired_inputs;
	Map<Pair<size_t, size_t>, size_t> paired_input_map;

	fl::Sequential model;
};

#endif //WHAT_EVENTDISCERNERNET_H
