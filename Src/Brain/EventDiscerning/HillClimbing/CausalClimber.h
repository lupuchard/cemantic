#ifndef WHAT_CAUSALCLIMBER_H
#define WHAT_CAUSALCLIMBER_H

#include <queue>
#include "Knowledge/Causal.h"
#include "EpisodicMemory.h"
#include "SemanticMemory.h"

class CausalClimber;

struct Evaluation {
	Evaluation(float coverage, float accuracy): coverage(coverage), accuracy(accuracy) { }
	float coverage;
	float accuracy;
};

struct EvCausal {
	EvCausal(MutCausal causal): causal(causal) { }
	MutCausal causal;
	float coverage = -1;
	float accuracy = -1;
	float weight = 0;

	float get_effective_coverage() const;
	float get_effective_accuracy() const;

	bool variables_expanded = false;
	bool condition_expanded = false;
	Box<CausalClimber> subclimber;
	Box<Vec<MutCausal>> subclimber_result;
};

struct Occurrence {
	Occurrence(MemoryDiffHist diff): diff(std::move(diff)) { }
	MemoryDiffHist diff;
	Vec<Pair<ItemId, Tk>> relevant_changes;

	const Vec<Pair<ItemId, Tk>>& get_changes() {
		return relevant_changes.empty() ? diff.get_changes() : relevant_changes;
	}
};

template<typename EvPtr>
bool ev_queue_compare(EvPtr& left, EvPtr& right) {
	if (left->weight < right->weight) {
		return true;
	} else if (left->weight > right->weight) {
		return false;
	} else if (left->causal.get_condition().num_terms() > right->causal.get_condition().num_terms()) {
		return true;
	} else if (left->causal.get_condition().num_terms() < right->causal.get_condition().num_terms()) {
		return false;
	} else {
		return left->causal.get_hash() < right->causal.get_hash();
	}
}

// TODO: the plan
// when coverage is insufficient, sometimes split the process, performing two separate climbs
class CausalClimber {
public:
	CausalClimber(Log& log, const SemanticMemory& sem, EpisodicMemory& mem, const Visible& visible, int depth = 0):
		log(log), sem(sem), mem(mem), visible(visible), depth(depth) { }

	const SemanticMemory& get_sem() const { return sem; }
	const EpisodicMemory& get_mem() const { return mem; }
	const Visible& get_visible() const { return visible; }

	void begin_discern(const Action& action);
	inline float get_best_coverage() const { return best_coverage; }
	inline float get_best_accuracy() const { return best_accuracy; }

	std::optional<Vec<MutCausal>> step();
	Vec<EvCausal*> get_queue();

private:
	void begin_discern(const Action& action, Vec<Occurrence> occurrences);

	bool raise_coverage(EvCausal& causal);
	void expand_variables(const MutCausal& causal);
	void split_causal(EvCausal& causal);
	void step_subclimber(EvCausal& ev);

	bool raise_accuracy(EvCausal& causal);
	void expand_condition(const MutCausal& causal);

	void enqueue_with_new_condition(const MutCausal& causal, Boolean new_term);
	void enqueue(MutCausal causal);
	void update_weight(EvCausal& causal);
	bool is_diff_covered(const EvCausal& ev, MemoryDiffHist& diff);

	Action relevant_action;
	Vec<Occurrence> relevant_occurrences;
	Vec<Box<EvCausal>> queue;
	Set<Hash<MutCausal>> attempted;

	Log& log;
	const SemanticMemory& sem;
	EpisodicMemory& mem;
	const Visible& visible;

	std::optional<Vec<MutCausal>> best;
	float best_coverage = 0;
	float best_accuracy = 0;

	int depth = 0;
};

#endif //WHAT_CAUSALCLIMBER_H
