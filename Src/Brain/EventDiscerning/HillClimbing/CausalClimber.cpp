#include "CausalClimber.h"
#include "Evaluator.h"
#include "StlUtil.h"
#include <set>
#include <ranges>

/*
 * BasicAction cause;
	Boolean event;
	Proposition condition;
 */

//
// cause.kind  <- derived from given action, never changes
// cause.item1 <- can become variable
// cause.item2 <- can become variable
// event <- first level, we iterate through all the changed traits and each creates a branch
// event.item  <- can become variable
// event.value <- can become variable
// condition
//
// need to go back and forth between increasing coverage and increasing accuracy:
//   creating variables will always increase coverage
//   but creating conditions should increase accuracy

void CausalClimber::begin_discern(const Action& action) {
	auto occurrences = std::move_transform<Occurrence>(mem.get_occurrences());
	begin_discern(action, occurrences);
}

void CausalClimber::begin_discern(const Action& action, Vec<Occurrence> occurrences) {
	relevant_action = action;
	queue.clear();
	attempted.clear();
	relevant_occurrences = std::move(occurrences);

	Set<Boolean> events;
	for (Occurrence& occ : relevant_occurrences) {
		for (auto& change : occ.get_changes()) {
			events.emplace(change.first, change.second, occ.diff.get_after(change));
		}
	}

	for (auto& event : events) {
		enqueue(MutCausal(event)); // TODO: add action as condition
	}

	best.reset();
	best_coverage = 0;
}

std::optional<Vec<MutCausal>> CausalClimber::step() {
	if (queue.empty()) {
		log.info("Queue empty.");
		return Vec<MutCausal>();
	}

	Box<EvCausal> ev = std::pop_heap(queue, ev_queue_compare<Box<EvCausal>>);

	log.info("Top of queue: {}", ev->causal.to_string(visible));
	log.info("Coverage: {}, Accuracy: {}", ev->coverage, ev->accuracy);

	float accuracy = ev->get_effective_accuracy();
	float coverage = ev->get_effective_coverage();

	if (accuracy > 0.9 && coverage > best_coverage) { // TODO: magic number
		Vec<MutCausal> causals = { ev->causal };
		if (ev->subclimber_result) {
			std::insert(causals, *ev->subclimber_result);
		}

		best_coverage = coverage;
		best_accuracy = accuracy;

		if (best_coverage == 1) {
			log.info("Hello"); // TODO
			//log.info("Finished: {}", best->to_string(visible));
			return causals;
		}
	}

	bool succeed = (coverage > accuracy) ? raise_accuracy(*ev) : raise_coverage(*ev);

	if (succeed) {
		update_weight(*ev);
		std::push_heap(queue, std::move(ev), ev_queue_compare<Box<EvCausal>>);
	}

	return {};
}

void CausalClimber::enqueue(MutCausal causal) {
	auto hash = causal.get_hash();
	if (attempted.count(hash)) {
		return; // Identical causal was already enqueued.
	}

	auto ev = std::make_unique<EvCausal>(causal);

	// When calculating coverage, we look at each change, at every occurrence
	// A change is 'covered' if:
	//   1. The condition passes
	//   2. The occurrence's action matches
	//   3. The change is explained
	int total_changes = 0;
	float covered_changes = 0;
	for (auto& occ : relevant_occurrences) {
		if (occ.get_changes().empty()) continue;
		total_changes += occ.get_changes().size();

		if (!is_diff_covered(*ev, occ.diff)) continue;

		for (auto& change : occ.get_changes()) {
			auto& event = ev->causal.get_event();
			ItemId event_item = rep(event.get_item(), ACTION_TARGET, occ.diff.action().target); // TODO: repeated in split_causal?
			if (event_item == change.first && event.get_trait().get_key() == change.second) {
				covered_changes++;
				break;
			}
		}
	}
	ev->coverage = total_changes > 0 ? (float)covered_changes / total_changes : 0;

	// When calculating accuracy, we look only at applicable changes
	int total_occurrences = 0;
	int successful_occurrences = 0;
	for (auto& occ : relevant_occurrences) {
		if (!is_diff_covered(*ev, occ.diff)) continue;

		total_occurrences++;
		Evaluator evaluator(visible);
		evaluator.use_assignment(ACTION_TARGET, occ.diff.action().target); // TODO: this also repeats?
		if (discrete(evaluator.evaluate_bool(ev->causal.get_event(), occ.diff.get_after_moment()))) {
			successful_occurrences++;
		}
	}
	ev->accuracy = total_occurrences > 0 ? (float)successful_occurrences / total_occurrences : 0;
	update_weight(*ev);

	std::push_heap(queue, std::move(ev), ev_queue_compare<Box<EvCausal>>);
	attempted.insert(hash);
}

bool CausalClimber::raise_coverage(EvCausal& ev) {
	if (!ev.variables_expanded) {
		expand_variables(ev.causal);
		ev.variables_expanded = true;
	} else if (!(ev.subclimber || ev.subclimber_result) && depth < 1) {
		split_causal(ev);
	} else if (ev.subclimber && !ev.subclimber_result){
		step_subclimber(ev);
	} else {
		return false;
	}

	return true;
}

bool CausalClimber::raise_accuracy(EvCausal& ev) {
	if (!ev.condition_expanded) {
		expand_condition(ev.causal);
		ev.condition_expanded = true;
	} else {
		return false;
	}

	return true;
}

void CausalClimber::expand_variables(const MutCausal& causal) {
	//uint32_t var = get_num_variables(causal.get_cause()) + 1;
	//Vec<Action> actions = add_var(causal.get_cause(), var);
	//if (actions.empty()) return;

	Vec<Boolean> events = add_var(causal.get_event(), ACTION_TARGET);
	events.push_back(causal.get_event());

	Vec<CnfFormula> conditions = add_var(causal.get_condition(), ACTION_TARGET);
	conditions.push_back(causal.get_condition());

	Vec<MutCausal> causals;
	//for (Action& cause : actions) {
		for (Boolean& event : events) {
			for (CnfFormula& condition : conditions) {
				enqueue(MutCausal(event, condition));
			}
		}
	//}

	log.info("Raising coverage: added {} generic forms", events.size() * conditions.size());
}

// If not covered, assignment returned is invalid
bool CausalClimber::is_diff_covered(const EvCausal& ev, MemoryDiffHist& diff) {
	Evaluator evaluator(visible);
	evaluator.use_assignment(ACTION_TARGET, diff.action().target);
	return discrete(evaluator.evaluate_bool(ev.causal.get_condition(), diff.get_before()));
}

void CausalClimber::split_causal(EvCausal& ev) {
	Vec<Occurrence> uncovered;
	for (auto& occ : relevant_occurrences) {
		if (occ.get_changes().empty()) continue;

		bool covered = is_diff_covered(ev, occ.diff);
		if (!covered) {
			uncovered.push_back(occ);
			continue;
		}

		auto& changes = occ.get_changes();
		Vec<Pair<ItemId, Tk>> uncovered_changes;
		for (auto& change : changes) {
			auto& event = ev.causal.get_event();
			ItemId event_item = rep(event.get_item(), ACTION_TARGET, occ.diff.action().target);
			if (event_item != change.first || event.get_trait().get_key() != change.second) {
				uncovered_changes.push_back(change);
			}
		}

		if (!uncovered_changes.empty()) {
			uncovered.push_back(occ);
			if (uncovered_changes.size() < changes.size()) {
				uncovered.back().relevant_changes = uncovered_changes;
			}
		}
	}

	ev.subclimber.reset(new CausalClimber(log, sem, mem, visible, depth + 1));
	ev.subclimber->begin_discern(relevant_action, uncovered);
	step_subclimber(ev);
	// TODO: Any subclimber's action X implicitly has the condition of being NOT covered by the superclimber
}

void CausalClimber::step_subclimber(EvCausal& ev) {
	float best_subcoverage_before = ev.subclimber->get_best_coverage();

	for (size_t i = 0; i < 2; i++) {
		auto result = ev.subclimber->step();
		if (result) {
			ev.subclimber_result = std::make_unique<Vec<MutCausal>>(result.value());

			auto condition = ev.causal.get_condition().get_negation();
			if (condition.num_terms() > 0) {
				for (MutCausal& subclimber_causal : *ev.subclimber_result) {
					subclimber_causal.get_condition().and_prop(condition);
				}
			}

			break;
		}
	}
}

struct StateSets {
	// item.tk = val
	Vec<Pair<ItemId, MutTrait>> item_traits;

	// X.tk = val
	Vec<MutTrait> traits;

	// item.tk = X
	Vec<Pair<ItemId, Tk>> item_attributes;

	// X.tk = X
	Vec<Tk> attributes;

	StateSets() { }

	StateSets(const Moment& moment, bool include_vars) {
		item_attributes = moment.get_items();

		std::set<Pair<ItemId, MutTrait>> item_trait_set;
		for (Pair<ItemId, Tk> pair : item_attributes) {
			TraitVal val = moment.get_state(pair);
			item_traits.emplace_back(pair.first, MutTrait(pair.second, val));
		}
		std::sort(item_traits);

		if (include_vars) {
			for (Pair<ItemId, Tk> pair : item_attributes) {
				TraitVal val = moment.get_state(pair);
				traits.emplace_back(pair.second, val);
				attributes.push_back(pair.second);
			}

			std::sort(item_attributes);
			std::sort(traits);
			traits.erase(std::unique(traits.begin(), traits.end()), traits.end());
			std::sort(attributes);
			attributes.erase(std::unique(attributes.begin(), attributes.end()), attributes.end());
		}
	}

	static StateSets from_intersection(const StateSets& in1, const StateSets& in2, bool include_vars) {
		StateSets out;
		std::set_intersection(in1.item_traits, in2.item_traits, out.item_traits);
		if (include_vars) {
			std::set_intersection(in1.traits, in2.traits, out.traits);
			std::set_intersection(in1.item_attributes, in2.item_attributes, out.item_attributes);
			std::set_intersection(in1.attributes, in2.attributes, out.attributes);
		}
		return out;
	}
};

void CausalClimber::expand_condition(const MutCausal& causal) {
	log.info("Raising accuracy");

	int num_vars = get_num_variables(causal);
	StateSets intersection;

	// TODO: a critical optimization would be to remove all traits that never changed
	bool first = true;
	for (Occurrence& occ : relevant_occurrences) {
		//auto assignment = Assignment::from(causal.get_cause(), occ.diff.action());
		// if (!assignment.valid()) continue; TODO: what did this condition do?
		Evaluator evaluator(visible);
		evaluator.use_assignment(ACTION_TARGET, occ.diff.action().target);

		float applicable = evaluator.evaluate_bool(causal.get_condition(), occ.diff.get_before());
		if (applicable < 0.1) continue; // TODO: magic number

		StateSets next = StateSets(occ.diff.get_before(), num_vars > 0);
		if (first) {
			intersection = next;
			first = false;
		} else {
			intersection = StateSets::from_intersection(intersection, next, num_vars > 0);
		}
	}

	for (Pair<ItemId, MutTrait> item_trait : intersection.item_traits) {
		enqueue_with_new_condition(causal, Boolean(item_trait.first, item_trait.second));
	}

	for (int i = 1; i <= num_vars; i++) {
		for (MutTrait trait : intersection.traits) {
			enqueue_with_new_condition(causal, Boolean(ItemId(i), trait));
		}

		for (Pair<ItemId, Tk> item_attribute : intersection.item_attributes) {
			enqueue_with_new_condition(causal, Boolean(item_attribute.first, item_attribute.second, ItemId(i)));
		}

		for (int j = 1; j <= num_vars; j++) {
			for (Tk attribute : intersection.attributes) {
				enqueue_with_new_condition(causal, Boolean(ItemId(i), attribute, ItemId(j)));
			}
		}
	}
}

void CausalClimber::enqueue_with_new_condition(const MutCausal& causal, Boolean term) {
	if (causal.get_condition().num_terms() == 0) {
		enqueue(MutCausal(causal.get_event(), CnfFormula(term)));
	} else {
		CnfFormula new_condition1 = causal.get_condition();
		auto new_condition2 = new_condition1.get_disjunction(term);
		new_condition1.and_bool(term);
		enqueue(MutCausal(causal.get_event(), new_condition1));
		enqueue(MutCausal(causal.get_event(), new_condition2));
	}
}

Vec<EvCausal*> CausalClimber::get_queue() {
	Vec<EvCausal*> sorted_queue;
	for (auto& elem : queue) {
		sorted_queue.push_back(elem.get());
	}

	std::sort_heap(sorted_queue.begin(), sorted_queue.end(), ev_queue_compare<EvCausal*>);
	std::reverse(sorted_queue.begin(), sorted_queue.end());
	return sorted_queue;
}

void CausalClimber::update_weight(EvCausal& ev) {
	float weight = ev.get_effective_coverage() * std::pow(ev.get_effective_accuracy(), 2);
	//if (ev.variables_expanded) weight *= 0.9;
	if (ev.condition_expanded) weight *= 0.9;
	if (ev.subclimber != nullptr) weight *= 0.9;
	if (ev.subclimber_result != nullptr) weight *= 0.8;

	for (size_t i = 0; i < ev.causal.get_condition().num_terms(); i++) {
		weight *= 0.9;
	}

	ev.weight = weight;

	// POSSIBILITIES:
	// lower weight based on condition length?
	// lower weight if new condition changed absolutely nothing?
}

float EvCausal::get_effective_coverage() const {
	if (subclimber) {
		return coverage + subclimber->get_best_coverage() * (1 - coverage);
	} else {
		return coverage;
	}
}

float EvCausal::get_effective_accuracy() const {
	if (subclimber) {
		return coverage * accuracy + (1 - coverage) * subclimber->get_best_accuracy();
	} else {
		return accuracy;
	}
}
