#ifndef WHAT_MOMENTEXTRACTOR_H
#define WHAT_MOMENTEXTRACTOR_H

#include <armadillo>
#include "EpisodicMemory.h"
#include "AssociationWeb.h"

namespace arma {
	typedef arma::ivec tvec;
	typedef arma::Col<unsigned char> bvec;
}

class MomentExtractor {
public:
	MomentExtractor(const EpisodicMemory& mem, const AssociationWeb& web): mem(mem), web(web) { }

	//void extract_moments(size_t how_many, const BasicAction& action);

	// Retrieves all MemoryDiffs for moments when a particular kind of action were performed.
	//Vec<Box<MemoryDiff>> get_diffs_for_action(BasicAction::Kind action_kind);

	void read_diffs(const Vec<Box<MemoryDiff>>& diffs);

	static float calc_significance(const Action& action, MemoryDiff& moment);
	static float calc_significance(const Action& action, ItemId item, Tk attribute);
	static float calc_significance(const Action& action, MemoryDiff& moment, ItemId item, Tk attribute);

private:
	const EpisodicMemory& mem;
	const AssociationWeb& web;
	Map<Pair<ItemId, Tk>, arma::tvec> before;
	Map<Pair<ItemId, Tk>, arma::tvec> after;
};


#endif //WHAT_MOMENTEXTRACTOR_H
