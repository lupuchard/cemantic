#include "EventDiscernerNet.h"
#include "EventDiscerning/Modules/PermutationLayer.h"
#include "LogicNetwork.h"

const float LEARNING_RATE = 0.0001;
const float MOMENTUM = 0.9;
const int NUM_EPOCHS = 100;

/*

 could be equal to any particular moment/trait's items/tokens, how to specify?
 the problem with making inputs invariant, is they can't be referenced specifically

 action.kind  (BasicActionKind)
   Relevant, 1st
 action.item1 (Item?)
   X, Relevant
 action.item2 (Item?)
   X, Y, Relevant
 event.item  (Item)
   X, Y, 1st Before, 2nd Before, 1st After, 2nd After, action.item1, action.item2
 event.key   (Tk)
   X, Y, 1st Before, 2nd Before, 1st After, 2nd After
 event.value (Item/Tk)
   X, Y, 1st Before, 2nd Before, 1st After, 2nd After, action.item1, action.item2
 cond1.item  (Item)
   X, Y, 1st After, 2nd After, 1st Before, 2nd Before, action.item1, action.item2, event.item
 cond1.key   (Tk)
   X, Y, 1st After, 2nd After, 1st Before, 2nd Before, event.key
 cond1.value (Item/Tk)
   X, Y, 1st After, 2nd After, 1st Before, 2nd Before, action.item1, action.item2, event.key
 cond2.item  (Item)
   ditto
 cond2.key   (Tk)
   ditto
 cond2.value (Item/Tk)
   ditto

 cond - and
 cond - or

*/

EventDiscernerNet::EventDiscernerNet() {
	LogicNetworkConfig config;

	/*int action_group = config.add_group(1);
	int in_action_kind  = config.add_input(action_group, DataType::ActionKind, true);
	int in_action_item1 = config.add_input(action_group, DataType::Item, true);
	int in_action_item2 = config.add_input(action_group, DataType::Item, true);

	int trait_group           = config.add_group(NUM_INPUT_MOMENTS * NUM_INPUT_TRAITS, true);
	int in_trait_item         = config.add_input(trait_group, DataType::Item, true);
	int in_trait_key          = config.add_input(trait_group, DataType::Tk, true);
	int in_trait_value_before = config.add_input(trait_group, DataType::ItemTk, true);
	int in_trait_value_after  = config.add_input(trait_group, DataType::ItemTk, true);
	int in_trait_moment       = config.add_input(trait_group, DataType::Moment, true);
	int in_trait_cause_kind   = config.add_input(trait_group, DataType::ActionKind, true);
	int in_trait_cause_item1  = config.add_input(trait_group, DataType::Item, true);
	int in_trait_cause_item2  = config.add_input(trait_group, DataType::Item, true);*/

	/*int out_cause_kind  = config.add_output(DataType::ActionKind, { in_action_kind, in_trait_cause_kind }, false);
	int out_cause_item1 = config.add_output(DataType::Item, { in_action_item1, in_trait_cause_item1 }, true);
	int out_cause_item2 = config.add_output(DataType::Item, { in_action_item2, in_trait_cause_item2 }, true);
	int out_event_item  = config.add_output(DataType::Item, { in_trait_item }, true);
	int out_event_key   = config.add_output(DataType::Tk,   { in_trait_key }, true);
	int out_event_value = config.add_output(DataType::ItemTk, { in_trait_value_before, in_trait_value_after }, true);
	int out_cond1_item  = config.add_output(DataType::Item, { in_trait_value_before, in_trait_value_after }, true);
	int out_cond1_key   = config.add_output(DataType::Tk, { in_trait_key }, true);
	int out_cond1_value = config.add_output(DataType::ItemTk, { in_trait_value_before, in_trait_value_after }, true);
	int out_cond2_item  = config.add_output(DataType::Item, { in_trait_value_before, in_trait_value_after }, true);
	int out_cond2_key   = config.add_output(DataType::Tk, { in_trait_key }, true);
	int out_cond2_value = config.add_output(DataType::ItemTk, { in_trait_value_before, in_trait_value_after }, true);
	int out_cond_type   = config.add_output(DataType::Boolean, false);*/

	setup_paired_inputs();
	setup_network();
}

TraitVal EventDiscernerNet::get_before(RawInput& raw_input) {
	return raw_input.moment->get_before(raw_input.item, raw_input.attribute);
}

TraitVal EventDiscernerNet::get_after(RawInput& raw_input) {
	return raw_input.moment->get_after(raw_input.item, raw_input.attribute);
}

void EventDiscernerNet::train(Vec<InputData>& data, const Vec<MutCausal>& raw_outputs) {
	assert(data.size() == raw_outputs.size());

	fl::Tensor inputs(fl::Shape({ NUM_INPUT_MOMENTS * NUM_INPUT_TRAITS * NUM_SUBINPUTS, (long)data.size() }));
	for (size_t i = 0; i < data.size(); i++) {
		inputs(i) = prep_input(data[i]);
	}

	fl::Tensor outputs(fl::Shape({ NUM_OUTPUTS, (long)raw_outputs.size() }));
	for (size_t i = 0; i < raw_outputs.size(); i++) {
		outputs(i) = prep_output(raw_outputs[i]);
	}

	fl::TensorDataset dataset({ inputs, outputs });
	fl::SGDOptimizer optimizer(model.params(), LEARNING_RATE, MOMENTUM);
	fl::AverageValueMeter meter;
	fl::MeanSquaredError loss;

	for (int e = 0; e < NUM_EPOCHS; e++) {
		meter.reset();
		/*for (auto& sample : fl::DatasetIterator(&dataset)) {
			optimizer.zeroGrad();
			auto result = model(fl::input(sample[0]));
			auto l = loss(result, fl::noGrad(sample[1]));
			l.backward();
			optimizer.step();
			meter.add(l.scalar<float>());
		}*/
	}
}

/*MutCausal EventDiscernerNet::run(InputData& data) {
	fl::Tensor input = prep_input(data);
	auto result = model(fl::input(input));
	return parse_output(result);
}*/

fl::Tensor EventDiscernerNet::prep_input(InputData& data) {
	assert((int)data.moments.size() == NUM_INPUT_MOMENTS);
	assert((int)data.traits.size() == NUM_INPUT_TRAITS);
	Vec<RawInput> raw_inputs;
	for (auto& moment : data.moments) {
		for (const Pair<ItemId, Tk>& trait : data.traits) {
			raw_inputs.emplace_back(moment, trait.first, trait.second);
		}
	}

	fl::Tensor input(fl::Shape({ (long)(paired_inputs.size() * NUM_SUBINPUTS) }));
	for (size_t i = 0; i < paired_inputs.size(); i++) {
		RawInput& raw_input1 = raw_inputs[paired_inputs[i].first];
		RawInput& raw_input2 = raw_inputs[paired_inputs[i].second];
		input(i*2)     = (get_before(raw_input1) == get_before(raw_input2));
		input(i*2 + 1) = (get_after(raw_input1)  == get_after(raw_input2));
	}

	return input;
}

enum class ActionKindOutputs {
	Relevant,
	First,
	Second,
	COUNT
};

enum class ActionItem1Outputs {
	X,
	Relevant,
	EventItem,
	COUNT
};

enum class ActionItem2Outputs {
	X, Y,
	Relevant,
	EventItem,
	COUNT
};

fl::Tensor EventDiscernerNet::prep_output(const MutCausal& output) {

}

MutCausal EventDiscernerNet::parse_output(fl::Tensor& output) {

}

void EventDiscernerNet::setup_paired_inputs() {
	for (int i = 0; i < NUM_INPUT_MOMENTS * NUM_INPUT_TRAITS - 1; i++) {
		for (int j = i + 1; j < NUM_INPUT_MOMENTS * NUM_INPUT_TRAITS; j++) {
			paired_input_map[std::make_pair(i, j)] = paired_inputs.size();
			paired_inputs.emplace_back(i, j);
		}
	}
}

void EventDiscernerNet::setup_network() {
	int total_inputs = choose2(NUM_INPUT_MOMENTS * NUM_INPUT_TRAITS) * NUM_SUBINPUTS;

	model.add(PermutationLayer(gen_permutations(), total_inputs, LAYER_1_WIDTH));
	model.add(fl::ReLU());
	model.add(fl::Linear(LAYER_1_WIDTH, HIDDEN_LAYER_WIDTHS));

	for (int i = 1; i < NUM_HIDDEN_LAYERS; i++) {
		model.add(fl::ReLU());
		model.add(fl::Linear(HIDDEN_LAYER_WIDTHS, HIDDEN_LAYER_WIDTHS));
	}

	model.add(fl::ReLU());
	model.add(fl::Linear(HIDDEN_LAYER_WIDTHS, NUM_OUTPUTS));
}

Vec<Vec<fl::Index>> EventDiscernerNet::gen_permutations() {
	int total_permutations = factorial(NUM_INPUT_MOMENTS * NUM_INPUT_TRAITS);

	// Raw permutations with STL
	Vec<Vec<int>> raw_permutations;
	raw_permutations.emplace_back();
	for (int i = 0; i < NUM_INPUT_MOMENTS * NUM_INPUT_TRAITS; i++) {
		raw_permutations.back().push_back(i);
	}
	while ((int)raw_permutations.size() < total_permutations) {
		raw_permutations.push_back(raw_permutations.back());
		std::next_permutation(raw_permutations.back().begin(), raw_permutations.back().end());
	}

	// Paired permutations
	Vec<Vec<size_t>> paired_permutations;
	for (auto& raw_permutation : raw_permutations) {
		paired_permutations.emplace_back();
		for (size_t i = 0; i < raw_permutation.size(); i++) {
			auto& pair = paired_inputs[i];
			auto permuted_pair = std::make_pair(raw_permutation[pair.first], raw_permutation[pair.second]);
			paired_permutations.back().emplace_back(paired_input_map[permuted_pair]);
		}
	}

	// Split paired permutations
	Vec<Vec<fl::Index>> final;
	for (auto& paired_permutation : paired_permutations) {
		Vec<size_t> permutation;
		for (size_t index : paired_permutation) {
			for (int i = 0; i < NUM_SUBINPUTS; i++) {
				permutation.push_back(index * NUM_SUBINPUTS + i);
			}
		}
	}

	return final;
}
