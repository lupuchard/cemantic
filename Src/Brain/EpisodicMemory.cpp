#include "EpisodicMemory.h"
#include <memory>

EpisodicMemory::EpisodicMemory(const Visible& visible): visible(visible) {
	remember_moment(Action(), Vec<ItemId> { });
}

const Moment& EpisodicMemory::remember_moment(const Action& action, const Vec<ItemId>& relevant) {
	auto new_moment = new Moment(action, current_instant++, memory.size());

	for (ItemId item : relevant) {
		if (!new_moment->state.count(item)) {
			auto data = Map<Tk, MutTrait>();
			for (Tk tk : visible.item_attributes(item)) {
				data[tk] = MutTrait(tk, visible.item_attribute(item, tk));
			}
			for (Tk tk : visible.item_children(item)) {
				data[tk] = MutTrait(tk, visible.item_child(item, tk));
			}
			new_moment->state.insert(std::make_pair(item, std::move(data)));
		}
	}

	if (action != Action()) {
		//action_occurrence_map[action.get_hash()].insert(new_moment);
		//action_kind_occurrence_map[action.get_kind()].insert(new_moment);

		Vec<Pair<Action, Assignment>> forms = get_generic_forms_with_assignments(action);
		for (auto& form : forms) {
			Action& form_action = form.first;
			//auto& assignment = form.second;
			action_occurrence_map[form_action.get_hash()].emplace(new_moment, form.second);
		}
	}

	return *memory.emplace_back(new_moment);
}

const Moment& EpisodicMemory::current() const {
	return *memory.back();
}

const Moment& EpisodicMemory::get(size_t index) const {
	assert(index < memory.size());
	return *memory[index];
}

Vec<MemoryDiffHist> EpisodicMemory::get_occurrences() const {
	assert(false);
}

Vec<MaybeBox<Moment>> EpisodicMemory::get_occurrences(const Action& action) const {
	auto iter = action_occurrence_map.find(action.get_hash());
	if (iter == action_occurrence_map.end()) {
		return Vec<MaybeBox<Moment>>();
	}

	Vec<MaybeBox<Moment>> out;
	for (auto& pair : iter->second) {
		if (pair.second.length() == 0) {
			out.emplace_back(pair.first);
		} else {
			// If BasicAction is generic, we return generic forms of the moments

			MaybeBox moment(std::make_unique<Moment>(*pair.first));
			Map<ItemId, ItemId> inversion = pair.second.invert()[0];
			replace_keys(moment->state, [&](ItemId key) {
				auto iter = inversion.find(key);
				return (iter == inversion.end()) ? std::nullopt : std::optional<ItemId> { iter->second };
			});

			for (auto state_iter = moment->state.begin(); state_iter != moment->state.end(); ++state_iter) {
				for (auto trait_iter = state_iter.value().begin(); trait_iter != state_iter.value().end(); ++trait_iter) {
					Vec<ItemId> items = trait_iter->second.get_all_items();
					for (size_t i = 0; i < items.size(); i++) {
						auto find_iter = inversion.find(items[i]);
						if (find_iter != inversion.end()) {
							trait_iter.value().set_item(i, find_iter->second);
						}
					}
				}
			}
		}
	}

	return out;
}

/*Vec<MemoryDiffHist> EpisodicMemory::get_occurrences(BasicAction::Kind action) const {
	auto iter = action_kind_occurrence_map.find(action);
	if (iter == action_kind_occurrence_map.end()) {
		return Vec<MemoryDiffHist>();
	}

	Vec<MemoryDiffHist> occurrences;
	for (const Moment* moment : iter->second) {
		if (moment->index == memory.size() - 1) continue;
		MemoryDiffHist diff(*moment, *memory[moment->index + 1]);
		occurrences.push_back(std::move(diff));
	}

	return occurrences;
}*/

Box<MemoryDiff> EpisodicMemory::get_diff(const Moment& prev) const {
	if (prev.index == memory.size() - 1) {
		return std::make_unique<MemoryDiffCur>(prev);
	} else {
		return std::make_unique<MemoryDiffHist>(prev, *memory[prev.index + 1]);
	}
}

/*Vec<MemoryDiffHist> EpisodicMemory::get_changes(BasicAction::Kind action) const {
	Vec<MemoryDiffHist> occurrences = get_occurrences(action);
	Vec<MemoryDiffHist> changes;
	for (auto& diff : occurrences) {
		if (diff.get_changes().empty()) continue;
		changes.push_back(std::move(diff));*/

		/*for (Pair<ItemId, Map<Tk, MutTrait>> elem : moment->state) {
			for (Pair<Tk, MutTrait> trait : elem.second) {
				TraitVal before = trait.second.get_value();
				TraitVal after = (next_moment != nullptr ? next_moment->get_state(elem.first, trait.first) : Tk::Unknown);
				if (before == after) continue;
				changes.emplace_back();
				changes.back().moment_before = moment;
				changes.back().moment_after = next_moment;
				changes.back().item = elem.first;
				changes.back().key = trait.first;
				changes.back().before = before;
				changes.back().after = after;
			}
		}*/
//	}

//	return changes;
//}

// TODO: change the underlying data structure to make this more efficient?
const Vec<Pair<ItemId, Tk>>& MemoryDiff::get_changes() {
	if  (!changes_cached) {
		for (auto& key_value : before.state) {
			const Map<Tk, MutTrait>& bef_item = key_value.second;
			const Map<Tk, MutTrait>* cur_item = get_after(key_value.first);
			if (cur_item == nullptr) continue;

			for (auto elem : bef_item) {
				Tk key = elem.first;
				auto iter = cur_item->find(key);
				if (iter != cur_item->end() && iter->second != elem.second) {
					changes.emplace_back(key_value.first, key);
				}
			}
		}

		auto after = get_after();
		for (Pair<ItemId, Tk>& elem : after) {
			auto iter = before.state.find(elem.first);
			if (iter == before.state.end()) continue;
			if (iter->second.find(elem.second) == iter->second.end()) {
				changes.push_back(elem);
			}
		}

		changes_cached = true;
	}

	return changes;
}
