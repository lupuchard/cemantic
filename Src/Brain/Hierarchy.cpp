#include "Hierarchy.h"
#include "MathUtil.h"

const float LAMBDA_THRESHOLD = 0.5;
const float INSERTED_DIST_THRESHOLD = 5;
const float UNSHARED_KNOWLEDGE_WEIGHT = 0.01;

Hierarchy Hierarchy::create(Visible& visible, Log& log) {
	Hierarchy hierarchy(visible, log);

	hierarchy.prepare_data();
	if (hierarchy.data.empty()) {
		return hierarchy;
	}

	hierarchy.clink_clusterize();
	hierarchy.root_cluster = std::make_unique<Cluster>(hierarchy.to_cluster(hierarchy.linkage_matrix.size() - 1));
	hierarchy.clusterization_postprocessing(*hierarchy.root_cluster);

	return hierarchy;
}

void Hierarchy::reclassify(ItemData* new_item, Cluster& cluster) {
	prepare_data(cluster);

	if (new_item != nullptr) {
		data.emplace_back(new_item, 1);
	}

	if (data.size() <= 1) {
		assert(false);
		data.clear();
		return;
	}

	if (new_item == nullptr) {
		log.info("[Hierarchy] Reclassifying from modification - {} items", data.size());
	} else {
		log.info("[Hierarchy] Reclassifying from insert ({}) - {} items", visible.item_name(new_item->id), data.size());
	}

	clink_clusterize();

	Cluster new_cluster = to_cluster(linkage_matrix.size() - 1);
	Cluster* new_cluster_p;
	if (cluster.parent != nullptr) {
		int cluster_idx;
		auto& parent_triple = *cluster.parent->triple();
		for (cluster_idx = 0; cluster_idx < 3; cluster_idx++) {
			if (parent_triple[cluster_idx].get() == &cluster) {
				break;
			}
		}

		auto new_cluster_u = std::make_unique<Cluster>(std::move(new_cluster));
		new_cluster_p = new_cluster_u.get();
		new_cluster_p->parent = cluster.parent;
		(*cluster.parent->triple())[cluster_idx] = std::move(new_cluster_u);
	} else {
		assert(&cluster == root_cluster.get());
		root_cluster.reset(new Cluster(std::move(new_cluster)));
		//cluster = std::move(new_cluster);
		new_cluster_p = root_cluster.get();
	}

	clusterization_postprocessing(*new_cluster_p);
}

template<typename T>
void for_each(const std::array<std::unique_ptr<Cluster>, 3>& triple, T func) {
	// Triples always have at least 2 subclusters
	func(*triple[0]);
	func(*triple[1]);
	if (triple[2] != nullptr) func(*triple[2]);
}

template<typename T>
void for_each_i(const std::array<std::unique_ptr<Cluster>, 3>& triple, T func) {
	// Triples always have at least 2 subclusters
	func(*triple[0], 0);
	func(*triple[1], 1);
	if (triple[2] != nullptr) func(*triple[2], 2);
}

Hierarchy::ItemData& Hierarchy::item_data(ItemId item) {
	auto iter = item_data_map.find(item);
	if (iter == item_data_map.end()) {
		return new_item_data(item);
	}
	return *iter->second;
}

Hierarchy::ItemData& Hierarchy::new_item_data(ItemId item) {
	auto item_data = std::make_unique<ItemData>(item);
	item_data->update_traits(visible);
	data.emplace_back(item_data.get(), 1);
	ItemData& ret = *item_data;
	item_data_map[item] = std::move(item_data);
	return ret;
}

void Hierarchy::prepare_data() {
	auto& items = visible.all_items();
	for (ItemId item : items) {
		if (!item.is_null() && !visible.item_attributes(item).empty() && !visible.item_children(item).empty()) {
			new_item_data(item);
		}
	}
}

void Hierarchy::prepare_data(Cluster& cluster) {
	std::unordered_map<ItemData*, float> data_set;
	prepare_data(cluster, data_set);
	data.insert(data.end(), std::make_move_iterator(data_set.begin()), std::make_move_iterator(data_set.end()));
}

void Hierarchy::prepare_data(Cluster& cluster, std::unordered_map<ItemData*, float>& data_set) {
	auto items = cluster.subitems();
	if (items != nullptr) {
		for (auto& item_pair : *items) {
			if (item_pair.second <= 0) continue;
			ItemData* item = item_data_map[item_pair.first].get();
			auto iter = data_set.find(item);
			if (iter == data_set.end()) {
				data_set[item] = item_pair.second;
			} else {
				iter->second += item_pair.second;
			}
			item->clusters.erase(&cluster);
		}
	} else {
		for_each(*cluster.triple(), [&](auto& c){ prepare_data(c, data_set); });
	}
}

void Hierarchy::clusterization_postprocessing(Cluster& root) {
	determine_guardianship(root);
	gen_cluster_knowledge(root);
	fuzz_items(root);
	data.clear();
	linkage_matrix.clear();
}

void Hierarchy::remove(int32_t link) {
	if (link == -1) {
		return;
	}

	remove(linkage_matrix[link]->link1);
	remove(linkage_matrix[link]->link2);

	linkage_matrix[link].reset();
	unused_links.push_back(link);
}

const Cluster& Hierarchy::get_root() const {
	assert(root_cluster != nullptr);
	return *root_cluster;
}


// calls 'combine' on every element shared between sets and 'dont_combine' on every element not shared
template<typename R, typename C1, typename C2, typename F1, typename F2, typename F3>
R combine_sorted(const C1& set1, const C2& set2, F1 compare, F2 combine, F3 dont_combine) {
	R return_val = R();
	auto iter1 = set1.begin();
	auto iter2 = set2.begin();
	while (iter1 != set1.end() && iter2 != set2.end()) {
		auto& elem1 = maybe_deref(*iter1);
		auto& elem2 = maybe_deref(*iter2);
		if (compare(elem1, elem2)) {
			dont_combine(return_val, elem1, false);
			++iter1;
		} else if (compare(elem2, elem1)) {
			dont_combine(return_val, elem2, true);
			++iter2;
		} else {
			combine(return_val, elem1, elem2);
			++iter1;
			++iter2;
		}
	}
	while (iter1 != set1.end()) {
		dont_combine(return_val, maybe_deref(*iter1), false);
		++iter1;
	}
	while (iter2 != set2.end()) {
		dont_combine(return_val, maybe_deref(*iter2), true);
		++iter2;
	}
	return return_val;
}

// this one simply skips the unshared elements
template<typename R, typename C1, typename C2, typename F1, typename F2>
R combine_sorted(const C1& set1, const C2& set2, F1 compare, F2 combine) {
	R return_val = R();
	auto iter1 = set1.begin();
	auto iter2 = set2.begin();
	while (iter1 != set1.end() && iter2 != set2.end()) {
		auto& elem1 = maybe_deref(*iter1);
		auto& elem2 = maybe_deref(*iter2);
		if (compare(elem1, elem2)) {
			++iter1;
		} else if (compare(elem2, elem1)) {
			++iter2;
		} else {
			combine(return_val, elem1, elem2);
			++iter1;
			++iter2;
		}
	}
	return return_val;
}

float Hierarchy::dist(const ItemData& item1, const ItemData& item2) {
	//float unshared_weight = (item1.is_category() || item2.is_category()) ? 0 : UNSHARED_KNOWLEDGE_WEIGHT;

	float distance = combine_sorted<float>(item1.traits, item2.traits,
		[&](auto& t1, auto& t2) { return t1.v().compare_type(t2); },
		[&](float& r, auto& t1, auto& t2) { r += (float)(t1.v() != t2.v()); },
		[&](float& r, auto& t, bool w) { r += UNSHARED_KNOWLEDGE_WEIGHT; }
	);

	// TODO issue: causals would have no effect on distance, because only conflicts create distance, and causals can't currently conflict
	//distance += combine_sorted<float>(item1.get_causals(), item2.get_causals(),
	//	[&](auto& t1, auto& t2) { return t1.v().compare(t2); },
	//	[&](float& r, auto& t1, auto& t2) { /*r -= t1.get_confidence() * t2.get_confidence() * SHARED_CAUSAL_WEIGHT;*/ },
	//	[&](float& r, auto& t, bool w) { r += t.get_confidence() * UNSHARED_KNOWLEDGE_WEIGHT; }
	//);

	return distance;
}

float Hierarchy::dist(const ItemData& item, const Cluster& cluster) {
	//float unshared_weight = (item.is_category()) ? 0 : UNSHARED_KNOWLEDGE_WEIGHT;

	//float distance = combine_sorted<float>(item.get_causals(), cluster.causals,
	//	[&](auto& t1, auto& t2) { return generic_causal_compare(t1, t2); },
	//	[&](float& r, auto& t1, auto& t2) { /*r -= t1.get_confidence() * t2.get_confidence() * SHARED_CAUSAL_WEIGHT;*/ },
	//	[&](float& r, auto& t, bool w) { r += t.get_confidence() * UNSHARED_KNOWLEDGE_WEIGHT; }
	//);

	float distance = combine_sorted<float>(item.traits, cluster.traits,
		[&](auto& t1, auto& t2) { return t1.v().compare_type(t2); },
		[&](float& r, auto& t1, auto& t2) { r += (float)(t1.v() != t2.v()) * t2.get_confidence(); },
		[&](float& r, auto& t, bool w) { r += t.get_confidence() * UNSHARED_KNOWLEDGE_WEIGHT; }
	);

	return distance;
}

int32_t Hierarchy::slink_clusterize() {
	arma::uvec pies(data.size(), arma::fill::zeros);
	arma::fvec lambdas(data.size(), arma::fill::zeros);
	arma::fvec mus(data.size(), arma::fill::zeros);

	for (size_t n = 1; n < data.size(); n++) {
		pies[n] = n;
		lambdas[n] = std::numeric_limits<float>::infinity();

		for (size_t i = 0; i < n; i++) {
			mus[i] = dist(*data[i].first, *data[n].first);
		}

		for (size_t i = 0; i < n; i++) {
			if (lambdas[i] >= mus[i]) {
				mus[pies[i]] = std::min(mus[pies[i]], lambdas[i]);
				lambdas[i] = mus[i];
				pies[i] = n;
			} else {
				mus[pies[i]] = std::min(mus[pies[i]], mus[i]);
			}
		}

		for (size_t i = 0; i < n; i++) {
			if (lambdas[i] >= lambdas[pies[i]]) {
				pies[i] = n;
			}
		}
	}

	return to_linkage_matrix(pies, lambdas);
}

// "An efficient algorithm for a complete link method" D. Defays
int32_t Hierarchy::clink_clusterize() {
	arma::uvec pies(data.size(), arma::fill::zeros);
	arma::fvec lambdas(data.size(), arma::fill::zeros);
	arma::fvec mus(data.size(), arma::fill::zeros);

	for (size_t n = 1; n < data.size(); n++) {
		pies[n] = n;
		lambdas[n] = std::numeric_limits<float>::infinity();

		for (size_t i = 0; i < n; i++) {
			mus[i] = dist(*data[i].first, *data[n].first);
		}

		for (size_t i = 0; i < n; i++) {
			if (lambdas[i] < mus[i]) {
				mus[pies[i]] = std::max(mus[pies[i]], mus[i]);
				mus[i] = std::numeric_limits<float>::infinity();
			}
		}

		size_t a = n - 1;
		for (size_t i = 0; i < n; i++) {
			if (lambdas[n - i - 1] >= mus[pies[n - i - 1]]) {
				if (mus[n - i - 1] < mus[a]) {
					a = n - i - 1;
				}
			} else {
				mus[n - i - 1] = std::numeric_limits<float>::infinity();
			}
		}

		size_t b = pies[a];
		float c = lambdas[a];
		pies[a] = n;
		lambdas[a] = mus[a];

		if (a < n - 1) {
			while (true) {
				if (b < n - 1) {
					size_t d = pies[b];
					float e = lambdas[b];
					pies[b] = n;
					lambdas[b] = c;
					b = d;
					c = e;
					continue;
				} else if (b == n - 1) {
					pies[b] = n;
					lambdas[b] = c;
				}

				break;
			}
		}

		for (size_t i = 0; i < n; i++) {
			if (pies[pies[i]] == n && lambdas[i] >= lambdas[pies[i]]) {
				pies[i] = n;
			}
		}
	}

	return to_linkage_matrix(pies, lambdas);
}

uint32_t find(std::vector<uint32_t>& vec, uint32_t a) {
	if (vec[a] == a) {
		return a;
	}

	vec[a] = find(vec, vec[a]);
	return vec[a];
}

void join(std::vector<uint32_t>& vec, uint32_t a, uint32_t b) {
	uint32_t a_parent = find(vec, a);
	uint32_t b_parent = find(vec, b);

	// Make sure that the biggest number is the representative
	if (b_parent > a_parent) {
		vec[a_parent] = b_parent;
	} else {
		vec[b_parent] = a_parent;
	}
}

int32_t Hierarchy::repr_to_link(uint32_t repr, const std::vector<int32_t>& lm_indices, ItemId& item, float& conf) {
	if (repr < data.size()) {
		item = data[repr].first->id;
		conf = data[repr].second;
		return -1;
	}

	return lm_indices[repr - data.size()];
}

int32_t Hierarchy::to_linkage_matrix(const arma::uvec& pies, const arma::fvec& lambdas) {
	assert(pies.size() == data.size());
	assert(lambdas.size() == data.size());

	std::vector<uint32_t> union_find;
	union_find.resize(data.size() * 2);
	for (size_t i = 0; i < data.size() * 2; i++) {
		union_find[i] = i;
	}

	std::vector<std::pair<int, float>> iv;
	iv.reserve(data.size());
	for (size_t i = 0; i < data.size(); i++) {
		iv.emplace_back(i, lambdas[i]);
	}

	sort(iv.begin(), iv.end(), [](const std::pair<int, float>& a, const std::pair<int, float>& b) -> bool {
		return a.second < b.second;
	});

	std::vector<int32_t> linkage_matrix_indices;
	linkage_matrix_indices.reserve(data.size());
	int32_t new_link_index;
	for (size_t i = 0; i < data.size() - 1; i++) {
		int index = iv[i].first;
		float lambda = lambdas[index];
		int pie = pies[index];

		uint32_t repr_index = find(union_find, index);
		uint32_t repr_pie = find(union_find, pie);

		new_link_index = unused_links.empty() ? linkage_matrix.size() : unused_links.back();
		linkage_matrix_indices.push_back(new_link_index);

		ItemId item1, item2;
		float conf1, conf2;
		int32_t link1 = repr_to_link(repr_index, linkage_matrix_indices, item1, conf1);
		int32_t link2 = repr_to_link(repr_pie, linkage_matrix_indices, item2, conf2);

		if (unused_links.empty()) {
			linkage_matrix.push_back(std::make_unique<Link>(item1, item2, conf1, conf2, link1, link2, lambda));
		} else {
			linkage_matrix[unused_links.back()].reset(new Link(item1, item2, conf1, conf2, link1, link2, lambda));
			unused_links.pop_back();
		}

		join(union_find, index, lambdas.size() + i);
		join(union_find, pie, lambdas.size() + i);
	}

	return new_link_index; // Returns root link
}

Cluster Hierarchy::to_cluster(int32_t link_idx) {
	Link& link = *linkage_matrix[link_idx];
	Cluster cluster1 = (link.item1.is_null() ? to_cluster(link.link1) : Cluster(link.lambda, link.item1, link.conf1));
	Cluster cluster2 = (link.item2.is_null() ? to_cluster(link.link2) : Cluster(link.lambda, link.item2, link.conf2));
	auto items1 = cluster1.subitems();
	auto items2 = cluster2.subitems();

	if (items1 == nullptr && items2 == nullptr) {
		// If both clusters contain subclusters, and one has sufficiently
		// low lambda difference, we kill it and takes its children.
		auto& triple1 = *cluster1.triple();
		auto& triple2 = *cluster2.triple();
		float lambda1 = (triple1[2] == nullptr ? cluster1.lambda : -std::numeric_limits<float>::infinity());
		float lambda2 = (triple2[2] == nullptr ? cluster2.lambda : -std::numeric_limits<float>::infinity());
		if (link.lambda - std::max(lambda1, lambda2) < LAMBDA_THRESHOLD) {
			Cluster new_cluster(link.lambda);
			auto& triple = *new_cluster.triple();
			if (cluster1.lambda > cluster2.lambda) {
				triple[0] = std::move(triple1[0]);
				triple[1] = std::move(triple1[1]);
				triple[2] = std::make_unique<Cluster>(std::move(cluster2));
			} else {
				triple[0] = std::move(triple2[0]);
				triple[1] = std::move(triple2[1]);
				triple[2] = std::make_unique<Cluster>(std::move(cluster1));
			}
			return new_cluster;
		}
	} else if (link.lambda - std::min(cluster1.lambda, cluster2.lambda) < LAMBDA_THRESHOLD) {
		if (items1 != nullptr && items2 != nullptr) {
			items1->insert(items1->end(), items2->begin(), items2->end());
			return cluster1;
		} else if (items1 == nullptr) {
			auto& triple = *cluster1.triple();
			if (triple[2] == nullptr) {
				triple[2] = std::make_unique<Cluster>(std::move(cluster2));
				return cluster1;
			}
		} else {
			auto& triple = *cluster2.triple();
			if (triple[2] == nullptr) {
				triple[2] = std::make_unique<Cluster>(std::move(cluster1));
				return cluster2;
			}
		}
	}

	Cluster new_cluster(link.lambda);
	auto& triple = *new_cluster.triple();
	triple[0] = std::make_unique<Cluster>(std::move(cluster1));
	triple[1] = std::make_unique<Cluster>(std::move(cluster2));
	return new_cluster;
}

void Hierarchy::gen_cluster_knowledge(Cluster& cluster) {
	auto triple_ptr = cluster.triple();
	if (triple_ptr != nullptr) {
		auto& triple = *triple_ptr;
		for_each(triple, [&](Cluster& c){ gen_cluster_knowledge(c); });

		Vec<Box<Trait>> traits = trait_intersect(triple[0]->traits, triple[1]->traits);
		Vec<Box<Causal>> causals = causal_intersect(triple[0]->causals, triple[1]->causals);

		if (triple[2] == nullptr) {
			cluster.traits = std::move(traits);
			cluster.causals = std::move(causals);
		} else {
			cluster.traits = trait_intersect(std::move(traits), triple[2]->traits);
			cluster.causals = causal_intersect(std::move(causals), triple[2]->causals);
		}

		return;
	}

	auto& items = *cluster.subitems();
	if (items.size() <= 1) {
		assert(!items.empty());
		ItemData& item = item_data(items[0].first);
		for (auto& trait : item.traits) {
			cluster.traits.push_back(Trait::box(trait));
		}
		for (auto& causal : item.causals) {
			cluster.causals.push_back(Causal::box(causal.v().convert()));
		}
		return;
	}

	// TODO: should weight be higher when item is a predefined category?

	float total_weight = items[0].second + items[1].second;
	assert(total_weight > 0);
	auto& item1 = item_data(items[0].first);
	auto& item2 = item_data(items[1].first);
	auto traits = trait_union(item1.traits, item2.traits, items[0].second / total_weight);
	auto causals = causal_union(item1.causals, item2.causals, items[0].second / total_weight);
	for (size_t i = 2; i < items.size(); i++) {
		if (items[i].second <= 0) continue;
		total_weight += items[i].second;
		ItemData& item = item_data(items[i].first);
		traits = trait_union(item.traits, std::move(traits), items[i].second / total_weight);
		causals = causal_union(item.causals, std::move(causals), items[i].second / total_weight);
	}
	cluster.traits = std::move(traits);
	cluster.causals = std::move(causals);
}

template<typename C1, typename C2>
Vec<Box<Trait>> Hierarchy::trait_intersect(const C1& traits1, const C2& traits2) {
	return combine_sorted<Vec<Box<Trait>>>(traits1, traits2,
		[&](auto& t1, auto& t2) { return t1.v().compare_type(t2); },
		[&](Vec<Box<Trait>>& out, auto& t1, auto& t2) { merge_traits(out, t1, t2, t1.get_confidence() * t2.get_confidence()); }
	);
}

template<typename C1, typename C2>
Vec<Box<Causal>> Hierarchy::causal_intersect(const C1& causals1, const C2& causals2) {
	return combine_sorted<Vec<Box<Causal>>>(causals1, causals2,
		[&](auto& t1, auto& t2) { return t1.v().compare(t2); },
		[&](Vec<Box<Causal>>& out, auto& t1, auto& t2) { push_knowledge(out, t1, t1.get_confidence() * t2.get_confidence()); }
	);
}

template<typename C1, typename C2>
Vec<Box<Trait>> Hierarchy::trait_union(const C1& traits1, const C2& traits2, float weight1) {
	float weight2 = 1 - weight1;
	return combine_sorted<Vec<Box<Trait>>>(traits1, traits2,
		[&](auto& trait1, auto& trait2) {
			return trait1.v().compare_type(trait2);
		},
		[&](Vec<Box<Trait>>& out, auto& trait1, auto& trait2) {
			float confidence = trait1.get_confidence() * weight1 + trait2.get_confidence() * weight2;
			merge_traits(out, trait1, trait2, confidence);
		},
		[&](Vec<Box<Trait>>& out, auto& trait, bool which) {
			push_knowledge(out, trait, trait.get_confidence() * (which ? weight2 : weight1));
		}
	);
}

template<typename C1, typename C2>
Vec<Box<Causal>> Hierarchy::causal_union(const C1& causals1, const C2& causals2, float weight1) {
	float weight2 = 1 - weight1;
	return combine_sorted<Vec<Box<Causal>>>(causals1, causals2,
		[&](auto& causal1, auto& causal2) {
			return generic_causal_compare(causal1, causal2);
		},
		[&](Vec<Box<Causal>>& out, auto& causal1, auto& causal2) {
			float confidence = causal1.get_confidence() * weight1 + causal2.get_confidence() * weight2;
			push_knowledge(out, Causal::make(generic_causal_convert(causal1)), confidence);
		},
		[&](Vec<Box<Causal>>& out, auto& causal, bool which) {
			push_knowledge(out, Causal::make(generic_causal_convert(causal)), causal.get_confidence() * (which ? weight2 : weight1));
		}
	);
}

void Hierarchy::merge_traits(Vec<Box<Trait>>& traits, const Trait& trait1, const Trait& trait2, float conf) {
	assert(conf > 0);
	/*if (!is_enum_attrib(trait1.v().get_key())) {
		float min_val = std::min(trait1.v().get_value(), trait2.v().get_value());
		float max_val = std::max(trait1.v().get_max(), trait2.v().get_max());
		traits.push_back(Trait::box(trait1.v().get_attribute(), min_val, max_val));
		traits.back()->set_confidence(conf);
	} else */if (trait2.same(trait1)) {
		push_knowledge(traits, trait1, conf);
	}
}

template<typename T>
void Hierarchy::push_knowledge(Vec<Box<T>>& knowledge_set, const T& knowledge, float conf) {
	// TODO: this knowledge is being copied, and may not need to be?
	auto new_knowledge = knowledge;
	new_knowledge.set_confidence(conf);
	knowledge_set.push_back(std::make_unique<T>(std::move(new_knowledge)));
}

void Hierarchy::determine_guardianship(Cluster& cluster) {
	auto triple = cluster.triple();
	if (triple != nullptr) {
		for_each(*triple, [&](auto& c){
			c.parent = &cluster;
			determine_guardianship(c);
		});
	}
}

// Sometimes, items belong in multiple clusters. Fuzzing determines which clusters the item belongs in
// and it's weight for each cluster.
// This function also assigns Item::clusters
void Hierarchy::fuzz_items(Cluster& cluster) {
	// TODO: fuzzing shouldn't lead to duplicates within a single item cluster!

	auto triple = cluster.triple();
	if (triple != nullptr) {
		for_each(*cluster.triple(), [&](auto& c){ fuzz_items(c); });
		return;
	}

	if (cluster.parent == nullptr) {
		auto& subitems = *cluster.subitems();
		for (size_t i = 0; i < subitems.size(); i++) {
			ItemData& item = item_data(subitems[i].first);
			item.add_cluster(cluster, i);
		}
		return;
	}

	auto& subitems = *cluster.subitems();
	if (subitems.size() > 1) {
		// Only items with missing traits may be fuzzy
		size_t most = 0;
		Vec<ItemData*> items;
		items.reserve(subitems.size());
		for (size_t i = 0; i < subitems.size(); i++) {
			if (subitems[i].second < 1) { // TODO: this will result in not fuzzing already fuzzed items during reclassification
				items.push_back(nullptr);
			} else {
				ItemData& item = item_data(subitems[i].first);
				item.clear_clusters();
				item.add_cluster(cluster, i);
				items.push_back(&item);
				most = std::max(item.traits.size(), most);
			}
		}
		for (size_t i = 0; i < items.size(); i++) {
			if (items[i] != nullptr && items[i]->traits.size() < most) {
				// TODO: bug - fuzzed items always have one with confidence 1.0, which is wrong
				subitems[i].second = fuzz_items(*items[i], cluster);
			}
		}
	} else {
		ItemData& item = *item_data_map[subitems[0].first];
		item.clear_clusters();
		item.add_cluster(cluster, 0);
	}
}

float Hierarchy::fuzz_items(ItemData& item, Cluster& cluster) {
	std::vector<std::pair<float, Cluster*>> insertions;
	if (cluster.parent != nullptr) {
		fuzz_items(insertions, item, *cluster.parent, &cluster);
	}
	if (insertions.empty()) {
		return 1;
	}

	float cur_dist = dist(item, cluster);
	assert(cur_dist < 0.5);
	float total_weight = 0.5 - cur_dist;
	for (auto& pair : insertions) {
		assert(pair.second->item_cluster());
		total_weight += 0.5 - pair.first;
	}
	for (auto& pair : insertions) {
		assert((0.5 - pair.first) / total_weight < 1);
		auto& subitems = *pair.second->subitems();
		item.add_cluster(*pair.second, subitems.size());
		subitems.emplace_back(item.id, (0.5 - pair.first) / total_weight);
	}

	assert((0.5 - cur_dist) / total_weight < 1);
	return (0.5 - cur_dist) / total_weight;
}

// when ascending, "from" is equal to the cluster we ascended from
// when descending, "from" is null
void Hierarchy::fuzz_items(std::vector<std::pair<float, Cluster*>>& insertions, ItemData& item, Cluster& cluster, Cluster* from) {
	if (from != nullptr) {
		item.umbrella_cluster = &cluster;
	}

	// Descend!
	bool should_do_parent = false;
	for_each_i(*cluster.triple(), [&](Cluster& c, int i) {
		if (&c != from) {
			float distance = dist(item, c);
			if (distance < LAMBDA_THRESHOLD) {
				should_do_parent = true;
				if (c.item_cluster()) {
					insertions.emplace_back(distance, &c);
				} else {
					fuzz_items(insertions, item, c, nullptr);
				}
			}
		}
	});

	// Ascend!
	if (from != nullptr && should_do_parent && cluster.parent != nullptr) {
		fuzz_items(insertions, item, *cluster.parent, &cluster);
	}
}

void Hierarchy::insert(ItemId new_item) {
	insert(item_data(new_item), *root_cluster, false);
}

void Hierarchy::insert(ItemData& new_item, Cluster& cluster, bool should_fuzz) {
	auto items = cluster.subitems();
	if (items != nullptr) {
		new_item.add_cluster(cluster, items->size());
		items->emplace_back(new_item.id, 1);
		if (should_fuzz) {
			items->back().second = fuzz_items(new_item, cluster);
		}
		return;
	}

	auto& triple = *cluster.triple();
	float min_dist = std::numeric_limits<float>::infinity();
	int num_low_dists = 0;
	int min_dist_cluster = 0;
	for (int i = 0; i < 3; i++) {
		if (triple[i] != nullptr) {
			float distance = dist(new_item, *triple[i]);
			if (distance < min_dist) {
				min_dist = distance;
				min_dist_cluster = i;
			}
			if (distance < LAMBDA_THRESHOLD) {
				num_low_dists++;
			}
		}
	}

	cluster.inserted_dist += min_dist;
	if (cluster.inserted_dist > INSERTED_DIST_THRESHOLD) {
		reclassify(&new_item, cluster);
		return;
	}

	insert(new_item, *triple[min_dist_cluster], num_low_dists);
}

/*void Hierarchy::update(ItemId item_id, Trait* old_trait, const Trait& new_trait) {
	ItemData& item = *item_data_map[item_id];
	if (item.clusters.empty()) {
		insert(item_id);
		return;
	}

	Vec<Pair<Cluster*, size_t>> to_remove_from;

	// If item placement is fuzzy (it's in multiple clusters) we need to update it in each cluster.
	for (Pair<Cluster*, size_t> pair : item.clusters) {
		Cluster& cluster = *pair.first;
		// We want to ensure that the new knowledge still fits in the corresponding cluster.
		// If the new knowledge adds too much distance from the cluster, we reclassify.

		*//*if (cluster.item_holds_traits() && cluster.subitems()->size() <= 1) {
			// TODO: should we consider distance to parent knowledge in this case?
			continue;
		}*//*

		const Trait* cluster_knowledge = cluster.get_same_trait_type(new_trait);
		if (cluster_knowledge == nullptr) {
			// TODO: magic numbers
			if (cluster.subitems()->size() > 10) {
				if (add_dist(cluster, item, 0.5 * new_trait.get_confidence())) return;
			}
			continue;
		}

		float dist_diff = dist(*cluster_knowledge, new_trait);
		if (old_trait != nullptr) {
			dist_diff -= dist(*cluster_knowledge, *old_trait);
		}

		if (dist_diff > LAMBDA_THRESHOLD) {
			to_remove_from.push_back(pair);
		}

		if (dist_diff > 0) {
			if (add_dist(cluster, item, dist_diff)) return;
		}
	}

	if (to_remove_from.size() == item.clusters.size()) {
		return;
	}

	float extra_weight = 0;
	for (Pair<Cluster*, size_t> pair : to_remove_from) {
		auto& subitems = *pair.first->subitems();
		extra_weight += subitems[pair.second].second;
		subitems[pair.second].second = 0;
		item.clusters.erase(pair.first);
	}

	for (Pair<Cluster*, size_t> pair : item.clusters) {
		(*pair.first->subitems())[pair.second].second += extra_weight / item.clusters.size();
	}
}*/

void Hierarchy::update(Causal& causal) {
	Vec<ItemId> items_vec = causal.v().get_all_items();
	auto items = std::unordered_set<ItemId>(items_vec.begin(), items_vec.end());
	for (ItemId item_id : items) {
		ItemData& item = item_data(item_id);
		item.causals.push_back(GenericCausal::make(causal, ItemId()));
		for (Pair<Cluster*, size_t> pair : item.clusters) {
			if (add_dist(*pair.first, item, causal.get_confidence() * UNSHARED_KNOWLEDGE_WEIGHT)) return;
		}
	}
}

bool Hierarchy::add_dist(Cluster& cluster, ItemData& item, float dist) {
	cluster.inserted_dist += dist;
	if (cluster.inserted_dist > INSERTED_DIST_THRESHOLD) {
		Cluster* umb_cluster = item.umbrella_cluster;
		assert(umb_cluster != nullptr);
		bool use_parent = item.clusters.contains(umb_cluster) && umb_cluster->parent != nullptr;
		reclassify(nullptr, use_parent ? *umb_cluster->parent : *umb_cluster);
		return true;
	}
	return false;
}
