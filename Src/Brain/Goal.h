#ifndef PROJECT_GOAL_H
#define PROJECT_GOAL_H

#include "Knowledge/Causal.h"
#include <unordered_map>

class Goal {
public:
	virtual ~Goal() = default;

	enum Kind {
		Realize,
		DoAction
	};

	explicit Goal(Kind kind): kind(kind) { }

	inline Goal* get_parent() { return parent; }
	inline void set_parent(Goal& parent) { this->parent = &parent; }

	inline Kind get_kind() const { return kind; }

	inline void add_subgoal(std::unique_ptr<Goal> goal) {
		Goal* goal_ptr = goal.get();
		subgoals[goal_ptr] = std::move(goal);
	}

	inline void remove_subgoal(Goal& goal) {
		subgoals.erase(&goal);
	}

	inline const std::unordered_map<Goal*, std::unique_ptr<Goal>>& get_subgoals() const {
		return subgoals;
	}

	virtual std::string to_string(const Visible& visible) const = 0;

	virtual void get_all_items(std::vector<ItemId>& items) const = 0;

private:
	Kind kind;
	std::unordered_map<Goal*, std::unique_ptr<Goal>> subgoals;
	Goal* parent = nullptr;
};

class RealizeGoal : public Goal {
public:
	explicit RealizeGoal(Boolean boolean): Goal(Realize), to_realize(boolean) { }
	explicit RealizeGoal(CnfFormula to_realize): Goal(Realize), to_realize(std::move(to_realize)) { }

	inline CnfFormula& get() { return to_realize; }

	inline std::string to_string(const Visible& visible) const override {
		return "Realize " + to_realize.to_string(visible);
	}

	inline void get_all_items(std::vector<ItemId>& items) const override {
		to_realize.get_all_items(items);
	}

private:
	CnfFormula to_realize;
};

class ActionGoal : public Goal {
public:
	explicit ActionGoal(Action action): Goal(DoAction), action(action) { }
	inline const Action& get() const { return action; }

	inline std::string to_string(const Visible& visible) const override {
		return "Do " + action.to_string(visible);
	}

	inline void get_all_items(std::vector<ItemId>& items) const override {
		action.get_all_items(items);
	}

private:
	Action action;
};

#endif //PROJECT_GOAL_H
