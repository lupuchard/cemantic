#include <iostream>
#include <filesystem>
#include "Formats/KnowledgeParser.h"
#include "Brain.h"
#include "Sim.h"
#include "ImGuiManager.h"
#include "SDL2Display.h"

Rando r(12);

Sim setup_sim() {
	Sim sim;

	ItemId hub = sim.new_item("Hub");
	sim.set_item_attribute(hub, Tk::Kind, Tk::Hub);

	ItemId shed = sim.new_item("Shed");
	sim.set_item_attribute(shed, Tk::Kind, Tk::Shed);

	ItemId lever1 = sim.new_item("Lever1");
	sim.set_item_attribute(lever1, Tk::Kind, Tk::Lever);
	sim.set_item_child(shed, Tk::Distance, lever1);

	ItemId lever2 = sim.new_item("Lever2");
	sim.set_item_attribute(lever2, Tk::Kind, Tk::Lever);
	sim.set_item_child(shed, Tk::Distance, lever2);

	ItemId lever3 = sim.new_item("Lever3");
	sim.set_item_attribute(lever3, Tk::Kind, Tk::Lever);
	sim.set_item_child(shed, Tk::Distance, lever3);

	return sim;
}

ItemId rand_item(Visible& sim) {
	auto& items = sim.all_items();
	return items[r.rand(0, items.size())];
}

EpisodicMemory generate_memory(Sim& sim) {
	EpisodicMemory mem(sim);

	for (int i = 0; i < 100; i++) {
		Action action(rand_item(sim));
		mem.remember_moment(action, sim.all_items());
		sim.perform_action(action);
	}

	return mem;
}

int main(int argc, char** argv) {
	Log log;

	Sim sim1 = setup_sim();
	SemanticMemory knowledge1(sim1, log);
	Box<Goal> goal = std::make_unique<ActionGoal>(Action(sim1.all_items()[0]));
	Brain brain(log, sim1, knowledge1, std::move(goal));

	Sim sim2 = setup_sim();
	SemanticMemory knowledge2(sim2, log);
	auto lever1 = sim2.get_item("Lever1");
	auto lever2 = sim2.get_item("Lever2");
	MutCausal test_causal1(
		Boolean(sim2.get_item("Hub"), Tk::State, Tk::One),
		Proposition(Boolean(lever1, Tk::Self, lever1))
	);
	MutCausal test_causal2(
		Boolean(sim2.get_item("Hub"), Tk::State, Tk::Two),
		Proposition(Boolean(lever2, Tk::Self, lever2))
	);
	sim2.enable_event(test_causal1);
	sim2.enable_event(test_causal2);
	EpisodicMemory memory = generate_memory(sim2);
	CausalClimber climber(log, knowledge2, memory, sim2);
	climber.begin_discern(Action(sim2.get_item("Lever1")));

	ImGuiManager imgui(brain, climber, log);
	return SDL2Display::show(imgui);
}

/*
 * MutCausal test_causal1(
		BasicAction(BasicAction::Touch, ItemId(1)),
		Boolean(ItemId(1), Tk::Active, Tk::True),
		Proposition(Boolean(ItemId(1), Tk::Kind, Tk::Lever))
	);
 */
