

Layout:
  Kind: Hub
  Label: Root
  Children:
  - Kind: Shed
    Quantity: 10
    Children:
    - Kind: Key
  - Kind: Shed
    Children:
    - Kind: Key
      Label: CorrectKey
      OnUse:
        Target: Root
        Set: "Root.State = Active"

Goal: "Root.State = Active"

KnownCausals:
- Kind: Use
  Item1: CorrectKey
  Item2: Root
  Event: "Root.State = Active"



# we need a new concept for known causal
# EXISTS (use(X, Root) => Root.Active) SUCH THAT X.Kind = Key
# or something
