
#include "Knowledge/Proposition.h"
#include "Evaluator.h"

Boolean boolean(int i) {
	return Boolean(ItemId(i), Tk::State, Tk::One);
}

Pair<ItemId, MutTrait> var(int i) {
	return std::make_pair<ItemId, MutTrait>(ItemId(i), MutTrait(Tk::State, Tk::One));
}

CnfFormula random_proposition(Vec<Boolean>& booleans, Rando& rando) {
	Vec<Vec<Boolean>> clauses;
	int num_terms = rando.rand(booleans.size(), booleans.size() * 2 + 1);
	for (int i = 0; i < num_terms; i++) {
		Boolean term = booleans[rando.rand(0, booleans.size())];
		if (rando.rand(0, 2) == 0) term.set_negated(true);

		if (clauses.empty() || rando.rand(0, 3) == 0) {
			clauses.emplace_back();
		}
		clauses.back().push_back(term);
	}

	return CnfFormula(clauses);
}

TEST_CASE("Proposition and_bool or_bool") {
	CnfFormula prop(boolean(1));

	prop.and_bool(boolean(2));

	Vec<Vec<Boolean>> case1 = { { boolean(1) }, { boolean(2) } };
	REQUIRE(prop == CnfFormula(case1));

	prop = prop.get_disjunction(boolean(3));

	Vec<Vec<Boolean>> case2 = { { boolean(1), boolean(3) }, { boolean(2), boolean(3) } };
	REQUIRE(prop == CnfFormula(case2));

	prop.and_bool(boolean(4));

	Vec<Vec<Boolean>> case3 = { { boolean(1), boolean(3) }, { boolean(2), boolean(3) }, { boolean(4) } };
	REQUIRE(prop == CnfFormula(case3));
}

TEST_CASE("Proposition stress test") {
	Rando rando(1235456);

	Sim sim;
	Evaluator evaluator(sim);
	Vec<Boolean> booleans;
	for (size_t i = 0; i < 10; i++) {
		ItemId item = sim.new_item();
		sim.set_item_attribute(item, Tk::State, Tk::One);
		booleans.emplace_back(item, Tk::State, Tk::One);

		for (size_t j = 0; j < (i + 1) * 5; j++) {
			auto prop = random_proposition(booleans, rando);
			Boolean b = booleans[rando.rand(0, booleans.size())];

			auto prop_or = prop.get_disjunction(b);
			if (evaluator.evaluate_bool(prop_or) != (evaluator.evaluate_bool(prop) || evaluator.evaluate_bool(b))) {
				std::cout << prop.to_string(sim) << " or " << b.to_string(sim) << " => " << prop_or.to_string(sim) << std::endl;
			}
			REQUIRE(evaluator.evaluate_bool(prop_or) == (evaluator.evaluate_bool(prop) || evaluator.evaluate_bool(b)));

			auto prop_and = prop;
			prop_and.and_bool(b);
			if (evaluator.evaluate_bool(prop_and) != (evaluator.evaluate_bool(prop) && evaluator.evaluate_bool(b))) {
				std::cout << prop.to_string(sim) << " and " << b.to_string(sim) << " => " << prop_and.to_string(sim) << std::endl;
			}
			REQUIRE(evaluator.evaluate_bool(prop_and) == (evaluator.evaluate_bool(prop) && evaluator.evaluate_bool(b)));

			auto prop2 = random_proposition(booleans, rando);
			prop_and = prop;
			prop_and.and_prop(prop2);
			if (evaluator.evaluate_bool(prop_and) != (evaluator.evaluate_bool(prop) && evaluator.evaluate_bool(prop2))) {
				std::cout << prop.to_string(sim) << " and " << prop2.to_string(sim) << " => " << prop_and.to_string(sim) << std::endl;
			}
			REQUIRE(evaluator.evaluate_bool(prop_and) == (evaluator.evaluate_bool(prop) && evaluator.evaluate_bool(prop2)));

			if (prop.num_terms() > 0) {
				auto prop_not = prop.get_negation();

				if ((bool)evaluator.evaluate_bool(prop_not) != !evaluator.evaluate_bool(prop)) {
					std::cout << "not " << prop.to_string(sim) << " => " << prop_not.to_string(sim) << std::endl;
				}
				REQUIRE((bool)evaluator.evaluate_bool(prop_not) == !evaluator.evaluate_bool(prop));
			}
		}
	}
}
