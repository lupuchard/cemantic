
#include "Hierarchy.h"
#include "Formats/GraphUtil.h"
#include "Sim.h"

Vec<Box<Trait>> traits;

void add_trait(Sim& sim, ItemId body, Tk attribute, Tk value) {
	traits.push_back(Trait::box(attribute, value));
	sim.set_item_attribute(body, attribute, value);
}

void add_trait(Sim& sim, ItemId body, Tk attribute, ItemId child) {
	traits.push_back(Trait::box(attribute, child));
	sim.set_item_child(body, attribute, child);
}

ItemId make_body(Sim& sim, std::string name, Tk radius, Tk mass, Tk distance, ItemId orbiting) {
	ItemId body = sim.new_item();
	item_names[body] = name;
	add_trait(sim, body, Tk::Radius, radius);
	add_trait(sim, body, Tk::Mass, mass);
	add_trait(sim, body, Tk::Distance, distance);
	add_trait(sim, body, Tk::Orbiting, orbiting);
	return body;
}

void print_cluster(const Cluster& cluster, const std::string& indent) {
	auto items = cluster.subitems();
	if (items != nullptr) {
		for (size_t i = 0; i < items->size(); i++) {
			//std::cout << indent << ::to_string((*items)[i].first) << std::endl;
		}
	} else {
		std::cout << indent << "." << std::endl;
		std::string new_indent = indent + "  ";
		auto& triple = *cluster.triple();
		for (int i = 0; i < 3; i++) {
			if (triple[i] != nullptr) {
				print_cluster(*triple[i], new_indent);
			}
		}
	}
}

void graph_cluster(Hierarchy& categorizer, const std::string& filename) {
	GraphUtil graph_util;
	std::ofstream out(filename, std::ofstream::out | std::ofstream::trunc);
	graph_util.hierarchy_to_graphml(categorizer, out, filename);
}

TEST_CASE("Categorizer", "[trait]") {
	Sim sim;

	ItemId sun = sim.new_item();

	// Gas giants
	ItemId earth   = make_body(sim, "Earth",   Tk::Medium,   Tk::Medium,   Tk::Low,      sun);
	ItemId jupiter = make_body(sim, "Jupiter", Tk::VeryHigh, Tk::VeryHigh, Tk::Medium,   sun);
	ItemId saturn  = make_body(sim, "Saturn",  Tk::VeryHigh, Tk::High,     Tk::High,     sun);
	ItemId uranus  = make_body(sim, "Uranus",  Tk::High,     Tk::Medium,   Tk::VeryHigh, sun);
	ItemId neptune = make_body(sim, "Neptune", Tk::High,     Tk::Medium,   Tk::VeryHigh, sun);
	/*make_body("Jupiter", 69900, 1898200,  820, Sun);
	make_body("Saturn",  58200,  568300, 1510, Sun);
	make_body("Uranus",  25400,   86800, 3010, Sun);
	make_body("Neptune", 24600,  102400, 4540, Sun);*/

	// Moons
	make_body(sim, "Ganymede", Tk::Low,     Tk::Low,     Tk::Medium,   jupiter);
	make_body(sim, "Callisto", Tk::Low,     Tk::Low,     Tk::Medium,   jupiter);
	make_body(sim, "Io",       Tk::VeryLow, Tk::Low,     Tk::Medium,   jupiter);
	make_body(sim, "Europa",   Tk::VeryLow, Tk::Low,     Tk::Medium,   jupiter);
	make_body(sim, "Titan",    Tk::Low,     Tk::Low,     Tk::High,     saturn);
	make_body(sim, "Titania",  Tk::VeryLow, Tk::VeryLow, Tk::VeryHigh, uranus);
	make_body(sim, "Triton",   Tk::VeryLow, Tk::VeryLow, Tk::VeryHigh, neptune);
	make_body(sim, "Luna",     Tk::VeryLow, Tk::Low,     Tk::Low,      earth);
	/*make_body("Ganymede", 2630,     148,  820, Jupiter);
	make_body("Callisto", 2410,     108,  820, Jupiter);
	make_body("Io",       1820,      89,  820, Jupiter);
	make_body("Europa",   1560,      48,  820, Jupiter);
	make_body("Titan",    2570,     134, 1510, Saturn);
	make_body("Titania",   788,       3, 3010, Uranus);
	make_body("Triton",   1350,      21, 4540, Neptune);
	make_body("Luna",     1740,      73,  152, Earth);*/

	Log log;
	Hierarchy categorizer = Hierarchy::create(sim, log);
	const Cluster& root_cluster = categorizer.get_root();
	print_cluster(root_cluster, "");

	/*auto& linkage_matrix = categorizer.get_linkage_matrix();
	for (size_t i = 0; i < linkage_matrix.size(); i++) {
		auto& link = linkage_matrix[i];
		std::string repr1 = (link.item1 == nullptr) ? std::to_string(link.link1) : Item::get(link.item1).to_string();
		std::string repr2 = (link.item2 == nullptr) ? std::to_string(link.link2) : Item::get(link.item2).to_string();
		std::string lambda = std::to_string(link.lambda);
		std::cout << "Cluster " << i << " (" << lambda << "): " << repr1 << ", " << repr2 << std::endl;
	}*/

	GraphUtil graph_util;
	//std::ofstream out1("correct.graphml");
	//graph_util.hierarchy_to_graphml(categorizer, out1);

	// Asteroids TODO: remaining
	categorizer.insert(make_body(sim, "Ceres", Tk::VeryLow, Tk::VeryLow, Tk::Medium, sun));
	//categorizer.insert(make_body("Ceres",      939,  0.938,  414, Sun));
	//graph_cluster(categorizer, "p2_ceres.graphml");
	//categorizer.insert(make_body("Vesta",      525,  0.259,  353, Sun));
	//graph_cluster(categorizer, "p3_vesta.graphml");
	//categorizer.insert(make_body("Pallas",     512,  0.204,  414, Sun));
	//graph_cluster(categorizer, "p4_pallas.graphml");
	//categorizer.insert(make_body("Hygiea",     434,  0.083,  470, Sun));
	//graph_cluster(categorizer, "p5_hygiea.graphml");
	//categorizer.insert(make_body("Interamnia", 332,  0.038,  458, Sun));
	//graph_cluster(categorizer, "p6_inter.graphml");
	//categorizer.insert(make_body("Davida",     289,  0.038,  474, Sun));
	//graph_cluster(categorizer, "p7_davida.graphml");
	//categorizer.insert(make_body("Sylvia",     286,  0.015,  521, Sun));
	//graph_cluster(categorizer, "p8_sylvia.graphml");
	//categorizer.insert(make_body("Euphrosyne", 268,  0.017,  471, Sun));
	//graph_cluster(categorizer, "p9_euphro.graphml");

	//std::ofstream out2("after_graph.graphml");
	//graph_util.hierarchy_to_graphml(categorizer, out2);
}
