#include "Rando.h"
//#include "Formats/KnowledgeReader.h"
#include "catch_amalgamated.hpp"
#include "MathUtil.h"
#include "TestUtil.h"
//#include "SolutionPathTests.cpp"
#include "CategorizerTests.cpp"
//#include "EventDiscernerTests.cpp"
#include "PropositionTests.cpp"
#include <iostream>
#include <map>
#include <utility>

void test_hash(uint64_t (*hash_func)(uint64_t, uint64_t)) {
	std::unordered_map<uint64_t, std::string> hashes;
	for (size_t i = 0; i < 1000; i++) {
		uint64_t val1 = i + 2;
		uint64_t val2 = rand() % (val1 + 1);
		std::string hash_str = std::to_string(val1) + ", " + std::to_string(val2);
		uint64_t hash = hash_func(val1, val2);

		auto iter = hashes.find(hash);
		if (iter == hashes.end()) {
			hashes[hash] = hash_str;
		} else {
			std::cout << "Collision between " + iter->second + " and " + hash_str << std::endl;
		}
	}

	std::cout << 1000 - hashes.size() << " total collisions" << std::endl;
	REQUIRE(hashes.size() >= 990);
}

template<typename U, typename V>
inline constexpr uint64_t hash_test(U hash1, V hash2) {
	return chc(chci(hash1), hash2);
}

TEST_CASE("Hash quality", "[hash]") {
	std::cout << chc(chc(chc(chci(_tpi(12)), _tpi(14)), _tpi(16)), _tpi(18)) << std::endl;
	test_hash(combine_hash);
	test_hash(hash_test);
}

TEST_CASE("AltFloat", "[float]") {
	REQUIRE(AltFloat(AltFloatEnum::ANY) != AltFloat(AltFloatEnum::NONE));
	REQUIRE(!AltFloat(AltFloatEnum::ANY).is_float());
	//REQUIRE(AltFloat(NAN).is_float());
	//REQUIRE(std::isnan(AltFloat(NAN)));
}


/*TEST_CASE("Reader", "[trait]") {
	Log log;
	KnowledgeReader reader = KnowledgeReader::create(log);
	auto results = reader.read("You must activate the hub. The hub will be active when its stat is reduced to zero.");

	for (auto& str : log.get()) {
		std::cout << str;
	}
	std::cout << std::flush;
}*/


TEST_CASE("VarUtil") {
	Boolean event1(ItemId(20), Tk::State, ItemId(21));
	Vec<Boolean> forms1 = get_generic_forms(event1);

	Vec<Boolean> expected1 = Vec<Boolean> {
		Boolean(ItemId(20), Tk::State, ItemId(21)),
		Boolean(ItemId(1), Tk::State, ItemId(21)),
		Boolean(ItemId(20), Tk::State, ItemId(1)),
		Boolean(ItemId(1), Tk::State, ItemId(2)),
	};

	REQUIRE(is_permutation(forms1, expected1));


	Boolean event2(ItemId(20), Tk::State, ItemId(20));
	Vec<Boolean> forms2 = get_generic_forms(event2);

	Vec<Boolean> expected2 = Vec<Boolean> {
		Boolean(ItemId(20), Tk::State, ItemId(20)),
		Boolean(ItemId(1), Tk::State, ItemId(20)),
		Boolean(ItemId(20), Tk::State, ItemId(1)),
		Boolean(ItemId(1), Tk::State, ItemId(1)),
		Boolean(ItemId(1), Tk::State, ItemId(2)),
	};

	REQUIRE(is_permutation(forms2, expected2));


	Boolean event3(ItemId(1), Tk::State, ItemId(20));
	Vec<Boolean> forms3 = get_generic_forms(event3);

	Vec<Boolean> expected3 = Vec<Boolean> {
			Boolean(ItemId(1), Tk::State, ItemId(20)),
			Boolean(ItemId(1), Tk::State, ItemId(2)),
	};

	REQUIRE(is_permutation(forms3, expected3));


	Boolean event4(ItemId(20), Tk::State, ItemId(1));
	Vec<Boolean> forms4 = get_generic_forms(event4);

	Vec<Boolean> expected4 = Vec<Boolean> {
			Boolean(ItemId(20), Tk::State, ItemId(1)),
			Boolean(ItemId(1), Tk::State, ItemId(2)),
	};

	//std::cout << event4.to_string() << std::endl;
	//std::cout << vec_to_string(forms4) << std::endl;
	REQUIRE(is_permutation(forms4, expected4));
}


