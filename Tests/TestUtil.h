#ifndef WHAT_TESTUTIL_H
#define WHAT_TESTUTIL_H

#include "TypeUtil.h"

template<typename T>
bool is_permutation(const Vec<T>& vec1, const Vec<T>& vec2) {
	if (vec1.size() != vec2.size()) return false;
	for (auto& elem : vec1) {
		if (std::find(vec2.begin(), vec2.end(), elem) == vec2.end()) {
			return false;
		}
	}
	return true;
}

#endif //WHAT_TESTUTIL_H
