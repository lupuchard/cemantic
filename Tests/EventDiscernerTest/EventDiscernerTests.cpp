//#include "EventDiscerner.h"
#include "Sim.h"
#include "TestUtil.h"

const Action imm(ItemId());

TEST_CASE("EventDiscerner no condition, obvious, specific item") {
	Sim sim;
	EpisodicMemory mem(sim);

	ItemId a = sim.new_item("a");
	ItemId b = sim.new_item("b");
	auto relevant_items = Vec<ItemId> { a, b };
	BasicAction action(BasicAction::Touch, b);

	// Case 1
	sim.set_item_attribute(a, Tk::State, Tk::One);
	sim.set_item_attribute(b, Tk::State, Tk::One);
	mem.remember_moment(imm, relevant_items);
	sim.set_item_attribute(b, Tk::State, Tk::Two);
	mem.remember_moment(action, relevant_items);

	// Case 2
	sim.set_item_attribute(b, Tk::State, Tk::Three);
	mem.remember_moment(imm, relevant_items);
	sim.set_item_attribute(b, Tk::State, Tk::Two);
	mem.remember_moment(action, relevant_items);

	// Case 3
	sim.set_item_attribute(a, Tk::State, Tk::Two);
	sim.set_item_attribute(b, Tk::State, Tk::One);
	mem.remember_moment(imm, relevant_items);
	sim.set_item_attribute(b, Tk::State, Tk::Two);
	mem.remember_moment(action, relevant_items);

	Log log(true);
	EventDiscerner discerner(mem, log);
	Vec<MutCausal> causals = discerner.discern_event(action);

	MutCausal expected(action, Boolean(b, MutTrait(Tk::State, Tk::Two)));
	REQUIRE(causals.size() == 1);
	REQUIRE(causals[0] == expected);
}

TEST_CASE("EventDiscerner no condition, ambiguous, specific item") {
	Sim sim;
	EpisodicMemory mem(sim);

	ItemId a = sim.new_item("a");
	ItemId b = sim.new_item("b");
	auto relevant_items = Vec<ItemId> { a, b };
	BasicAction action(BasicAction::Touch, b);

	// Case 1
	sim.set_item_attribute(a, Tk::State, Tk::One);
	sim.set_item_attribute(b, Tk::State, Tk::One);
	mem.remember_moment(imm, relevant_items);
	sim.set_item_attribute(b, Tk::State, Tk::Two);
	mem.remember_moment(action, relevant_items);

	// Case 2
	sim.set_item_attribute(b, Tk::State, Tk::Three);
	mem.remember_moment(imm, relevant_items);
	sim.set_item_attribute(b, Tk::State, Tk::Two);
	mem.remember_moment(action, relevant_items);

	// Case 3
	sim.set_item_attribute(a, Tk::State, Tk::Two);
	sim.set_item_attribute(b, Tk::State, Tk::One);
	mem.remember_moment(imm, relevant_items);
	sim.set_item_attribute(b, Tk::State, Tk::Two);
	mem.remember_moment(action, relevant_items);

	Log log;
	EventDiscerner discerner(mem, log);
	Vec<MutCausal> causals = discerner.discern_event(action);

	std::cout << vec_to_string(causals) << std::endl;
	MutCausal expected(action, Boolean(b, MutTrait(Tk::State, Tk::Two)));
	REQUIRE(causals.size() == 1);
	REQUIRE(causals[0] == expected);
}

TEST_CASE("EventDiscerner no condition, variable item") {
	Sim sim;
	EpisodicMemory mem(sim);

	ItemId a = sim.new_item("a");
	ItemId b = sim.new_item("b");
	auto relevant_items = Vec<ItemId> { a, b };
	BasicAction action_a(BasicAction::Touch, a);
	BasicAction action_b(BasicAction::Touch, b);

	// Case 1-2
	sim.set_item_attribute(a, Tk::State, Tk::One);
	sim.set_item_attribute(b, Tk::State, Tk::One);
	mem.remember_moment(imm, relevant_items);
	sim.set_item_attribute(a, Tk::State, Tk::Two);
	mem.remember_moment(action_a, relevant_items);
	sim.set_item_attribute(b, Tk::State, Tk::Two);
	mem.remember_moment(action_b, relevant_items);

	// Case 3-4
	sim.set_item_attribute(a, Tk::State, Tk::One);
	sim.set_item_attribute(b, Tk::State, Tk::Three);
	mem.remember_moment(imm, relevant_items);
	sim.set_item_attribute(b, Tk::State, Tk::Two);
	mem.remember_moment(action_b, relevant_items);
	sim.set_item_attribute(a, Tk::State, Tk::Two);
	mem.remember_moment(action_a, relevant_items);

	Log log;
	EventDiscerner discerner(mem, log);
	Vec<MutCausal> causals = discerner.discern_event(action_a);
	// Problem: discern(action_a) and discern(action_b) will only get occurrences for those relevant actions
	// we would need to look at ALL actions to be able to discern, say, act(x): x.state 1->2
	// but we don't actually want to look at all actions every time

	MutCausal expected(BasicAction(BasicAction::Touch, ItemId(1)), Boolean(ItemId(1), MutTrait(Tk::State, Tk::Two)));
	REQUIRE(causals.size() == 1);

	std::cout << causals[0].to_string() << " == " << expected.to_string() << std::endl;
	REQUIRE(causals[0] == expected);
}

 /*TEST_CASE("EventDiscerner one condition") {
	Sim sim;
	EpisodicMemory mem(sim);

	ItemId a = sim.new_item("a");
	ItemId b = sim.new_item("b");
	ItemId c = sim.new_item("c"); // noise
	auto relevant_items = Vec<ItemId> { a, b, c };
	BasicAction action(BasicAction::Touch, b);

	// Case 1
	sim.set_item_attribute(a, Tk::State, Tk::True);
	sim.set_item_attribute(b, Tk::State, Tk::One);
	sim.set_item_attribute(c, Tk::State, Tk::True);
	mem.remember_moment(imm, relevant_items);
	sim.set_item_attribute(b, Tk::State, Tk::Two);
	mem.remember_moment(action, relevant_items);

	// Case 2
	sim.set_item_attribute(a, Tk::State, Tk::False);
	sim.set_item_attribute(b, Tk::State, Tk::One);
	sim.set_item_attribute(c, Tk::State, Tk::True);
	mem.remember_moment(imm, relevant_items);
	sim.set_item_attribute(b, Tk::State, Tk::One);
	mem.remember_moment(action, relevant_items);

	// Case 3
	sim.set_item_attribute(a, Tk::State, Tk::True);
	sim.set_item_attribute(b, Tk::State, Tk::One);
	sim.set_item_attribute(c, Tk::State, Tk::False);
	mem.remember_moment(imm, relevant_items);
	sim.set_item_attribute(b, Tk::State, Tk::Two);
	mem.remember_moment(action, relevant_items);

	// Case 4
	sim.set_item_attribute(a, Tk::State, Tk::False);
	sim.set_item_attribute(b, Tk::State, Tk::One);
	sim.set_item_attribute(c, Tk::State, Tk::False);
	mem.remember_moment(imm, relevant_items);
	sim.set_item_attribute(b, Tk::State, Tk::One);
	mem.remember_moment(action, relevant_items);

	Log log;
	EventDiscerner discerner(mem, log);
	Vec<Causal> causals = discerner.discern_event(action);

	Proposition cond(Boolean(a, MutTrait(Tk::State, Tk::True)));
	MutCausal expected(action, Boolean(b, MutTrait(Tk::State, Tk::Two)), cond);

	REQUIRE(causals.size() == 1);
	std::cout << causals[0].v().to_string() << " " << expected.to_string() << std::endl;
	REQUIRE(causals[0].v() == expected);*/
//}

Boolean rand_bool(const Vec<ItemId>& items, const Vec<Tk>& keys, const Vec<Tk>& vals, Rando& rando) {
	ItemId item = rando.select(items);
	Tk key = rando.select(keys);
	Tk val = rando.select(vals);
	return Boolean(item, key, val);

	/*bool type = (bool)rando.rand(0, 1);
	ItemId item = items[rando.rand(0, 1)];
	Tk key = Tk(1 + (type ? num_keys : 0) + rando.rand(0, num_keys));
	auto event = type ?
			Boolean::attribute_equals(item, key, Tk(1 + num_keys * 2 + rando.rand(0, num_vals))) :
			Boolean::child_equals(item, key, items[rando.rand(0, items.size())]);*/
}

MutCausal random_causal(BasicAction action, const Vec<ItemId>& items, const Vec<Tk>& keys, const Vec<Tk>& vals, Rando& rando) {
	auto event = rand_bool(items, keys, vals, rando);
	int num_conditions = rando.rand(0, 3);
	if (num_conditions == 0) {
		return MutCausal(action, event);
	} else {
		Proposition cond(rand_bool(items, keys, vals, rando));
		for (int i = 1; i < num_conditions; i++) {
			cond.and_bool(rand_bool(items, keys, vals, rando));
		}
		return MutCausal(action, event, cond);
	}
}

/*TEST_CASE("EventDiscerner random stress test") {
	Rando rando(14);

	const int num_tests = 100;
	const int num_occurrences_per_test = 1000;
	const int max_items = 4;
	const int max_keys = 3;
	const int max_vals = 4;
	const int max_actions = 3;
	//const int max_causals_per_action = 2;

	for (int i = 0; i < num_tests; i++) {
		int num_items = rando.rand(1, max_items + 1);
		int num_keys = rando.rand(1, max_keys + 1);
		int num_vals = rando.rand(2, max_vals + 1);
		int num_actions = rando.rand(1, max_actions + 1);

		Vec<ItemId> items;
		for (int i = 0; i < num_items; i++) {
			items.push_back(ItemId(NUM_RESERVED_ITEM_IDS + i));
		}

		Vec<Tk> keys;
		for (int i = 0; i < num_keys; i++) {
			keys.push_back(Tk(1 + i));
		}

		Vec<Tk> vals;
		for (int i = 0; i < num_vals; i++) {
			vals.push_back(Tk(1 + num_keys + i));
		}

		Vec<BasicAction> actions;
		Vec<MutCausal> causals;
		for (int i = 0; i < num_actions; i++) {
			actions.emplace_back(BasicAction::Touch, ItemId(NUM_RESERVED_ITEM_IDS + i));
			causals.push_back(random_causal(actions.back(), items, keys, vals, rando));
		}

		Sim sim;
		Log log;
		SemanticMemory sem(sim, log);
		EpisodicMemory mem(sim);
		EventDiscerner discerner(mem);
		Evaluator eval(sem, sim);

		for (int i = 0; i < num_occurrences_per_test; i++) {
			for (ItemId item : items) {
				for (Tk key : keys) {
					sim.set_item_attribute(item, key, rando.select(vals));
				}
				mem.remember_moment(imm, items);

				size_t action = rando.rand(0, actions.size());
				MutCausal& causal = causals[action];
				if (eval.evaluate_bool(causal.get_condition())) {
					auto& trait = causal.get_event().get_trait();
					sim.set_item_attribute(causal.get_event().get_item(), trait.get_key(), trait.get_value().tk());
				}
				mem.remember_moment(actions[action], items);
			}
		}

		for (size_t i = 0; i < actions.size(); i++) {
			auto discerned = discerner.discern_event(actions[i]);
			REQUIRE(discerned.size() == 1);
			REQUIRE(discerned[0] == causals[i]);
		}
	}
}*/
