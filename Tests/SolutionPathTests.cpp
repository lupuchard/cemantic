
#include "SolutionPath/SolutionPath.h"
#include "TestUtil.h"
#include "Sim.h"

TEST_CASE("SolutionPath two single state change") {
	Log log;
	Sim sim;
	SemanticMemory sem(sim, log);

	ItemId a = sim.new_item();
	ItemId b = sim.new_item();
	sim.set_item_attribute(a, Tk::State, Tk::False);
	sim.set_item_attribute(b, Tk::State, Tk::False);
	sem.add_causal(Causal::make(BasicAction(BasicAction::Touch, a), Boolean(a, Tk::State, Tk::True)));
	sem.add_causal(Causal::make(BasicAction(BasicAction::Touch, b), Boolean(b, Tk::State, Tk::True)));

	Proposition goal(Boolean(a, Tk::State, Tk::True));
	goal.and_bool(Boolean(b, Tk::State, Tk::True));

	SolutionPath path(sem, sim);
	auto actions = path.find_path(goal);

	Vec<BasicAction> expected = Vec<BasicAction> {
			BasicAction(BasicAction::Touch, a),
			BasicAction(BasicAction::Touch, b),
	};

	//std::cout << vec_to_string(actions) << " == " << vec_to_string(expected) << std::endl;
	REQUIRE(is_permutation(actions, expected));
}

TEST_CASE("SolutionPath triple state change with condition") {
	Log log;
	Sim sim;
	SemanticMemory sem(sim, log);

	ItemId a = sim.new_item();
	ItemId b = sim.new_item();
	ItemId c = sim.new_item();
	sim.set_item_attribute(a, Tk::State, Tk::False);
	sim.set_item_attribute(b, Tk::State, Tk::False);
	sim.set_item_attribute(c, Tk::State, Tk::False);

	Proposition cond(Boolean(b, Tk::State, Tk::True));
	cond.and_bool(Boolean(c, Tk::State, Tk::True));
	sem.add_causal(Causal::make(BasicAction(BasicAction::Touch, a), Boolean(a, Tk::State, Tk::True), cond));
	sem.add_causal(Causal::make(BasicAction(BasicAction::Touch, b), Boolean(b, Tk::State, Tk::True)));
	sem.add_causal(Causal::make(BasicAction(BasicAction::Touch, c), Boolean(c, Tk::State, Tk::True)));

	Proposition goal(Boolean(a, Tk::State, Tk::True));

	SolutionPath path(sem, sim);
	auto actions = path.find_path(goal);

	Vec<BasicAction> expected = Vec<BasicAction> {
			BasicAction(BasicAction::Touch, c),
			BasicAction(BasicAction::Touch, b),
			BasicAction(BasicAction::Touch, a),
	};

	//std::cout << vec_to_string(actions) << std::endl;
	REQUIRE(is_permutation(actions, expected));
	REQUIRE(actions.back().get_item1() == a);
}

// TODO: This pathing checks 6 nodes, but with a better heuristic it could surely get away with only checking 4?
TEST_CASE("SolutionPath causals with variable") {
	Log log;
	Sim sim;
	SemanticMemory sem(sim, log);

	ItemId a = sim.new_item();
	ItemId b = sim.new_item();
	ItemId r = sim.new_item();

	sim.set_item_attribute(a, Tk::State, Tk::False);
	sim.set_item_attribute(b, Tk::State, Tk::False);

	sim.set_item_attribute(a, Tk::Parent, Tk::One);
	sim.set_item_attribute(b, Tk::Parent, Tk::One);

	/*sem.add_trait(Trait::make(Attribute::Active, false), a.id());
	sem.add_trait(Trait::make(Attribute::Active, false), b.id());

	sem.add_trait(Trait::make(Attribute::Loc, q.id().val), a.id());
	sem.add_trait(Trait::make(Attribute::Loc, q.id().val), b.id());*/

	sem.add_causal(Causal::make(BasicAction(BasicAction::Touch, ItemId(1)), Boolean(ItemId(1), Tk::State, Tk::True)));
	sem.add_causal(Causal::make(
			BasicAction(BasicAction::Use, r, ItemId(1)),
			Boolean(ItemId(1), Tk::Parent, Tk::Two),
			Proposition(Boolean(ItemId(1), Tk::State, Tk::True)))
	);

	Proposition goal(Boolean(a, Tk::Parent, Tk::Two));
	goal.and_bool(Boolean(b, Tk::Parent, Tk::Two));

	SolutionPath path(sem, sim);
	auto actions = path.find_path(goal);

	Vec<BasicAction> expected = Vec<BasicAction> {
			BasicAction(BasicAction::Touch, a),
			BasicAction(BasicAction::Touch, b),
			BasicAction(BasicAction::Use, r, a),
			BasicAction(BasicAction::Use, r, b),
	};

	//std::cout << vec_to_string(actions) << std::endl;
	REQUIRE(is_permutation(actions, expected));

	bool a_touched = false;
	bool b_touched = false;
	for (BasicAction& action : actions) {
		if (action == expected[0]) a_touched = true;
		if (action == expected[1]) b_touched = true;
		if (action == expected[2]) REQUIRE(a_touched);
		if (action == expected[3]) REQUIRE(b_touched);
	}
}

TEST_CASE("SolutionPath basic return tape") {
	Log log;
	Sim sim;
	SemanticMemory sem(sim, log);

	/*auto& a = Item::create("a");
	auto& b = Item::create("b");
	auto& q = Item::create("q");
	auto& r = Item::create("r");

	// TODO: R conditions would only show up in second causal?
	auto N = ItemId(1);
	auto V = ItemId(2);
	auto R = ItemId(3);
	Proposition construct_pre(Boolean::new_item_loc_equals(V, N));
	construct_pre.and_bool(Boolean::new_item_kind_equals(V, BasicItemKind::Vacant));
	construct_pre.and_bool(Boolean::new_item_kind_equals(R, BasicItemKind::Return));
	construct_pre.and_bool(Boolean::new_item_loc_equals(R, ItemId(0)));
	sem.add_causal(Causal::make(
			BasicAction(BasicAction::Use, r.id(), N),
			Event(Boolean::new_item_loc_equals(R, N)),
			construct_pre
	));
	sem.add_causal(Causal::make(
			BasicAction(BasicAction::Use, r.id(), N),
			Event(Boolean::new_item_loc_equals(V, ItemId(0))),
			construct_pre
	));



	sem.add_causal(Causal::make(BasicAction(BasicAction::Touch, ItemId(1)), Event(Boolean::new_item_active(ItemId(1)))));
	sem.add_causal(Causal::make(
			BasicAction(BasicAction::Use, r.id(), ItemId(1)),
			Event(Boolean::new_item_loc_equals(ItemId(1), r.id())),
			Proposition(Boolean::new_item_active(ItemId(1))))
	);*/
}
