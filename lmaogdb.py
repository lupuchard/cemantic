
class ExprPrinter:
    def __init__(self, val):
        self.val = val

    def to_string(self):
        val_type = self.val["type"]
        if val_type == 0:
            return "Void"
        elif val_type == 1:
            return str(self.val["integer"])
        elif val_type == 2:
            item_type = self.val["item"]["type"]
            return ["None", "Key", "Location"][item_type] + str(self.val["item"]["val"])
        elif val_type == 3:
            var = self.val["var"]["d"].dereference()
            return ["g", "a", "p"][var["type"]] + str(var["id"])
        elif val_type == 4:
            return "Expr"
        else:
            func_type = self.val["func"].dereference()["id"]
            return ["Nah", "Equals", "Is", "Causes", "IsOpen", "Realize", "Find", "Wander", "Use"][func_type]


def lookup_type(val):
    if str(val.type) == "Expr":
        return ExprPrinter(val)
    return None


gdb.pretty_printers.append(lookup_type)
